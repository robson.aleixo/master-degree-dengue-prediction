## Contextualização

Esse é um projeto de mestrado do Robson Aleixo, orientado pelo professor Fabio Kon, no Instituto de Ciências e Matemática da Universidade de São Paulo.

O projeto está sendo desenvolvido em parceria com o Insituto de Estudo para Políticas de Saúde da FGV que é coordenado pelo professor Rudy Rocha, como participantes desse instituto temos Marcela Camargo e Thiago Tachibana.

## Escopo da Análise

Este trabalho busca realizar a previsão do número de casos de dengue em uma determinada região ao longo do tempo, considerando análises temporais e da relação do posicionamento entre as região. Além disso, foram consideradas dados  de clima e quantidade estabelecimentos de saúde por região.


## Variáveis Utilizadas

Nesse trabalho estão sendo consideradas as seguintes variáveis:

- Estabelecimentos de Saúde (quantidade de unidade e serviços de saúde)
- Clima (Temperatura e Precipitação)
- Epidemológicos (Casos de dengue)

## Descrição dos Arquivos

Em relação a descrição dos arquivos, tem-se:

- Diretório "codigos_higienizacao"


    - leitura_e_higienizacao.py
        
        Aqui estão localizados o métodos de limpeza das bases originais e consolidação.


- Diretório "machine_learning"

    - preProcessing.py

        Métodos de limpeza da base para criação do input do modelo.

    - mlMethods.py

        Modelos testados para a previsão.

    - mlSupportMethods.py

        Métodos que suportam a execução dos modelos.

    - measuringResults.py

        Métodos para mensuração do desempenho dos modelos.

    - visualization.py

        Métodos para plotagem dos resultados em gráficos.

    - template.py

        Exemplo de como utilizar os métodos basicos para leitura do input e geração da previsão.


## Requisitos para Execução

O código está escrito em Python 3.6 e pip 20.0.2. Foram utilizadas as seguintes bibliotecas:

- pandas 0.24.2
- numpy 1.17.0
- scikit-learn 0.20.3 
- seaborn 0.9.0
- xgboost 1.0.1
- statsmodels 0.11.0
- matplotlib  3.0.3


Para rodar o código basta executar o arquivo template.py e será criado um csv com o resultado da previsão do modelo. 