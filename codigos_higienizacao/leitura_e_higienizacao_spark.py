#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 19 17:54:58 2020

@author: robson
"""

import pandas as pd
import numpy as np
import os
import re
from datetime import date


import findspark
findspark.init('/home/robson/spark-3.0.0-preview2-bin-hadoop2.7')
# findspark.init(os.getcwd() + '/spark-3.0.0-preview2-bin-hadoop2.7')
from pyspark.sql import SparkSession
from pyspark.sql.functions import countDistinct, array_distinct

spark = SparkSession.builder.appName('Basics').getOrCreate()

'''
TO DOs:
    Inserir dados de dengue e influenza
    Inserir dados de chuva e precipatação (cod estão com variação no numero de digitos)    
'''

os.chdir(r'/home/robson/Documents/Mestrado/Topicos avançados em cidades inteligentes/projeto/dados-de-saude')
base_path = os.getcwd()

regiao_code = 'geo_dengue_code_neighborhood'
regiao_name = 'geo_dengue_name_neighborhood'


def cria_base_basica(df, diagnosis_variable):
    
    df = df_dengue_spark.groupby([regiao_code, regiao_name]).agg(
        {diagnosis_variable : 'sum'})
    
    df_pandas = df.select("*").toPandas()
    
    return df


def sanitiza_dengue_with():
    
    df_dengue_spark = spark.read.csv(
        base_path + '/data_sus/input/sinan_dengue_ime.csv', inferSchema=True, 
            header=True)   
    
    df_basis = cria_base_basica(df_dengue_spark, 'dengue_diagnosis')
    

def compare_spark_pandas():
    
    df_dengue_original = pd.read_csv(
        base_path + '/data_sus/input/sinan_dengue_ime.csv')
    
    df_dengue = df_dengue_spark.select('*').toPandas()
    
    df_dengue_original[df_dengue_original.eq(df_dengue).all(axis=1)==False]
    

def comandos_spark():
    
    df_dengue_spark.select(regiao_name).distinct().show(700)