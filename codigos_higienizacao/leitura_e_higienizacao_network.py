#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:39:15 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
from IPython import embed

from machine_learning.preProcessing import *

pd.options.mode.chained_assignment = None
base_path = os.getcwd()


def network_neigboor(df_input):
    '''
    O método consolida as informações de vizinhança e casos
    :param path_input_read:
    :return: dataframe com colunas dos vizinhos de todos os bairros, cada posição de vizinhos tem
    a quantidade de casos de t-1, t-3 e t-6
    '''

    df_net = pd.read_excel(
        base_path + \
        '/data_sus/input/Network_Bairros/Pasta de trabalho.xlsx',
        sheet_name="Higienizado")
        
    columns_net = df_input['nome_bairro'].unique()
        
    df_net_geral = df_input.copy()

    # Cria a coluna de todas as regiões vizinhas
    for c in columns_net:
        df_net_geral[c + ' (t-1)'] = [np.nan] * df_net_geral.shape[0]
        df_net_geral[c + ' (t-3)'] = [np.nan] * df_net_geral.shape[0]
        df_net_geral[c + ' (t-6)'] = [np.nan] * df_net_geral.shape[0]
        
    df_input.index = df_input['nome_bairro']
    df_net_geral.index = df_net_geral['chave']

    # Cada linha do df_net possui a região na coluna Regiao e os respectivos vizinhos nas colunas seguintes
    for i in list(range(0, df_net.shape[0])):

        # connection é a linha que possui a região e os respectivos vizinhos
        connection = df_net.iloc[i,:]
        # c será a região de conexão, o range vai até 18 porque é o número máximo de vizinhos
        for c in list(range(1, 18)):

            # Se igual a zero significa que acabaram as regiões vizinhas e pode analisar ir para a próxima região
            if connection[c] == 0:
                break
            else:

                for a in list(df_net_geral['ano'].unique()):
                    for m in list(df_net_geral['mes'].unique()):
                        try:
                            aux = df_input[(df_input["ano"] == a) & (df_input["mes"] == m)]

                            # Para os casos em que nem todos os anos forem até 12 meses
                            if aux.shape[0] == 0:
                                continue

                            # Cria a chave para especificar a região, ano e mês que terá o valor inserido
                            # chave_regiao_origem = str(df_input.loc[connection['Regiao'], "cod_bairro"])\
                            #                       +str(a)+str(m)

                            chave_regiao_origem = str(aux.loc[connection['Regiao'], "cod_bairro"])\
                                                  +str(a)+str(m)

                            # Busca a chave para a região de análise
                            # chave_regiao_conectada = str(df_input.loc[connection[c], "cod_bairro"])\
                            #                          +str(a)+str(m)
                            chave_regiao_conectada = str(aux.loc[connection[c], "cod_bairro"])\
                                                     +str(a)+str(m)

                            # Na linha cidade conectada coloca os valores de t-1, soma até t-3 e soma até t-6
                            df_net_geral.loc[int(chave_regiao_origem), connection[c]+" (t-1)"] = np.sum(df_net_geral.loc[
                                int(chave_regiao_conectada), "t-1"])
                            df_net_geral.loc[int(chave_regiao_origem), connection[c] + " (t-3)"] = np.sum(df_net_geral.loc[
                                int(chave_regiao_conectada), ["t-1", "t-2", "t-3"]])
                            df_net_geral.loc[int(chave_regiao_origem), connection[c] + " (t-6)"] = np.sum(df_net_geral.loc[
                                int(chave_regiao_conectada), ["t-1", "t-2", "t-3", "t-4", "t-5", "t-6"]])
                        except Exception as ex:
                            embed()

    return df_net_geral


def network_neigboor_mean(df_input):
    '''
    O método consolida as informações de vizinhança e casos
    :param path_input_read:
    :return: dataframe com colunas dos vizinhos de todos os bairros, cada posição de vizinhos tem
    a quantidade de casos de t-1, t-3 e t-6
    '''

    df_net = pd.read_excel(
        base_path + \
        '/data_sus/input/Network_Bairros/Pasta de trabalho.xlsx',
        sheet_name="Higienizado")

    columns_net = df_input['nome_bairro'].unique()

    df_net_geral = df_input.copy()

    # Cria a coluna de todas as regiões vizinhas
    for c in columns_net:
        df_net_geral[c + ' (t-1)'] = [np.nan] * df_net_geral.shape[0]
        df_net_geral[c + ' (t-3)'] = [np.nan] * df_net_geral.shape[0]
        df_net_geral[c + ' (t-6)'] = [np.nan] * df_net_geral.shape[0]

    df_input.index = df_input['nome_bairro']
    df_net_geral.index = df_net_geral['chave']

    # Cada linha do df_net possui a região na coluna Regiao e os respectivos vizinhos nas colunas seguintes
    for i in list(range(0, df_net.shape[0])):

        # connection é a linha que possui a região e os respectivos vizinhos
        connection = df_net.iloc[i, :]
        # c será a região de conexão, o range vai até 18 porque é o número máximo de vizinhos
        for c in list(range(1, 18)):

            # Se igual a zero significa que acabaram as regiões vizinhas e pode analisar ir para a próxima região
            if connection[c] == 0:
                break
            else:

                for a in list(df_net_geral['ano'].unique()):
                    for m in list(df_net_geral['mes'].unique()):
                        try:
                            aux = df_input[(df_input["ano"] == a) & (df_input["mes"] == m)]

                            # Para os casos em que nem todos os anos forem até 12 meses
                            if aux.shape[0] == 0:
                                continue

                            # Cria a chave para especificar a região, ano e mês que terá o valor inserido
                            # chave_regiao_origem = str(df_input.loc[connection['Regiao'], "cod_bairro"])\
                            #                       +str(a)+str(m)

                            chave_regiao_origem = str(aux.loc[connection['Regiao'], "cod_bairro"]) \
                                                  + str(a) + str(m)

                            # Busca a chave para a região de análise
                            # chave_regiao_conectada = str(df_input.loc[connection[c], "cod_bairro"])\
                            #                          +str(a)+str(m)
                            chave_regiao_conectada = str(aux.loc[connection[c], "cod_bairro"]) \
                                                     + str(a) + str(m)

                            # Na linha cidade conectada coloca os valores de t-1, soma até t-3 e soma até t-6
                            df_net_geral.loc[int(chave_regiao_origem), connection[c] + " (t-1)"] = np.mean(
                                df_net_geral.loc[
                                    int(chave_regiao_conectada), "t-1"])
                            df_net_geral.loc[int(chave_regiao_origem), connection[c] + " (t-3)"] = np.mean(
                                df_net_geral.loc[
                                    int(chave_regiao_conectada), ["t-1", "t-2", "t-3"]])
                            df_net_geral.loc[int(chave_regiao_origem), connection[c] + " (t-6)"] = np.mean(
                                df_net_geral.loc[
                                    int(chave_regiao_conectada), ["t-1", "t-2", "t-3", "t-4", "t-5", "t-6"]])
                        except Exception as ex:
                            embed()

    return df_net_geral


def get_indices_columns_time(columns):

    columns_time_um = list()
    columns_time_tres = list()
    columns_time_seis = list()
    columns_others = list()

    for c in columns:
        if re.match(".*\(t-1\).*", c):
            columns_time_um.append(c)
        elif re.match(".*\(t-3\).*", c):
            columns_time_tres.append(c)
        elif re.match(".*\(t-6\).*", c):
            columns_time_seis.append(c)
        else:
            columns_others.append(c)

    columns_sequence = columns_others + columns_time_um + columns_time_tres + columns_time_seis

    index_time_cols_um = (len(columns_others), len(columns_others) + len(columns_time_um) - 1)
    index_time_cols_tres = (index_time_cols_um[1]+1,  index_time_cols_um[1] + len(columns_time_tres))
    index_time_cols_seis = (index_time_cols_tres[1] + 1, index_time_cols_tres[1] + len(columns_time_seis))

    return columns_sequence, index_time_cols_um, index_time_cols_tres, index_time_cols_seis


def mean_max_neighboors():

    path_input_read = base_path + '/data_sus/finais/input_ml_ocorrencia_doencas_v3.csv'

    df_input_net = network_neigboor(path_input_read)

    df_input = read_and_get_input_v2(path_input_read)

    for c in list(df_input.columns):
        if re.match(".*Unname.*", c):
            df_input.drop(columns=[c], inplace=True)
            if c in list(df_input_net.columns):
                df_input_net.drop(columns=[c], inplace=True)
        elif c in ['min_vizinhos', 'max_vizinhos']:
            df_input.drop(columns=[c], inplace=True)

    columns_sequence, index_time_cols_um, index_time_cols_tres, index_time_cols_seis = \
        get_indices_columns_time(list(df_input_net.columns))

    df_input_net = df_input_net[columns_sequence]

    max_cases_um = list()
    mean_cases_um = list()
    max_cases_tres = list()
    mean_cases_tres = list()
    max_cases_seis = list()
    mean_cases_seis = list()
    for i in list(range(df_input_net.shape[0])):
        max_cases_um.append(np.max(df_input_net.iloc[i, index_time_cols_um[0]:index_time_cols_um[1]]))
        mean_cases_um.append(np.mean(df_input_net.iloc[i, index_time_cols_um[0]:index_time_cols_um[1]]))
        max_cases_tres.append(np.max(df_input_net.iloc[i, index_time_cols_tres[0]:index_time_cols_tres[1]]))
        mean_cases_tres.append(np.mean(df_input_net.iloc[i, index_time_cols_tres[0]:index_time_cols_tres[1]]))
        max_cases_seis.append(np.max(df_input_net.iloc[i, index_time_cols_seis[0]:index_time_cols_seis[1]]))
        mean_cases_seis.append(np.mean(df_input_net.iloc[i, index_time_cols_seis[0]:index_time_cols_seis[1]]))

    df_input['max_vizinhos_t-1'] = max_cases_um
    df_input['media_vizinhos_t-1'] = mean_cases_um
    df_input['max_vizinhos_t-3'] = max_cases_tres
    df_input['media_vizinhos_t-3'] = mean_cases_tres
    df_input['max_vizinhos_t-6'] = max_cases_seis
    df_input['media_vizinhos_t-6'] = mean_cases_seis

    df_input['media_vizinhos_t-1'] = df_input['media_vizinhos_t-1'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['media_vizinhos_t-3'] = df_input['media_vizinhos_t-3'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['media_vizinhos_t-6'] = df_input['media_vizinhos_t-6'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['max_vizinhos_t-1'] = df_input['max_vizinhos_t-1'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['max_vizinhos_t-3'] = df_input['max_vizinhos_t-3'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['max_vizinhos_t-6'] = df_input['max_vizinhos_t-6'].apply(lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)

    df_input.to_csv(base_path + \
            '/data_sus/finais/input_ml_ocorrencia_doencas_v5.csv')


def sum_neighboors(df_input):
    '''
    Consolida as colunas de vizinhos nas colunas sum_vizinhos
    :param df_input:
    :return:
    '''
    df_input_net = network_neigboor(df_input)

    # df_input = read_and_get_input_v2(path_input_read)

    for c in list(df_input.columns):
        if re.match(".*Unname.*", c):
            df_input.drop(columns=[c], inplace=True)
            if c in list(df_input_net.columns):
                df_input_net.drop(columns=[c], inplace=True)
        # elif c in ['min_vizinhos', 'max_vizinhos']:
        #     df_input.drop(columns=[c], inplace=True)

    columns_sequence, index_time_cols_um, index_time_cols_tres, index_time_cols_seis = \
        get_indices_columns_time(list(df_input_net.columns))

    df_input_net = df_input_net[columns_sequence]

    sum_cases_um = list()
    sum_cases_tres = list()
    sum_cases_seis = list()

    for i in list(range(df_input_net.shape[0])):
        sum_cases_um.append(np.sum(df_input_net.iloc[i, index_time_cols_um[0]:index_time_cols_um[1]]))
        sum_cases_tres.append(np.sum(df_input_net.iloc[i, index_time_cols_tres[0]:index_time_cols_tres[1]]))
        sum_cases_seis.append(np.sum(df_input_net.iloc[i, index_time_cols_seis[0]:index_time_cols_seis[1]]))

    df_input['sum_vizinhos_t-1'] = sum_cases_um
    df_input['sum_vizinhos_t-3'] = sum_cases_tres
    df_input['sum_vizinhos_t-6'] = sum_cases_seis

    df_input['sum_vizinhos_t-1'] = df_input['sum_vizinhos_t-1'].apply(
        lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['sum_vizinhos_t-3'] = df_input['sum_vizinhos_t-3'].apply(
        lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
    df_input['sum_vizinhos_t-6'] = df_input['sum_vizinhos_t-6'].apply(
        lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)

    # df_input.to_csv(base_path + \
    #         '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    return df_input


def sum_neighboors_to_prediction(df_input, neighbor_columns, taxa=False):
    '''
    Consolida as colunas de vizinhos nas colunas sum_vizinhos
    :param df_input:
    :return:
    '''
    if not taxa:

        df_input_net = network_neigboor(df_input)

        for c in list(df_input_net.columns):
            if re.match(".*Unname.*", c):
                df_input_net.drop(columns=[c], inplace=True)
            # elif c in ['min_vizinhos', 'max_vizinhos']:
            #     df_input.drop(columns=[c], inplace=True)

        columns_sequence, index_time_cols_um, index_time_cols_tres, index_time_cols_seis = \
            get_indices_columns_time(list(df_input_net.columns))

        df_input_net = df_input_net[columns_sequence]

        sum_cases_um = list()
        sum_cases_tres = list()
        sum_cases_seis = list()

        for i in list(range(df_input_net.shape[0])):
            sum_cases_um.append(np.sum(df_input_net.iloc[i, index_time_cols_um[0]:index_time_cols_um[1]]))
            sum_cases_tres.append(np.sum(df_input_net.iloc[i, index_time_cols_tres[0]:index_time_cols_tres[1]]))
            sum_cases_seis.append(np.sum(df_input_net.iloc[i, index_time_cols_seis[0]:index_time_cols_seis[1]]))

        df_input['sum_vizinhos_t-1'] = sum_cases_um
        df_input['sum_vizinhos_t-3'] = sum_cases_tres
        df_input['sum_vizinhos_t-6'] = sum_cases_seis

        df_input['sum_vizinhos_t-1'] = df_input['sum_vizinhos_t-1'].apply(
            lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
        df_input['sum_vizinhos_t-3'] = df_input['sum_vizinhos_t-3'].apply(
            lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)
        df_input['sum_vizinhos_t-6'] = df_input['sum_vizinhos_t-6'].apply(
            lambda x: round(x) if re.match(".*[0-9].*", str(x)) else 0)

    # Não da certo utilizar isso aqui ainda, pois precisa modificar o método network_neigboor para calcular a
    # média dos t-n, porque nesse caso os t-n já são taxas por 100 habitantes
    else:

        df_input_net = network_neigboor_mean(df_input)

        for c in list(df_input_net.columns):
            if re.match(".*Unname.*", c):
                df_input_net.drop(columns=[c], inplace=True)
            # elif c in ['min_vizinhos', 'max_vizinhos']:
            #     df_input.drop(columns=[c], inplace=True)

        columns_sequence, index_time_cols_um, index_time_cols_tres, index_time_cols_seis = \
            get_indices_columns_time(list(df_input_net.columns))

        df_input_net = df_input_net[columns_sequence]

        avg_cases_um = list()
        avg_cases_tres = list()
        avg_cases_seis = list()

        for i in list(range(df_input_net.shape[0])):
            avg_cases_um.append(np.mean(df_input_net.iloc[i, index_time_cols_um[0]:index_time_cols_um[1]]))
            avg_cases_tres.append(np.mean(df_input_net.iloc[i, index_time_cols_tres[0]:index_time_cols_tres[1]]))
            avg_cases_seis.append(np.mean(df_input_net.iloc[i, index_time_cols_seis[0]:index_time_cols_seis[1]]))

        df_input['sum_vizinhos_t-1'] = avg_cases_um
        df_input['sum_vizinhos_t-3'] = avg_cases_tres
        df_input['sum_vizinhos_t-6'] = avg_cases_seis

        df_input['sum_vizinhos_t-1'] = df_input['sum_vizinhos_t-1'].apply(
            lambda x: round(x, 2) if re.match(".*[0-9].*", str(x)) else 0)
        df_input['sum_vizinhos_t-3'] = df_input['sum_vizinhos_t-3'].apply(
            lambda x: round(x, 2) if re.match(".*[0-9].*", str(x)) else 0)
        df_input['sum_vizinhos_t-6'] = df_input['sum_vizinhos_t-6'].apply(
            lambda x: round(x, 2) if re.match(".*[0-9].*", str(x)) else 0)

    # df_input.to_csv(base_path + \
    #         '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    return df_input[neighbor_columns]


if __name__ == "__main__":

    path_input_read = base_path + '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv'

    df = pd.read_csv(base_path + \
            '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    df = network_neigboor(df)
    df.to_csv(base_path + \
            '/data_sus/finais/input_dengue_network_sum_cases.csv')

    sum_neighboors(df)

