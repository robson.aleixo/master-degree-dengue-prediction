import requests
from selenium import webdriver
from pyvirtualdisplay import Display
from selenium.webdriver.common.by import By
import pandas as pd
import numpy as np
import time

DRIVER_PATH = '/home/robson/PycharmProjects/Olonus/WedDataCollection'

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36'}

url_wheater = "http://www.accuweather.com/pt/br/rio-de-janeiro/45449/april_weather"
city = "São Paulo"
country = "BR"

response = requests.get(url_wheater, headers=headers).json()

#####################################
#   API de temperatura              #
#####################################
def get_weather_information():

    months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 
              'august', 'september', 'october', 'november', 'december']
    
    month_json = {'january': 1, 'february': 2, 'march': 3, 'april': 4, 
                  'may': 5, 'june':6, 'july':7, 'august':8,
                  'september':9, 'october':10, 'november':11, 'december':12}
    
    years = ['2016', '2017', '2018', '2019']
    
    cids = requests.get("https://servicodados.ibge.gov.br/api/v1/localidades/estados/33/municipios").json()
    
    cids_list = list()
    for c in cids:
        cids_list.append(c['nome'])
    
    url_accuWeather = "https://www.accuweather.com/pt/br/#cid#/45449/#month#-weather/45449?year=#year#&view=list"
    chrome_opt = webdriver.ChromeOptions()
    chrome_opt.add_argument('--no-sandbox')
    
    display = Display(visible=0, size=(1700, 1200))
    display.start()
    
    driver = webdriver.Chrome()
    
    df_final = pd.DataFrame(columns=["Cidade", "Data", "Temp Max", 
                                     "Temp Min", "Preciptacao"])
    c_replaced = "rio-de-janeiro"
    c = "rio-de-janeiro"
    for n, c in enumerate(cids_list):
        print("{} / {}".format(n, len(cids_list)))
        c_replaced = sanitiza_string(c)
        for y in years:
            for m in months:
                try:
                    url_accuWeather_repl =  url_accuWeather.replace('#year#', y)
                    url_accuWeather_repl =  url_accuWeather_repl.replace('#month#', m)
                    url_accuWeather_repl =  url_accuWeather_repl.replace("#cid#", c_replaced)
                    driver.get(url_accuWeather_repl)
                    time.sleep(0.2)
                    elems_high = driver.find_elements_by_class_name('high')
                    elems_low = driver.find_elements_by_class_name('low')
                    elems_prec = driver.find_elements_by_tag_name('p')
                    elems_prec_sanitizada = dict()
                    for x, e in enumerate(elems_prec):
                        try:
                            if re.match("[0-9]{1,2}/[0-9]{1,2}", e.text):
                                elems_prec_sanitizada[e.text.split('/')[0]] = \
                                    float(elems_prec[x+2].text.replace("mm","").strip())
                        except:
                            print("erro precip: {}".format(elems_prec[x+2].text))
                            
                    
                    for i in list(range(0, len(elems_high))):
                        data = dict()
                        data["Cidade"] = [c]
                        data['Data'] = [date(int(y), month_json[m], i+1)]
                        try:
                            data['Temp Max'] = [int(re.match("([0-9]{1,2})", 
                                                     elems_high[i].text)[0])]
                        except:
                            data['Temp Max'] = [np.nan]
                        
                        try:
                            data['Temp Min'] = [int(elems_low[i].text.replace('°', '').replace("/", "").strip())]
                        except: 
                            data['Temp Min'] = [np.nan]
                        
                        try:
                            data['Preciptacao'] = [elems_prec_sanitizada[str(i+1)]]
                        except:
                            data['Preciptacao'] = [np.nan]
                            
                        df = pd.DataFrame(data)
                        df_final = df_final.append(df)
                        
                except Exception as ex:
                    print("cid: {} \ ano: {} \ mes: {} \ ex: {} ".format(
                        c, y, m, ex))
                    continue
    
    df_final.to_csv(base_path + "/data_sus/intermediarias/temp_preci_por_municipio_2010_2014.csv")
    
 