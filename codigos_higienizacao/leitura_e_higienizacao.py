import pandas as pd
import numpy as np
import os
import re
from datetime import date
import time
from IPython import embed
from matplotlib import pyplot as plt
from codigos_higienizacao.leitura_e_higienizacao_network import sum_neighboors_to_prediction

'''
TO DOs:
     - Olhar base baixa do inmet, agrupar por semana e ver como ficam 
    os missing values, lembrando que da para preencher com o anterior, ou com a média  
    
    - Com a url "https://www.accuweather.com/pt/br/rio-de-janeiro/45449/april-weather/45449?year=2020&view=list"
    pegar a temperatura do futuro (3 meses pra frente)
    
    - essa url "https://www.accuweather.com/pt/br/rio-de-janeiro/45449/april-weather/45449?year=2019"
    pode ser importante tbm para pegar dados do futuro, ver qual fica melhor
    
    - Ver como a coleta funciona para outros municipios do accuweather
    
    - Colocar a ocorrencia de sifilis na base geral. Pegar a da influenza
    
    
'''
base_path = os.getcwd()

regiao_code = 'geo_dengue_code_neighborhood'
regiao_name = 'geo_dengue_name_neighborhood'

# colunas_censo = ['cod_bairro', 'domicilios', 'dom_partic', 'dom_improv', 
#     'dom_coletivos', 'moradores','mor_partic', 'mor_coletivos', 
#     'media_mor_dom', 'perc_dom_agua_rede','perc_dom_agua_poco_nascente', 
#     'perc_dom_agua_chuva','perc_dom_esgoto_rede', 'perc_dom_esgoto_fossa',
#     'perc_dom_esgoto_fossa_rudi','perc_dom_esgoto_vala', 'perc_dom_esgoto_mar', 
#     'perc_dom_esgoto_outro','perc_dom_lixo_coletado', 
#     'perc_dom_lixo_coletado_servico','perc_dom_lixo_coletado_cacamba',
#     'perc_dom_sem_lixo_coletado','perc_dom_energia', 'perc_mor_esgoto_rede',
#     'perc_mor_esgoto_fossa', 'perc_mor_esgoto_fossa_rudi', 
#     'perc_mor_esgoto_vala', 'perc_mor_esgoto_mar', 'perc_mor_esgoto_outro']

# colunas_censo_m = ['cod_bairro', 'moradores']

def sanitiza_string(a):
    
    a = a.lower().strip()
    a = re.sub('á|à|ã|â', 'a', a)
    a = re.sub('é|ê', 'e', a)
    a = re.sub('í', 'i', a)
    a = re.sub('ó|õ|ô', 'o', a)
    a = re.sub('ú', 'u', a)
    a = re.sub('ç', 'c', a)
    a = a.replace('sao luiz do paraitinga', 'sao luis do paraitinga')
    a = a.replace('florinea', 'florinia')
    a = a.replace('moji', 'mogi')
    a = a.replace('', '')
    a = a.replace('a\x8d', 'i')
    a = a.replace(' ', '-')
    a = a.replace('\xa0', '')
    
    return a

######################
# Cria base geral    #
######################
'''
Para utilizar esse método é importante lembrar de atualizar as variáveis
"regiao_code e regiao_name"
'''
def cria_base_basica(df, diagnosis_variable, type_aggr_time):
    
    df_aux = pd.pivot_table(df,
                            index=[regiao_code,
                                   regiao_name],
                            values=diagnosis_variable, 
                            aggfunc=np.sum)
    
    df_aux[regiao_code] = list(df_aux.index.get_level_values(0))
    df_aux[regiao_name] = list(df_aux.index.get_level_values(1))

    # df_aux = df_aux.reset_index()
        
    df_basis = pd.DataFrame(df_aux.loc[:,[regiao_code, 
                                   regiao_name]])
    
    years = list(range(2007, 2023, 1))
    if type_aggr_time == 'mes':
        aggr_time = list(range(1, 13, 1))
    else:
        aggr_time = list(range(1, 54, 1))
        
        
    df = pd.DataFrame()
    
    for i in list(range(0, df_basis.shape[0])):
        for y in years:
            for m in aggr_time:            
               df = df.append([[df_basis.iloc[i,0],
                           df_basis.iloc[i,1],
                           y, m]])
               
    df.columns = [regiao_code, regiao_name,
                    'ano', type_aggr_time]
    
    df['chave'] = df[[regiao_code,
                    'ano', type_aggr_time]].apply(
                        lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
    
    return df


#############################
# Base tuberculose          #
#############################
    
def sanitiza_tuberculose():
    
    df_tuberculosis = pd.read_csv(
        base_path + '/data_sus/input/sinan_tuberculosis_ime.csv')
    
    df_basis = cria_base_basica(df_tuberculosis, 'tuberculosis_diagnosis')
    
    month = {'jan': 1, 'feb':2, 'mar':3, 'apr':4, 'may':5, 'jun':6, 'jul':7, 
             'aug':8, 'sep':9, 'oct':10, 'nov':11, 'dec':12}
    
    df_tuberculosis['mes/ano'] = df_tuberculosis['dt_notific'].apply(
            lambda x: date(int(x[5:]), month[x[2:5]], 1) if len(x) == 9 else "ddd")
    
    df_tuberculosis['ano'] = df_tuberculosis['mes/ano'].apply(lambda x: x.year)
    df_tuberculosis['mes'] = df_tuberculosis['mes/ano'].apply(lambda x: x.month)
    
    # Base de estabelecimentos só tem de 2015 pra frente
    df_tuberculosis = df_tuberculosis[df_tuberculosis['ano'] > 2006]
    
    df_tuberculosis['chave'] = df_tuberculosis[['geo_tube_code_neighborhood',
            'ano', 'mes']].apply(
                lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
    
    df_tuber_pivot = pd.pivot_table(df_tuberculosis,
                                            index=['chave'],
                                            values='tuberculosis_diagnosis', 
                                            aggfunc=np.sum)
    
    df_basis = pd.merge(df_basis, df_tuber_pivot,
                        on='chave', how='left')
        
    df_basis.to_csv(
        base_path + '/data_sus/intermediarias/sinasc_tuberculosis_agrup_bairro.csv')


#############################
# Base dengue               #
#############################
    
def sanitiza_dengue_mes():
    
    df_dengue = pd.read_csv(
        base_path + '/data_sus/input/sinan_dengue_ime.csv')
    
    df_basis = cria_base_basica(df_dengue, 'dengue_diagnosis', "mes")
    
    month = {'jan': 1, 'feb':2, 'mar':3, 'apr':4, 'may':5, 'jun':6, 'jul':7, 
             'aug':8, 'sep':9, 'oct':10, 'nov':11, 'dec':12}
    
    df_dengue['mes/ano'] = df_dengue['dt_notific'].apply(
            lambda x: date(int(x[5:]), month[x[2:5]], int(x[:2])) if len(str(x)) == 9 else np.nan)
    
    df_dengue.dropna(subset=['mes/ano'], inplace=True)
    
    df_dengue['ano'] = df_dengue['mes/ano'].apply(lambda x: x.year)
    df_dengue['mes'] = df_dengue['mes/ano'].apply(lambda x: x.month)
    
    # Base de estabelecimentos só tem de 2015 pra frente
    df_dengue = df_dengue[df_dengue['ano'] > 2006]
    
    df_dengue['chave'] = df_dengue[['geo_dengue_code_neighborhood',
            'ano', 'mes']].apply(
                lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
    
    df_dengue_pivot = pd.pivot_table(df_dengue,
                                            index=['chave'],
                                            values='dengue_diagnosis', 
                                            aggfunc=np.sum)
    
    df_basis = pd.merge(df_basis, df_dengue_pivot,
                        on='chave', how='left')
    
    df_basis.fillna(0, inplace=True)
        
    df_basis.to_csv(
        base_path + '/data_sus/intermediarias/Old/sinan_dengue_agrup_bairro_ano_mes.csv')
    

def sanitiza_dengue_semana():
    
    df_dengue = pd.read_csv(
        base_path + '/data_sus/input/sinan_dengue_ime.csv')
    
    df_basis = cria_base_basica(df_dengue, 'dengue_diagnosis', "semana")
    
    month = {'jan': 1, 'feb':2, 'mar':3, 'apr':4, 'may':5, 'jun':6, 'jul':7, 
             'aug':8, 'sep':9, 'oct':10, 'nov':11, 'dec':12}
    
    df_dengue['mes/ano'] = df_dengue['dt_notific'].apply(
            lambda x: date(int(x[5:]), month[x[2:5]], int(x[:2])) if len(str(x)) == 9 else np.nan)
    
    df_dengue.dropna(subset=['mes/ano'], inplace=True)
    
    df_dengue['ano'] = df_dengue['mes/ano'].apply(lambda x: x.year)
    df_dengue['semana'] = df_dengue['mes/ano'].apply(lambda x: x.isocalendar()[1])
    
    # Base de estabelecimentos só tem de 2015 pra frente
    df_dengue = df_dengue[df_dengue['ano'] > 2006]
    
    df_dengue['chave'] = df_dengue[['geo_dengue_code_neighborhood',
            'ano', 'semana']].apply(
                lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
    
    df_dengue_pivot = pd.pivot_table(df_dengue,
                                            index=['chave'],
                                            values='dengue_diagnosis', 
                                            aggfunc=np.sum)
    
    df_basis = pd.merge(df_basis, df_dengue_pivot,
                        on='chave', how='left')
    
    df_basis.fillna(0, inplace=True)
        
    df_basis.to_csv(
        base_path + '/data_sus/intermediarias/sinan_dengue_agrup_bairro_por_ano_semana.csv')


def sanitiza_dengue_bases_do_portal_rj():

    path = "/home/robson/Documents/Mestrado/Pesquisa/Dados/Casos de Dengue"

    arqs = os.listdir(path)

    match_1 = ".*Total.*|.*Bairro.*|.*Programática.*|.*I .*|.*II .*|.*III .*|.*VII .*|.*XXI .*|"
    match_2 = ".*XXIII .*|.*IV .*|.*V .*|.*VI .*|.*XXVII .*|.*VIII .*|.*IX .*|.*X .*|.*XI .*|"
    match_3 = ".*XX .*|.*XXIX .*|.*XXX .*|.*XII .*|.*XIII .*|.*nan.*|.*Obs.*|.*Xvi.*|.*XVII.*|.*Ignorado.*"

    match = match_1 + match_2 + match_3

    df_result = pd.DataFrame(columns=['Bairros', 'Pop. Censo 2010', 'Mes', 'Dengue', 'ano'])

    month = {'Jan': 1, 'Fev': 2, 'Mar': 3, 'Abr': 4, 'Mai': 5, 'Jun': 6, 'Jul': 7,
             'Ago': 8, 'Set': 9, 'Out': 10, 'Nov': 11, 'Dez': 12}

    for a in arqs:
        if re.match(".*xlsx", a):
            try:
                df_aux = pd.read_excel(path + \
                                       "/" + a)
                df_aux.columns = ['Bairros',
                                   'Pop. Censo 2010', 'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul',
                                   'Ago', 'Set', 'Out', 'Nov', 'Dez', 'Total']

                df_aux['fica'] = df_aux['Bairros'].apply(lambda x: 1 if not re.match(match, str(x)) else 0)
                df_aux = df_aux[df_aux['fica'] == 1]
                df_aux.drop(columns=["Total", 'fica'], inplace=True)
                ano = re.search("20[0-9]{2}", a).group()
                # print("{}: {}".format(ano, df_aux.shape[0]))
                df_aux = df_aux.melt(id_vars=['Bairros', 'Pop. Censo 2010'],
                                     value_vars=['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul',
                                   'Ago', 'Set', 'Out', 'Nov', 'Dez'])
                df_aux['ano'] = [int(ano)] * df_aux.shape[0]
                df_aux.columns = ['Bairros', 'Pop. Censo 2010', 'Mes', 'Dengue', 'ano']
                df_aux['Bairros'] = df_aux['Bairros'].apply(lambda x: x.replace("*", '').strip())

                df_result = df_result.append(df_aux)
            except Exception as ex:
                embed()

    df_result['Bairros'] = df_result['Bairros'].apply(lambda x: x.replace("ds", 'os').replace('Taua', 'Tauá'))
    df_result['Bairros'] = df_result['Bairros'].apply(lambda x: x.replace('Gericino', 'Gericinó').replace("Cavalcanti", "Cavalcante"))
    df_result['Bairros'] = df_result['Bairros'].apply(lambda x: x.replace('Gardenia Azul', 'Gardênia Azul'))
    df_result['Bairros'] = df_result['Bairros'].apply(lambda x: x.replace('Quintino Bocaiuva', 'Quintino Bocaiúva'))
    df_result['Bairros'] = df_result['Bairros'].apply(lambda x: x.replace("Complexo da Maré", 'Maré'))
    df_result['mes'] = df_result['Mes'].apply(lambda x: month[x])
    df_result['Pop. Censo 2010'] = df_result['Pop. Censo 2010'].apply(
        lambda x: int(str(x).replace("1.97", "1970").replace(".0", "").replace(".", "").replace(" ", "")))
    df_result.drop(columns=['Mes'], inplace=True)

    df_result.sort_values(by=["Bairros"], inplace=True)

    return df_result


#############################
# Base Influenza            #
#############################
    
def sanitiza_influenza():
    
    df = pd.read_csv(
        base_path + '/data_sus/input/sinan_influenza_ime.csv')
    
    df_basis = cria_base_basica(df, 'influenza_diagnosis', "mes")
    
    month = {'jan': 1, 'feb':2, 'mar':3, 'apr':4, 'may':5, 'jun':6, 'jul':7, 
             'aug':8, 'sep':9, 'oct':10, 'nov':11, 'dec':12}
    
    df['mes/ano'] = df['dt_notific'].apply(
            lambda x: date(int(x[5:]), month[x[2:5]], 1) if len(str(x)) == 9 else date(1900, 1, 1))
    
    df['ano'] = df['mes/ano'].apply(lambda x: x.year)
    df['mes'] = df['mes/ano'].apply(lambda x: x.month)
    
    # Base de estabelecimentos só tem de 2015 pra frente
    df = df[df['ano'] > 2006]
    
    df['chave'] = df[['geo_influ_code_neighborhood',
            'ano', 'mes']].apply(
                lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
    
    df_pivot = pd.pivot_table(df,
                                index=['chave'],
                                values='influenza_diagnosis', 
                                aggfunc=np.sum)
    
    df_basis = pd.merge(df_basis, df_pivot,
                        on='chave', how='left')
    
    df_basis.fillna(0, inplace=True)
    
    # df_estabelecimentos_temp = pd.read_csv(
    #     base_path + '/data_sus/finais/dengue_temp_prec_umi_reg_SARIMAX.csv')
    #
    # df_estabelecimentos_temp = df_estabelecimentos_temp[['chave', 'cnes',
    #                                                      'd_gpsv1', 'd_gpsv2',
    #                                                      'd_gpsv4', 'd_gpsv5',
    #                                                      'd_gpsv7', 'd_sus',
    #                                                      'alstdi03', 'prec_month',
    #                                                      'tuberculosis_diagnosis',
    #                                                      'dengue_diagnosis'
    #                                                      ]]
    #
    # df_basis = pd.merge(df_basis, df_estabelecimentos_temp,
    #                     on='chave')
        
    df_basis.to_csv(
        base_path + '/data_sus/intermediarias/sinan_influenza_agrup_bairro.csv')


#############################
# Base Sifilis              #
#############################
    
def sanitiza_sifilis():
    
    def prepare_base(df):
        
        month = {'jan': 1, 'feb':2, 'mar':3, 'apr':4, 'may':5, 'jun':6, 'jul':7, 
         'aug':8, 'sep':9, 'oct':10, 'nov':11, 'dec':12}
    
        df['mes/ano'] = df['dt_notific'].apply(
                lambda x: date(int(x[5:]), month[x[2:5]], 1) if len(str(x)) == 9 else date(1900, 1, 1))
        
        df['ano'] = df['mes/ano'].apply(lambda x: x.year)
        df['mes'] = df['mes/ano'].apply(lambda x: x.month)
        
        df = df[df['ano'] > 2006]
        
        df['chave'] = df[['geo_sifil_code_neighborhood',
                'ano', 'mes']].apply(
                    lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
        
        df_pivot = pd.pivot_table(df,
                                    index=['chave'],
                                    values='syphilis_diagnosis', 
                                    aggfunc=np.sum)
        
        return df_pivot
    
    def join_columns(df, columns, new_column_name):
        
        df_basis[new_column_name] = df_basis[columns].apply(
            lambda x: x[0] if not str(x[0]) == 'nan' else x[1] if not str(x[1]) == 'nan' else x[2], axis=1)
        
        df_basis.drop(columns=columns, inplace=True)    
        
        return df_basis
        
    df_adquirida = pd.read_csv(
        base_path + '/data_sus/input/sinan_sifilis_adquirida_ime.csv')
    
    df_congenita = pd.read_csv(
        base_path + '/data_sus/input/sinan_sifilis_congenita_ime.csv')
    
    df_gestante = pd.read_csv(
        base_path + '/data_sus/input/sinan_sifilis_gestante_ime.csv')
    
    df_basis_adquirida = cria_base_basica(df_adquirida, 'syphilis_diagnosis')
    df_basis_congenita = cria_base_basica(df_congenita, 'syphilis_diagnosis')
    df_basis_gestante = cria_base_basica(df_gestante, 'syphilis_diagnosis')
        
    df_basis = pd.merge(df_basis_adquirida, df_basis_congenita,
                        on='chave', how='outer')
    
    df_basis = pd.merge(df_basis, df_basis_gestante,
                        on='chave', how='outer')

    df_basis = join_columns(df_basis, 
                            ['geo_sifil_code_neighborhood_x', 
                             'geo_sifil_code_neighborhood_y', 
                             'geo_sifil_code_neighborhood'], 'cod_bairro')
    df_basis = join_columns(df_basis, 
                            ['geo_sifil_name_neighborhood_x', 
                             'geo_sifil_name_neighborhood_y', 
                             'geo_sifil_name_neighborhood'], 'nome_bairro')
    df_basis = join_columns(df_basis, ['ano_x', 'ano_y', 'ano'], "ano_")
    df_basis = join_columns(df_basis, ['mes_x', 'mes_y', 'mes'], "mes_")
    
    df_basis.columns = ['chave', 'cod_bairro', 'nome_bairro', 'ano', 'mes']

    df_pivot_adquirida = prepare_base(df_adquirida)
    df_pivot_congenita = prepare_base(df_congenita)
    df_pivot_gestante = prepare_base(df_gestante)

    df_pivot_adquirida.columns = ['syphilis_adquirida_diagnosis']
    df_pivot_congenita.columns = ['syphilis_congenita_diagnosis']
    df_pivot_gestante.columns = ['syphilis_gestante_diagnosis']
    
    df_basis = pd.merge(df_basis, df_pivot_adquirida,
                        on='chave', how='left')
    df_basis = pd.merge(df_basis, df_pivot_congenita,
                        on='chave', how='left')
    df_basis = pd.merge(df_basis, df_pivot_gestante,
                        on='chave', how='left')
    
    df_basis.fillna(0, inplace=True)
    
    df_estabelecimentos_temp = pd.read_csv(
        base_path + '/data_sus/finais/dengue_temp_prec_umi_reg_SARIMAX.csv')
    
    df_estabelecimentos_temp = df_estabelecimentos_temp[['chave', 'cnes', 
                                                         'd_gpsv1', 'd_gpsv2', 
                                                         'd_gpsv4', 'd_gpsv5', 
                                                         'd_gpsv7', 'd_sus',
                                                         'alstdi03', 'prec_month',
                                                         'tuberculosis_diagnosis',
                                                         'dengue_diagnosis'
                                                         ]]
    
    df_basis = pd.merge(df_basis, df_estabelecimentos_temp,
                        on='chave')
    
    df_basis.columns = ['chave', 'cod_bairro', 'nome_bairro', 'ano', 'mes',
       'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis',
       'syphilis_gestante_diagnosis', 'cnes', 'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 'qtd_serv_prenatal',
       'qtd_serv_neonatal', 'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 'temp_solo', 'prec_month',
       'tuberculosis_diagnosis', 'dengue_diagnosis']
    

    df_basis = df_basis[['chave', 'cod_bairro', 'nome_bairro', 'ano', 'mes',
                         'cnes', 'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 'qtd_serv_prenatal',
                         'qtd_serv_neonatal', 'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                         'temp_solo', 'prec_month', 'tuberculosis_diagnosis', 'dengue_diagnosis', 
                         'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis', 'syphilis_gestante_diagnosis']]
        
    df_basis.to_csv(
        base_path + '/data_sus/finais/input_ocorrencia_doencas.csv')


######################################
# Estabelecimento, Censo             #
######################################

def sanitiza_variaveis_ambiente(df_basis):
    
    def sanitiza_estabelecimento_mes():
        
        df_estabelecimentos = pd.read_csv(
            base_path + '/data_sus/input/estabelecimentos/estabelecimentos_cnes.csv', 
            sep='|') 
        
        df_estabelecimentos['ano'] = df_estabelecimentos['anomes'].apply(
            lambda x: str(x)[:4])
        
        df_estabelecimentos['mes'] = df_estabelecimentos['anomes'].apply(
            lambda x: str(x)[4:6].replace('0', ''))
        
        df_estabelecimentos['chave'] = df_estabelecimentos[
            ['code_neighborhood', 'ano', 'mes']].apply(
            lambda x: int(str(x[0]) + str(x[1]) + str(x[2])), axis=1)
                
        df_pivot_cnes = pd.pivot_table(df_estabelecimentos, 
                                           index=['chave'],
                                           values='cnes',
                                           aggfunc='count')
        
        df_pivot_services = pd.pivot_table(df_estabelecimentos, 
                                           index=['chave'],
                                           values=['d_gpsv1', 'd_gpsv2', 
                                                   'd_gpsv4', 'd_gpsv5',
                                                   'd_gpsv7', 'd_sus'],
                                           aggfunc=np.sum)
        
        df_estabelecimentos = pd.merge(df_pivot_cnes, 
                                     df_pivot_services,
                                     on='chave')
        
        df_estabelecimentos.to_csv(
            base_path + '/data_sus/intermediarias/estabelecimentos_sanitizada.csv')
                        
        return df_estabelecimentos
    
    def sanitiza_estabelecimento_semana(features):
        
        df_estabelecimentos = pd.read_csv(
            base_path + '/data_sus/input/estabelecimentos/estabelecimentos_cnes.csv', 
            sep='|') 
        
        df_estabelecimentos['ano'] = df_estabelecimentos['anomes'].apply(
            lambda x: str(x)[:4])
        
        df_estabelecimentos['mes'] = df_estabelecimentos['anomes'].apply(
            lambda x: str(x)[4:6].replace('0', ''))
        
        df_estabelecimentos['chave'] = df_estabelecimentos[
            ['code_neighborhood', 'ano', 'mes']].apply(
            lambda x: int(str(x[0]) + str(x[1]) + str(x[2])), axis=1)
                
        df_pivot_cnes = pd.pivot_table(df_estabelecimentos, 
                                           index=['chave'],
                                           values='cnes',
                                           aggfunc='count')
        
        df_pivot_services = pd.pivot_table(df_estabelecimentos, 
                                           index=['chave'],
                                           values=['d_gpsv1', 'd_gpsv2', 
                                                   'd_gpsv4', 'd_gpsv5',
                                                   'd_gpsv7', 'd_sus'],
                                           aggfunc=np.sum)
        
        df_estabelecimentos = pd.merge(df_pivot_cnes, 
                                     df_pivot_services,
                                     on='chave')
        
        # for c in features:
        #     sns.lineplot(x="data", y=c,
        #              hue="cluster",
        #              data=df_merge_real)
        #     plt.show()
        
        df_estabelecimentos.to_csv(
            base_path + '/data_sus/intermediarias/estabelecimentos_sanitizada.csv')
                        
        return df_estabelecimentos
        
    def sanitiza_censo():
    
        df_censo_2010 = pd.read_csv(
            base_path + '/data_sus/input/setor_censitario/setor_bairro_ime.csv', 
            sep='|')
    
        df_censo_2010 = df_censo_2010.loc[:,]
    
    def sanitiza_populacao():
    
        df_populacao = pd.read_excel(
            base_path + '/data_sus/input/populacao/pop_RJ_ano_mun.xlsx')

    def sanitiza_precipatacao():
        
        df_temp_prec = pd.read_csv(
            base_path + '/data_sus/input/temperatura/temp_perc_day.csv')
        
        df_dict = pd.read_excel(
            base_path + '/data_sus/input/temperatura/dicionario-para-bairros.xlsx')
        
        df_dict = df_dict.loc[:,['Código do bairro', 'Nome bairro (base)']]
        
        df_temp_prec = pd.merge(df_temp_prec, df_dict,
                                left_on='bairro', right_on='Nome bairro (base)', 
                                how='left')
        
        df_temp_prec['chave_temp_prec'] = df_temp_prec[
            ['Código do bairro', 'year', 'month']].apply(
            lambda x: int(str(x[0]) + str(x[1]) + str(x[2])), axis=1) 
        
        df_temp_prec = df_temp_prec.loc[:,['chave_temp_prec', 'alstd', 'alstdi01', 
                                           'alstdi02', 'alstdi03', 'prec_month']]
        
        df_temp_prec.to_csv(
            base_path + '/data_sus/intermediarias/temp_prec_sanitizada.csv')
                
        return df_temp_prec
    
    df_basis = pd.read_csv(
        base_path + '/data_sus/intermediarias/sinasc_tuberculosis_agrup_bairro.csv')
    
    df_estabelecimentos = sanitiza_estabelecimento_mes()
    df_temp_prec = sanitiza_precipatacao()
        
    df_basis = pd.merge(df_basis, df_estabelecimentos,
                        on='chave', how='left')
    
    df_basis['cod_bairro_ime_temp_prec'] = df_basis['geo_tube_code_neighborhood'].apply(
        lambda x: int(str(x)[:-2]))
    
    df_basis['chave_temp_prec'] = df_basis[
        ['cod_bairro_ime_temp_prec', 'ano', 'mes']].apply(
            lambda x: int(str(x[0]) + str(x[1]) + str(x[2])), axis=1)
    
    df_basis = pd.merge(df_basis, df_temp_prec,
                        on='chave_temp_prec', 
                        how='left')
    
    # df_basis['cod_bairro_ime_censo'] = df_basis['geo_tube_code_neighborhood'].apply(
    #     lambda x: str(x)[:7] + str(x)[9:])
    
    df_basis.to_csv(
        base_path + '/data_sus/intermediarias/sinasc_tuberculosis_agrup_bairro.csv')
    

######################################
# Sanitiza base população            #
######################################

def sanitiza_populacao():
    
    '''
        - Primeiro foram pegos os dados do censo e gerado as % dos municipios 
        pelas regiões administrativas, de acordo com a aba 
        "Pop por bairro 2000-2010"
        
        - Das estimativas de população, as fontes foram utilizadas da seguinte 
        forma:
            - 2000, 2010 Censo e Data Rio
            - 2001 a 2009 e 2011 a 2020 SES/RJ
    '''
    
    df_pop_censo = pd.read_excel(
        base_path + '/data_sus/input/populacao/Resumo Pop.xlsx', 
        sheet_name="Pop por bairro 2000-2010")
    
    df_pop_censo_ra = pd.pivot_table(df_pop_censo,
                                   index=['Região Administrativa'],
                                   values=['População 2000', 'População 2010'],
                                   aggfunc=np.sum)
     
    df_pop_censo_ra.columns = ['População 2000_RA', 'População 2010_RA']
     
    df_pop_censo = pd.merge(df_pop_censo, df_pop_censo_ra,
                             on='Região Administrativa')
     
    df_pop_censo['% pop 2010'] = df_pop_censo[['População 2010',
                                                'População 2010_RA']].apply(
                                                    lambda x: x[0]/x[1],
                                                         axis=1)

    return df_pop_censo


######################################
# Sanitiza base de umidade e chuva   #
######################################

def sanitiza_umidade_chuva():
    
    df_umidade_chuva = pd.read_csv(base_path+\
            '/data_sus/input/umidade_pressao_ar/dados_ARJ_met_0313.csv', 
            sep="|")
    
    df_umidade_chuva_2 = pd.read_csv(base_path+\
            '/data_sus/input/umidade_pressao_ar/dados_ARJ_met_1417.csv',
            sep="|")
        
    df_umidade_chuva = df_umidade_chuva.append(df_umidade_chuva_2)
        
    for i in list(df_umidade_chuva.columns):
        if df_umidade_chuva[i].isna().sum() > (0.3*df_umidade_chuva.shape[0]):
            print(i)
            df_umidade_chuva.drop(columns=[i], inplace=True)
            
    df_umidade_chuva_pivot = pd.pivot_table(df_umidade_chuva, 
                                      index=['station','year', 'month'],
                                      values=['umidade'], aggfunc='mean',
                                      dropna=True)
    
    # df_umidade_chuva_pivot['station'] = list(
    #     df_umidade_chuva_pivot.index.get_level_values(0))
    df_umidade_chuva_pivot['year'] = list(
        df_umidade_chuva_pivot.index.get_level_values(1))
    df_umidade_chuva_pivot['month'] = list(
        df_umidade_chuva_pivot.index.get_level_values(2))
    
    df_estacoes_bairros = pd.read_excel(base_path+\
            '/data_sus/input/umidade_pressao_ar/De Para - Estacoes bairros.xlsx')
        
    df_estacoes_bairros = df_estacoes_bairros.loc[:,['Cod_bairro (12_dig)',
                                                     'Estação meteorológica']]
    
    station = {'Irajá': 'iraja', 'São Cristóvão':'sao_cristovao', 
               'Jardim Botânico':'jardim_botanico', 
               'Rio Centro':'riocentro', 'Santa Cruz':'santa_cruz', 
               'Guaratiba':'guaratiba',
               'Alto da Boa Vista':'alto_da_boa_vista'}
    
    df_estacoes_bairros['station'] = df_estacoes_bairros['Estação meteorológica'].apply(
        lambda x: np.nan if str(x) == 'nan' else station[x])
    
    df_merge = pd.merge(df_estacoes_bairros, 
                        df_umidade_chuva_pivot,
                        on='station')
    
    df_merge['chave'] = df_merge[['Cod_bairro (12_dig)',
            'year', 'month']].apply(
                lambda x: int(str(x[0])+str(x[1])+str(x[2])), axis=1)
                
    df_merge =  df_merge.loc[:,['chave', 'umidade']]
     
    df_merge.to_csv(
        base_path + '/data_sus/intermediarias/umidade_media.csv')


def sanitiza_temperatura_precipitacao_arqs_imnet():

    path = "/home/robson/Documents/Mestrado/Pesquisa/Dados/INMET_retiradas_site"
    arqs = os.listdir(path)

    def sanitiza_valor(t):

        if re.match("^,[0-9]{1,3}", str(t)):
            value = float("0" + str(t).replace(",", "."))
        else:
            value = float(str(t).replace(",", "."))

        return abs(value)

    df_result = pd.DataFrame(columns=['ano', 'mes', 'PRECIPITAÇÃO TOTAL, HORÁRIO (mm)',
       'TEMPERATURA DO AR - BULBO SECO, HORARIA (°C)',
       'UMIDADE RELATIVA DO AR, HORARIA (%)',
       'VENTO, VELOCIDADE HORARIA (m/s)'])

    for a in arqs:
        try:
            df_aux = pd.read_csv(path+"/"+a, encoding="latin-1", sep=";")
            df_aux = df_aux[['DATA (YYYY-MM-DD)', 'PRECIPITAÇÃO TOTAL, HORÁRIO (mm)',
                             'TEMPERATURA DO AR - BULBO SECO, HORARIA (°C)',
                             'VENTO, VELOCIDADE HORARIA (m/s)',
                             'UMIDADE RELATIVA DO AR, HORARIA (%)']]

            for c in list(df_aux.columns):
                if not c == 'DATA (YYYY-MM-DD)':
                    df_aux[c] = df_aux[c].apply(sanitiza_valor)
                df_aux = df_aux[~df_aux[c].isin([9999])]

            df_aux["ano"] = df_aux['DATA (YYYY-MM-DD)'].apply(lambda x: x[:4])
            df_aux["mes"] = df_aux['DATA (YYYY-MM-DD)'].apply(lambda x: x[5:7])

            df_aux_prec = df_aux.pivot_table(index=["ano", "mes"],
                                        values=['PRECIPITAÇÃO TOTAL, HORÁRIO (mm)'],
                                        aggfunc="sum")

            df_aux_prec = df_aux_prec.reset_index()
            df_aux_prec['PRECIPITAÇÃO TOTAL, HORÁRIO (mm)'] = df_aux_prec['PRECIPITAÇÃO TOTAL, HORÁRIO (mm)'].apply(
                                                                                            lambda x: round(x, 0))
            df_aux_prec["chave"] = df_aux_prec[["ano", "mes"]].apply(lambda x: str(x[0]) + str(x[1]), axis=1)
            df_aux_prec.drop(columns=["ano", "mes"], inplace=True)

            df_aux = df_aux.pivot_table(index=["ano", "mes"],
                                        values=['TEMPERATURA DO AR - BULBO SECO, HORARIA (°C)',
                                                'VENTO, VELOCIDADE HORARIA (m/s)',
                                                'UMIDADE RELATIVA DO AR, HORARIA (%)'],
                                        aggfunc="mean")
            for c in list(df_aux.columns):
                if not c == 'DATA (YYYY-MM-DD)':
                    df_aux[c] = df_aux[c].apply(lambda x: round(x, 0))

            df_aux = df_aux.reset_index()

            df_aux["chave"] = df_aux[["ano", "mes"]].apply(lambda x: str(x[0]) + str(x[1]), axis=1)

            df_aux = pd.merge(df_aux, df_aux_prec, on="chave")

            est = re.search("(VILA MILITAR)|(FORTE DE COPACABANA)", a).group()

            df_aux['Estacao'] = [est] * df_aux.shape[0]

            df_result = df_result.append(df_aux)

        except Exception as ex:
            embed()

    df_result.sort_values(by=['Estacao', "ano", "mes"], inplace=True)
    df_result = df_result[['Estacao', "ano", "mes", 'PRECIPITAÇÃO TOTAL, HORÁRIO (mm)',
                                                'TEMPERATURA DO AR - BULBO SECO, HORARIA (°C)',
                                                'VENTO, VELOCIDADE HORARIA (m/s)',
                                                'UMIDADE RELATIVA DO AR, HORARIA (%)']]

    return df_result


#####################################
#   Sanitiza IDH                    #
#####################################

def sanitiza_idh():

    df_idh = pd.read_excel(base_path+\
            '/data_sus/input/idh_bairros_rj/idh_bairros_rj.xlsx')

    df_idh['nome_bairro'] = df_idh['Bairro ou grupo de bairros'].apply(
        lambda x: sanitiza_string(x))

    df_input = pd.read_csv(
        base_path + \
        '/data_sus/intermediarias/Old/tuberculosis_temp_prec_reg_fillnas.csv')

    df_input['nome_bairro_adaptado'] = df_input['geo_tube_name_neighborhood'].apply(
        lambda x: sanitiza_string(x))

    df_input = df_input[['nome_bairro_adaptado', 'chave']]

    df_result = pd.merge(df_input, df_idh,
                         left_on='nome_bairro_adaptado',
                         right_on='nome_bairro',
                         how="left")

    df_result.drop(columns=['nome_bairro_adaptado', 'nome_bairro',
                                        'Bairro ou grupo de bairros'], inplace=True)

    for c in list(df_result.columns):
        if not c == "chave":
            df_result[c] = df_result[c].apply(
                lambda x: float(str(x).replace(r"\xa0", "").replace(",", ".")))

    df_result.to_csv(
        base_path + \
        '/data_sus/intermediarias/Old/idh_in_time.csv')

##########################################
# Base input de ocorrência de doenças    #
##########################################


def main():

    df_input = pd.read_csv(
        base_path+\
            '/data_sus/intermediarias/Old/tuberculosis_temp_prec_reg_fillnas.csv')
        
    df_dengue = pd.read_csv(
        base_path+\
            '/data_sus/intermediarias/Old/sinan_dengue_agrup_bairro_ano_mes.csv')

    df_idh = pd.read_csv(
        base_path + \
        '/data_sus/intermediarias/Old/idh_in_time.csv')

    df_dengue = df_dengue.loc[:,['chave', 'dengue_diagnosis']]
        
    df_umidade = pd.read_csv(
        base_path+\
            '/data_sus/intermediarias/Old/umidade_media.csv')

    df_umidade = df_umidade.loc[:,['chave', 'umidade']]
        
    df_input = pd.merge(df_input, df_dengue,
                        on='chave', how='left')

    df_input = pd.merge(df_input, df_umidade,
                        on='chave', how='left')

    df_input = pd.merge(df_input, df_idh,
                        on='chave', how='left')
    
    df_input.to_csv(
        base_path + '/data_sus/finais/input_ml_ocorrencia_com_idh.csv')
    

def main_direct_from_sourcer():

    def translate_variables(df_dengue_vila_militar, df_dengue_copacabana):

        # Transformando tempo para treinamento supervisionado
        for i in list(range(1, 13)):
            df_dengue_vila_militar['t-' + str(i)] = df_dengue_vila_militar['dengue_diagnosis'].shift(i)
            df_dengue_copacabana['t-' + str(i)] = df_dengue_copacabana['dengue_diagnosis'].shift(i)

        for i in list(range(1, 3)):
            df_dengue_vila_militar['temperatura (°C)-' + str(i)] = df_dengue_vila_militar['temperatura (°C)'].shift(i)
            df_dengue_vila_militar['precipitacao (mm)-' + str(i)] = df_dengue_vila_militar['precipitacao (mm)'].shift(i)
            df_dengue_vila_militar['velocidade vento (m/s)-' + str(i)] = df_dengue_vila_militar[
                'velocidade vento (m/s)'].shift(i)
            df_dengue_vila_militar['umidade ar (%)-' + str(i)] = df_dengue_vila_militar['umidade ar (%)'].shift(i)
            df_dengue_copacabana['temperatura (°C)-' + str(i)] = df_dengue_copacabana['temperatura (°C)'].shift(i)
            df_dengue_copacabana['precipitacao (mm)-' + str(i)] = df_dengue_copacabana['precipitacao (mm)'].shift(i)
            df_dengue_copacabana['velocidade vento (m/s)-' + str(i)] = df_dengue_copacabana[
                'velocidade vento (m/s)'].shift(i)
            df_dengue_copacabana['umidade ar (%)-' + str(i)] = df_dengue_copacabana['umidade ar (%)'].shift(i)

        df_dengue_vila_militar = df_dengue_vila_militar[~df_dengue_vila_militar['ano'].isin(
            [np.min(df_dengue_vila_militar['ano'])])]

        df_dengue_copacabana = df_dengue_copacabana[~df_dengue_copacabana['ano'].isin(
            [np.min(df_dengue_copacabana['ano'])])]

        return df_dengue_vila_militar, df_dengue_copacabana

    def put_neighbors(df_dengue_vila_militar, df_dengue_copacabana):
        '''
        Prepara a base para obter as informações dos vizinhos, insere o código dos bairros e
        atualiza os nomes para ficar igual ao que tem na base antiga
        '''
        df_cod = pd.read_excel(base_path + "/data_sus/finais/bairros_rio.xlsx")
        df_cod.index = list(df_cod['nome_bairro_source'])

        df_dengue_vila_militar["cod_bairro"] = df_dengue_vila_militar['nome_bairro'].apply(
            lambda x: int(df_cod.loc[x, ['cod_source']]['cod_source']))
        df_dengue_copacabana["cod_bairro"] = df_dengue_copacabana['nome_bairro'].apply(
            lambda x: int(df_cod.loc[x, ['cod_source']]['cod_source']))

        df_dengue_vila_militar["chave"] = df_dengue_vila_militar[['cod_bairro', "ano", "mes"]].apply(
            lambda x: int(str(int(x[0])) + str(int(x[1])) + str(int(x[2]))), axis=1)
        df_dengue_copacabana["chave"] = df_dengue_copacabana[['cod_bairro', "ano", "mes"]].apply(
            lambda x: int(str(int(x[0])) + str(int(x[1])) + str(int(x[2]))), axis=1)

        df_dengue_vila_militar['nome_bairro'] = df_dengue_vila_militar['nome_bairro'].apply(
            lambda x: df_cod.loc[x, ['nome_bairro_source_ajust']]['nome_bairro_source_ajust'])
        df_dengue_copacabana['nome_bairro'] = df_dengue_copacabana['nome_bairro'].apply(
            lambda x: df_cod.loc[x, ['nome_bairro_source_ajust']]['nome_bairro_source_ajust'])

        df_neighbors = sum_neighboors_to_prediction(df_dengue_vila_militar.copy(), ['sum_vizinhos_t-1',
                                                                                    'sum_vizinhos_t-3',
                                                                                    'sum_vizinhos_t-6'])
        for c in df_neighbors:
            df_dengue_vila_militar[c] = list(df_neighbors[c])
            df_dengue_copacabana[c] = list(df_neighbors[c])

        return df_dengue_vila_militar, df_dengue_copacabana

    df_dengue = sanitiza_dengue_bases_do_portal_rj()

    df_clima = sanitiza_temperatura_precipitacao_arqs_imnet()

    df_dengue['chave'] = df_dengue[['ano', 'mes']].apply(lambda x: str(x[0]) + str(x[1]),
                                                         axis=1)

    df_clima['chave'] = df_clima[['ano', 'mes']].apply(lambda x: str(x[0]) + str(int(x[1])),
                                                         axis=1)

    df_clima.drop(columns=['mes', 'ano'], inplace=True)

    df_dengue = pd.merge(df_dengue, df_clima, on="chave")

    df_dengue.sort_values(by=['Bairros', 'ano', 'mes'], inplace=True)

    df_dengue.columns = ['nome_bairro', 'Pop. Censo 2010', 'dengue_diagnosis', 'ano', 'mes', 'chave',
                            'Estacao', 'precipitacao (mm)',
                            'temperatura (°C)',
                            'velocidade vento (m/s)',
                            'umidade ar (%)']

    df_dengue_vila_militar = df_dengue[df_dengue['Estacao'] == 'VILA MILITAR']
    df_dengue_copacabana = df_dengue[df_dengue['Estacao'] == 'FORTE DE COPACABANA']

    df_dengue_vila_militar, df_dengue_copacabana = translate_variables(df_dengue_vila_militar, df_dengue_copacabana)

    df_dengue_vila_militar, df_dengue_copacabana = put_neighbors(df_dengue_vila_militar, df_dengue_copacabana)

    df_litoral = pd.read_excel(base_path + "/data_sus/finais/bairros_rio.xlsx")
    df_litoral = df_litoral[df_litoral['Litoral'] == "x"]

    df_dengue_vila_militar = df_dengue_vila_militar[~df_dengue_vila_militar['nome_bairro'].isin(
        list(df_litoral['nome_bairro_source_ajust'].unique()))]

    df_dengue_copacabana = df_dengue_copacabana[df_dengue_copacabana['nome_bairro'].isin(
        list(df_litoral['nome_bairro_source_ajust'].unique()))]

    df_dengue_origem = df_dengue_vila_militar.append(df_dengue_copacabana)

    df_dengue_vila_militar.to_csv(base_path + "/data_sus/finais/dengue_input_militar_from_source.csv")
    df_dengue_copacabana.to_csv(base_path + "/data_sus/finais/dengue_input_copacabana_from_source.csv")

    df_pop = df_dengue_origem[(df_dengue_origem["ano"] == 2012) & (df_dengue_origem["mes"] == 1)][['cod_bairro', "Pop. Censo 2010"]]
    df_pop.columns = ['cod_bairro', "Populacao"]
    df_dengue_origem = pd.merge(df_dengue_origem, df_pop, on="cod_bairro")
    df_dengue_origem.drop(columns=['Pop. Censo 2010'], inplace=True)

    df_dengue_origem.to_csv(base_path + "/data_sus/finais/dengue_input_from_source_v2.csv")


if __name__ == '__main__':

    df_influ = pd.read_csv(
        base_path + '/data_sus/intermediarias/sinan_influenza_agrup_bairro.csv')

    df_influ = df_influ[['chave', 'influenza_diagnosis']]

    df_input = pd.read_csv(
        base_path + '/data_sus/finais/input_ocorrencia_doencas.csv')

    df_input = df_input[["chave", 'tuberculosis_diagnosis',
       'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis',
       'syphilis_gestante_diagnosis']]

    df = pd.merge(df_input, df_influ,
                    on="chave", how="left")

    df_input_ori = pd.read_csv(base_path+\
            '/data_sus/finais/input_ml_ocorrencia_com_idh.csv')

    df_input_ori.drop(columns=['tuberculosis_diagnosis'], inplace=True)

    df = pd.merge(df_input_ori, df,
                    on="chave", how="left")

    df_min_max_neighbor = pd.read_csv(os.getcwd() +
                                      '/data_sus/finais/input_ml_ocorrencia_com_idh_min_max_neighboor.csv')

    df_min_max_neighbor = df_min_max_neighbor[['chave', 'min_vizinhos', 'max_vizinhos']]

    df = pd.merge(df, df_min_max_neighbor, on="chave", how="left")

    df.drop(columns=['Unnamed: 0', 'Unnamed: 0.1', 'cod_bairro_ime_temp_prec', 'chave_temp_prec',
                     'alstd', 'alstdi01', 'alstdi02'], inplace=True)

    df = df[['geo_tube_code_neighborhood', 'geo_tube_name_neighborhood', 'ano',
       'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2', 'd_gpsv4', 'd_gpsv5',
       'd_gpsv7', 'd_sus', 'alstdi03', 'prec_month',
       'umidade', 'min_vizinhos', 'max_vizinhos', 'Esperança de vida ao nascer (em anos)',
       'Taxa de alfabetização de adultos (%)',
       'Taxa bruta de frequência escolar (%)',
       'Renda per capita (em R$ de 2000)', 'Índice de Longevidade (IDH-L)',
       'Índice de Educação (IDH-E)', 'Índice de Renda (IDH-R)',
       'Índice de Desenvolvimento Humano (IDH)', 'tuberculosis_diagnosis',
       'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis',
       'syphilis_gestante_diagnosis', 'influenza_diagnosis', 'dengue_diagnosis']]

    df.columns = ['cod_bairro', 'nome_bairro', 'ano',
             'mes', 'chave', 'qtd_cnes', 'qtd_serv_atencao_basica', 'qtd_serv_ACS_program',
              'qtd_serv_prenatal', 'qtd_serv_neonatal',
             'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 'temp_solo', 'precipitacao',
             'umidade', 'min_vizinhos', 'max_vizinhos', 'Esperança de vida ao nascer (em anos)',
             'Taxa de alfabetização de adultos (%)',
             'Taxa bruta de frequência escolar (%)',
             'Renda per capita (em R$ de 2000)', 'IDH-L',
             'IDH-E', 'IDH-R', 'IDH', 'tuberculosis_diagnosis',
             'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis',
             'syphilis_gestante_diagnosis', 'influenza_diagnosis', 'dengue_diagnosis']

    df.to_csv(base_path+\
            '/data_sus/finais/input_ocorrencia_doencas_v2.csv')