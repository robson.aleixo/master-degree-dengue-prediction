import pandas as pd
import numpy as np
import re

SIM_DATASET_PATH = '/home/felipe/Documents/Datasets/SUS/dados_sus_ime/dados_sus_ime/sim_ime.csv'
SINASC_DATASET_PATH = '/home/felipe/Documents/Datasets/SUS/dados_sus_ime/dados_sus_ime/sinasc_ime.csv'

##############################################
#           SINASC DATABASE                  #
##############################################
 
# Read the entire csv file considering only desired columns
cols_sinasc = list(pd.read_csv(SINASC_DATASET_PATH, nrows =1))
columns_to_drop_sinasc = ['horanasc', 'tpmetestim', 'sttrabpart', 'tpnascassi',
                          'fill_tpfuncresp', 'fill_codprof', 'fill_orgemissor',
                          'fill_dtdeclarac', 'adm_numerolote', 'adm_dtcadastro']
keep_cols_sinasc = [name for name in cols_sinasc if name not in columns_to_drop_sinasc]
df_sinasc = pd.read_csv(SINASC_DATASET_PATH, usecols = keep_cols_sinasc)

# Add binary columns based on anomaly codes from ICD10
# http://www.datasus.gov.br/cid10/V2008/WebHelp/cap17_3d.htm
# For every QXX code, where XX ranges from 00 to 99,
# it creates a new column to every code and assign a
# to it 0. The code D18 is also considered.
list_anomaly_column_name = ['D18']
df_sinasc['D18'] = 0

for i in range(100):
    if  i < 10:
        new_column = 'Q0' + str(i)
    else:
        new_column = 'Q' + str(i)
    list_anomaly_column_name.append(new_column)
    df_sinasc[new_column] = 0

ICD10_ANOMALY_CODE_REGEX = '(?:Q|D)[0-9][0-9]'
indexes = df_sinasc['codanomal'][df_sinasc['codanomal'].notna()].index

for index in indexes:
    icd_code = df_sinasc['codanomal'].iloc[index]
    for code in re.findall(ICD10_ANOMALY_CODE_REGEX, icd_code):
        if code in list_anomaly_column_name:
            df_sinasc.at[index, code] = 1

##############################################
#           SIM DATABASE                     #
##############################################
cols_sim = list(pd.read_csv(SIM_DATASET_PATH, nrows =1))
columns_to_drop_sim = ['female', 'esc2010', 'seriescfal', 'escfalagr2',
                       'escfalagr1', 'invest_obit', 'invest_dt', 'invest_fonte',
                       'med_atestante', 'med_comunsvoim', 'med_dtatestado',
                       'cid_causabas_regra', 'capcid_causabas', 'capcid_causaori',
                       'gpcid_causabas' 'gpcid_causaori']

keep_cols_sim = [name for name in cols_sim if name not in columns_to_drop_sim]
df_sim = pd.read_csv(SIM_DATASET_PATH, usecols = keep_cols_sim)
#df_sim = df_sim[df_sim['ano'] > 2014]

# Drops columns that are entirely null
df_sim.dropna(axis=1, how='all', inplace=True)

# Filter for occurrences of Syphilis
# These filters the rows in which the patitent
# died from Syphilis.
# The codes are based on
# http://www.datasus.gov.br/cid10/V2008/WebHelp/cap01_3d.htm
# ICD10_SYPHILIS_CODE_REGEX = 'A5[0-3]*'
# syphilis_filter = df_sim['icd10'].fillna('')o.str.contains(icd10_syphilis_code_pattern, regex=True)
# syphilis_filter = syphilis_filter | df_sim['cid_causabas'].fillna('').str.contains(ICD10_SYPHILIS_CODE_REGEX, regex=True)
# syphilis_filter = syphilis_filter | df_sim['cid_causaori'].fillna('').str.contains(ICD10_SYPHILIS_CODE_REGEX, regex=True)
#
# for ch in ['a', 'b', 'c', 'd', 'ii']:
#     syphilis_filter = syphilis_filter | df_sim['linha' + ch].fillna('').str.contains(ICD10_SYPHILIS_CODE_REGEX, regex=True)
#     syphilis_filter = syphilis_filter | df_sim['linha' + ch + '_o'].fillna('').str.contains(ICD10_SYPHILIS_CODE_REGEX, regex=True)
#
# df_sim = df_sim[syphilis_filter]

