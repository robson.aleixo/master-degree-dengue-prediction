import pandas as pd
import matplotlib.pyplot as plt
from shapely.geometry import Polygon
from shapely.geometry import MultiPolygon
import numpy as np
import seaborn as sns
import os
from datetime import date
import json
import geopandas as gpd
import geoplot
import mapclassify as mc
import ppscore as pps

base_path = os.getcwd()

from machine_learning.mlMethods import *
from machine_learning.machine_learning_ocorrencia_doenca import *
from machine_learning.measuringResults import *
from machine_learning.visualization import *
from machine_learning.measuring_r2 import *
from lightgbm import LGBMClassifier
from catboost import CatBoostClassifier
from sklearn.metrics import classification_report, roc_curve, roc_auc_score, f1_score
from sklearn import metrics

'''
##### Regression ####
'''
def run_main_first_step_1(target, ano_teste, sample=False, n=0):
    '''
    Step 1 of main process: prepare train and test bases
    '''
    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_taxa_v3.csv")
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_v3.csv")

    columns_filtered = ['qtd_cnes',
                        'mes',
                        'precipitacao (mm)-1',
                        'temperatura (°C)-1',
                        'umidade ar (%)-2',
                        'sum_vizinhos_t-1',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6']

    categorical_columns = ['mes']

    columns_filtered_categorical = ['mes_1', 'mes_2', 'mes_3', 'mes_4']

    neighbor_columns = ['sum_vizinhos_t-1']

    df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=ano_teste, mes_teste=12, ano_treino=3)

    X_train, y_train = prepare_base_treino(df_dengue_origem, columns_filtered, columns_filtered_categorical,
                            categorical_columns, target_variable=target)

    if sample:
        X_train_sample, X_test_sample, y_train_sample, y_test_sample = train_test_split(X_train,
                                                                                        y_train,
                                                                                        test_size=0.1,
                                                                                        random_state=n)
    else:
        X_train_sample = X_train.copy()
        y_train_sample = y_train.copy()

    return df_treino, df_teste, X_train_sample, y_train_sample, columns_filtered, categorical_columns, \
           columns_filtered_categorical, neighbor_columns


def run_main_first_step_2(df_teste, X_train, y_train, columns_filtered, categorical_columns,
           columns_filtered_categorical, neighbor_columns, target, m):
    '''
    Step 2 of main process: training model and predicting values
    '''

    if m in ["randomforest", "extratrees", "xgboost", "lightGBM"]:
        regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)
        scaler = False
    else:
        regressor, scaler, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

    df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                               columns_filtered, columns_filtered_categorical,
                                                               list(X_train.columns), categorical_columns,
                                                               neighbor_columns, target_variable=target,
                                                               scaler=scaler)

    df_teste = pd.merge(df_teste, df_output_model[["chave", target+'_previsto']], on="chave", how="left")

    return df_teste


def comparacao():

    df_from_source = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")
    df_dengue_antigo = read_and_get_input_v2(base_path+ \
                                     '/data_sus/finais/Inputs/Old version/input_ml_ocorrencia_doencas_v6.csv')

    df_from_source = df_from_source.pivot_table(index=["ano", "mes"], values=["dengue_diagnosis"], aggfunc="sum")
    df_antiga = df_dengue_antigo.pivot_table(index=["ano", "mes"], values=["dengue_diagnosis"], aggfunc="sum")

    df_from_source = df_from_source.reset_index()
    df_from_source["data"] = df_from_source[["ano", "mes"]].apply(lambda x: date(x[0], x[1], 1), axis=1)
    df_from_source = df_from_source[df_from_source["ano"] < 2018]

    plot_two_lines_v2(list(df_from_source['dengue_diagnosis']), list(df_antiga['dengue_diagnosis']),
                   list(df_from_source["data"]))


def read_and_prepare_v3(df_input, ano_teste=2017, mes_teste=12, ano_treino=3):

    df_treino = df_input[df_input['ano'].isin(
        list(range(ano_teste - ano_treino, ano_teste, 1)))]
    df_teste = df_input[df_input['ano'].isin(
        list(range(ano_teste, ano_teste +1, 1)))]
    df_teste = df_teste[df_input['mes'].isin(
        list(range(1, mes_teste+1, 1)))]

    return df_treino, df_teste


def main_old():

    target = 'dengue_diagnosis'
    target = "taxa_dengue"

    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")

    df_dengue_origem["taxa_dengue"] = df_dengue_origem[['dengue_diagnosis', 'Populacao']].apply(
                                                                        lambda x: round((x[0]/x[1])*100, 2), axis=1)

    columns_filtered = ['Populacao', 'mes',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                        # 't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                        'temperatura (°C)-1', 'precipitacao (mm)-1',
                        'umidade ar (%)-2', 'sum_vizinhos_t-1']

    categorical_columns = ['mes']

    columns_filtered_categorical = ['mes_3']

    neighbor_columns = ['sum_vizinhos_t-1']

    df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=2019, mes_teste=12, ano_treino=3)

    X_train, y_train = prepare_base_treino(df_dengue_origem, columns_filtered, columns_filtered_categorical,
                            categorical_columns, target_variable=target)

    regressor, r2 = run_lightGBM(X_train, y_train, grid_search=False)

    df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                               columns_filtered, columns_filtered_categorical,
                                                               list(X_train.columns), categorical_columns,
                                                               neighbor_columns, target_variable=target)

    df_aux = df_output_model[['chave', target + '_previsto']]
    df_teste = pd.merge(df_teste, df_aux, on='chave', how='left')
    df_teste['diff'] = df_teste[[target, target+'_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    df_variance_each_city = apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(
        df_treino, df_teste, target_variable=target)

    df_feature_importance = apply_shap_values(X_train, regressor, "lightGBM", base_path)


def r2_score_ajustado(df_treino, df_teste, df_output_model, target_variable='dengue_diagnosis'):

    df_aux = df_output_model[['chave', target_variable+'_previsto']]
    df_teste = pd.merge(df_teste, df_aux, on='chave', how='left')
    df_teste['diff'] = df_teste[[target_variable, target_variable+'_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    df_variance_each_city = apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(
        df_treino, df_teste, target_variable)

    return np.mean(df_variance_each_city['r2'])


def r2_score_padrao(df_teste, df_output_model, target_variable='dengue_diagnosis'):

    df_r2_padrao = apply_r2_standart_for_each_city_test_data(df_output_model, df_teste, target_variable)

    return np.mean(df_r2_padrao['r2'])


def outbreak_validation(df_dengue_origem, df_teste, df_output_model, title, target_variable="dengue_diagnosis"):

    outbreak_value = np.percentile(list(df_dengue_origem[target_variable]), 80)

    outbreak_real = list()
    outbreak_previsto = list()

    for r in list(df_teste['mes'].unique()):
        list_r = df_teste[df_teste['mes'] == r][target_variable]
        list_f = df_output_model[df_output_model['mes'] == r][target_variable+"_previsto"]
        r_aux = [1 if n >= outbreak_value else 0 for n in list_r]
        f_aux = [1 if n >= outbreak_value else 0 for n in list_f]
        outbreak_real.append(np.sum(r_aux))
        outbreak_previsto.append(np.sum(f_aux))

    df_outbreak = pd.DataFrame(outbreak_real, columns=['Valor'])
    df_outbreak_aux = pd.DataFrame(outbreak_previsto, columns=['Valor'])
    df_outbreak['Categoria'] = ["Real"] * int(df_teste['mes'].nunique())
    df_outbreak['Mês'] = list(range(1, int(df_teste['mes'].nunique())+1))
    df_outbreak_aux['Categoria'] = ["Previsto"] * int(df_teste['mes'].nunique())
    df_outbreak_aux['Mês'] = list(range(1, int(df_teste['mes'].nunique())+1))
    df_outbreak = df_outbreak.append(df_outbreak_aux)

    # f, ax = plt.subplots(figsize=(15, 6))
    # sns.set_context(font_scale=0.9)
    # sns.lineplot(
    #     data=df_outbreak, x="Mês", y="Valor", hue="Categoria", linewidth=5)
    # plt.ylabel("Número de Surtos")
    # # plt.ylim(0, 100)
    # plt.title(title)
    # plt.show()

    return df_outbreak


def avalia_anos():

    ano = list(range(2013, 2021))
    # ano = [2017, 2018]

    columns_filtered = ['Populacao',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                        # 't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                        'temperatura (°C)-1', 'precipitacao (mm)-1',
                        'umidade ar (%)-2', 'sum_vizinhos_t-1']

    categorical_columns = []

    columns_filtered_categorical = []

    neighbor_columns = ['sum_vizinhos_t-1']

    target = 'dengue_diagnosis'
    target = "taxa_dengue"

    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")

    df_dengue_origem["taxa_dengue"] = df_dengue_origem[['dengue_diagnosis', 'Populacao']].apply(
                                                                        lambda x: round((x[0]/x[1])*100, 2), axis=1)

    if target == "taxa_dengue":

        df_dengue_origem.drop(columns=['t-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                                       't-7', 't-8', 't-9', 't-10', 't-11', 't-12'], inplace=True)

        for i in list(range(1, 13)):
            df_dengue_origem['t-' + str(i)] = df_dengue_origem["taxa_dengue"].shift(i)

        df_neighbors = sum_neighboors_to_prediction(df_dengue_origem.copy(), neighbor_columns, round=False)
        for n in neighbor_columns:
            df_dengue_origem[n] = list(df_neighbors[n])

    # df_dengue_origem["campanha"] = df_dengue_origem["ano"].apply(
    #     lambda x: 1 if int(x) in [2014, 2015, 2017, 2018] else 0)
    
    columns_result = ['ano teste', 'anos treino', 'r2_treino', 'r2_padrao', 'r2_ajustado']
    df_result = pd.DataFrame(columns=columns_result)
    df_result_outbreak = pd.DataFrame(columns=['Valor', 'Categoria', 'Mês', 'ano_teste', 'ano_treino'])

    for a in ano:
        for p in list(range(3, 4)):
            if a - p < 2012:
                continue

            df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=a, mes_teste=12, ano_treino=p)

            X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                                categorical_columns, target_variable=target)

            regressor, r2 = run_lightGBM(X_train, y_train, grid_search=False)

            df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                                       columns_filtered, columns_filtered_categorical,
                                                                       list(X_train.columns), categorical_columns,
                                                                       neighbor_columns, target_variable=target)

            r2_padrao = r2_score_padrao(df_teste, df_output_model, target_variable=target)
            r2_ajustado = r2_score_ajustado(df_treino, df_teste, df_output_model, target_variable=target)

            aux = pd.DataFrame([[a, p, r2, r2_padrao, r2_ajustado]], columns=columns_result)

            df_result = df_result.append(aux)

            # plot_one_neighbor(df_teste, df_output_model, "{} (treino {} anos)".format(a, p))
            df_out = outbreak_validation(df_dengue_origem, df_teste, df_output_model, "{} (treino {} anos)".format(a, p),
                                target_variable=target)

            df_out["ano_teste"] = [a] * df_out.shape[0]
            df_out["ano_treino"] = [p] * df_out.shape[0]

            df_result_outbreak = df_result_outbreak.append(df_out)

    df_result_outbreak.to_csv(base_path+"/data_sus/finais/analise_outbreak_geral.csv")
    df_result_outbreak = pd.read_csv(base_path+"/data_sus/finais/analise_outbreak_geral.csv")


def plot_outbreak(df_result_outbreak):

    df_outbreak = df_result_outbreak[(df_result_outbreak["ano_treino"] == 3)]

    df_outbreak = df_result_outbreak.pivot_table(index=["Mês", "Categoria"], values=["Valor"], aggfunc="sum")

    df_outbreak = df_outbreak.pivot_table(index=["Mês", "Categoria"], values=["Valor"], aggfunc="sum")
    df_outbreak = df_outbreak.reset_index()

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.lineplot(
        data=df_outbreak, x="Mês", y="Valor", hue="Categoria", linewidth=5)
    plt.ylabel("Número de Surtos")
    plt.ylim(0, 160)
    plt.title("Analise Surto Geral 3 anos treino (dengue taxa)")
    plt.show()


def r2_in_time(m, df_treino, df_teste, columns_filtered, categorical_columns,
               neighbor_columns, columns_filtered_categorical):

    df_r2_time = analyse_r2_by_time_v3(m, df_treino, df_teste,
                       columns_filtered, categorical_columns,
                       columns_filtered_categorical, neighbor_columns)

    df_r2_time.to_csv(base_path + \
                          '/data_sus/finais/r2_por_trimestre_{}.csv'.format(m))

def feature_importance_analysis():

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'

    ano = list(range(2015, 2021))
    models = ['randomforest', "extratrees", "xgboost", "lightGBM"]

    df_resultado = pd.DataFrame(
        columns=['feature', 'importance', 'importance_perc', 'model', 'ano'])

    for m in models:
        for a in ano:
            df_treino, df_teste, X_train, y_train, columns_filtered, categorical_columns, \
            columns_filtered_categorical, neighbor_columns = run_main_first_step_1(target, 2019)

            regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

            df_features_importance = apply_shap_values(X_train, regressor, m, base_path)

            df_features_importance['model'] = [m] * df_features_importance.shape[0]
            df_features_importance['ano'] = [a] * df_features_importance.shape[0]

            df_resultado = df_resultado.append(df_features_importance)

    df_resultado.to_csv(base_path +
            '/data_sus/finais/feature_importances/feature_importances_consolidado_base_atualizada.csv')


####################
#   Visualization  #
####################

def plot_maps(df_values, title, l):

    f = open(base_path+"/Bairros.json", )
    maps = json.load(f)
    df_maps = pd.DataFrame(columns=['nome', 'geometry'])
    df_maps["nome"] = df_maps["nome"].apply(lambda x: x.title())

    for m in maps['features']:
        try:
            if m['geometry']['type'].upper() == "POLYGON":
                geo = Polygon(m['geometry']['coordinates'][0])
            elif  m['geometry']['type'].upper() == "MULTIPOLYGON":
                polygons = [Polygon(x[0]) for x in m['geometry']['coordinates']]
                geo = MultiPolygon(polygons)

            nome = m['properties']['NM_BAIRRO']
            df = pd.DataFrame([[nome, geo]], columns=['nome', 'geometry'])
            df_maps = df_maps.append(df)
        except Exception as ex:
            embed()

    df_maps_a = pd.merge(df_maps, df_values,
                       left_on='nome', right_on='Bairro', how='left')

    gpd_maps = gpd.GeoDataFrame(df_maps)

    scheme = mc.FisherJenks(np.round(gpd_maps[title].astype('float64'), 2), k=10)
    scheme = mc.EqualInterval(np.round(gpd_maps[title].astype('float64'), 2), k=10)

    geoplot.choropleth(
        gpd_maps, hue=title,
        edgecolor='gray', linewidth=1,
        cmap='Greens', figsize=(12, 10),
        legend=True, scheme=scheme
    )
    plt.title(title + " (meses futuro {})".format(l))
    plt.show()


def plot_one_neighbor(df_teste, df_output_model, title, neighbor="Santa Cruz"):

    df_teste['data'] = df_teste[['ano', 'mes']].apply(lambda x: date(x[0], x[1], 1), axis=1)

    df_real = df_teste[df_teste['nome_bairro'] == neighbor]
    df_prev = df_output_model[df_output_model['nome_bairro'] == neighbor]

    plot_two_lines_v2(list1=list(df_real["dengue_diagnosis"]),
                      list2=list(df_prev["dengue_diagnosis_previsto"]),
                      listX=list(df_real['data']), labelList1="Real", labelList2="Previsto",
                      ylabel="Número de casos de Dengue", title="{} - {}".format(neighbor, title))


def plot_r2_in_time():
    '''
    This method will plot the assessment of R² in time, considering years from 2016 until 2020
    :return:
    '''

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'

    m = "lightGBM"

    ano = list(range(2015, 2021))

    df_consolidate = pd.DataFrame(columns=["Time Prediction (Months)", "Model", "R2", "ano"])

    for a in ano:

        df_treino, df_teste, X_train, y_train, columns_filtered, categorical_columns, \
        columns_filtered_categorical, neighbor_columns = run_main_first_step_1(target, a)

        df = analyse_r2_by_time_v3(m, df_treino, df_teste,
                              columns_filtered, categorical_columns,
                              columns_filtered_categorical, neighbor_columns, target)

        df['ano'] = [a] * df.shape[0]

        df_consolidate = df_consolidate.append(df)

    f, ax = plt.subplots(figsize=(12, 6))
    sns.set_context(font_scale=10)
    sns.boxplot(x='Time Prediction (Months)', y='R2',
                data=df_consolidate)
    # sns.despine(offset=10, trim=True)
    plt.ylim(-0.2, 1.1)
    plt.show()


def each_neighboor_analysis_r2():

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'

    m = "lightGBM"

    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_taxa_v2.csv")
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_v3.csv")

    ano = list(range(2015, 2021))

    df_consolidate_r2_each_neighbor = pd.DataFrame(columns=["city", "r2", "ano"])
    df_consolidate_teste = pd.DataFrame(columns=['chave', 'nome_bairro', 'ano', 'mes',
                                                 'taxa_dengue', 'taxa_dengue_previsto'])

    for a in ano:
        df_treino, df_teste, X_train, y_train, columns_filtered, categorical_columns, \
        columns_filtered_categorical, neighbor_columns = run_main_first_step_1(target, a)

        df_teste = run_main_first_step_2(df_teste, X_train, y_train, columns_filtered, categorical_columns,
                              columns_filtered_categorical, neighbor_columns, target, m)

        df_consolidate_teste = df_consolidate_teste.append(df_teste[['chave', 'nome_bairro', 'ano', 'mes',
                                                 'taxa_dengue', 'taxa_dengue_previsto']])

        df_variance_each_city = apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(
            df_treino, df_teste, target)

        df_variance_each_city["ano"] = [a] * df_variance_each_city.shape[0]

        df_consolidate_r2_each_neighbor = df_consolidate_r2_each_neighbor.append(df_variance_each_city)

    df_pivot = df_consolidate_r2_each_neighbor.pivot_table(index=['city'], values=['r2'], aggfunc=np.mean)

    df = df_dengue_origem.pivot_table(index=['nome_bairro'], values=['Populacao'], aggfunc=np.mean)

    df_pivot = df_pivot.reset_index()
    df = df.reset_index()

    df_pivot = pd.merge(df_pivot, df,
                        left_on='city', right_on='nome_bairro', how='left')

    df_pivot['Populacao'] = df_pivot['Populacao'].apply(lambda x: x/1000)

    f, ax = plt.subplots(figsize=(30, 20))
    sns.set(style="whitegrid", font_scale=1)
    sns.displot(df_consolidate_r2_each_neighbor, x="r2", bins=100)
    plt.show()

    f, ax = plt.subplots(figsize=(12, 6))
    sns.set(style="white", font_scale=1)
    sns.relplot(
        data=df_pivot,
        x="Populacao", y="r2"
    )
    plt.show()


    plt.figure(figsize=(30, 20))
    sns.boxplot(data=df_consolidate_r2_each_neighbor, y="r2")
    plt.show()


def all_base_analysis_r2():

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'

    models = ['randomforest', "extratrees", "xgboost", "lightGBM", 'svr', "linear_regression", "polynomial_regression"]

    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_taxa_v3.csv")
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_v3.csv")

    ano = list(range(2015, 2021))

    df_consolidate_teste = pd.DataFrame(columns=['chave', 'nome_bairro', 'ano', 'mes', 'taxa_dengue', 'taxa_dengue_previsto'])

    df_metrics = pd.DataFrame(columns=["Modelo", "R2", "MAE", "RMSE"])

    for m in models:

        try:
            for i in list(range(10)):
                for a in ano:
                    df_treino, df_teste, X_train, y_train, columns_filtered, categorical_columns, \
                    columns_filtered_categorical, neighbor_columns = run_main_first_step_1(target, a, sample=True, n=i)

                    df_teste = run_main_first_step_2(df_teste, X_train, y_train, columns_filtered, categorical_columns,
                                          columns_filtered_categorical, neighbor_columns, target, m)

                    df_consolidate_teste = df_consolidate_teste.append(df_teste[['chave', 'nome_bairro', 'ano', 'mes',
                                                             'taxa_dengue', 'taxa_dengue_previsto']])

                df = apply_adjusted_r2_all_base(df_dengue_origem[['chave', 'nome_bairro', 'ano', 'mes',
                                                         'taxa_dengue']], df_consolidate_teste, target)
                df["Modelo"] = [m] * df.shape[0]
                df_metrics = df_metrics.append(df)
        except Exception as ex:
            continue

        df_metrics.to_csv(base_path+\
                          "/data_sus/finais/Resultados/Regressao/Resultado_base_total/resultado_total_{}.csv".format(m))



    f, ax = plt.subplots(figsize=(30, 20))
    sns.set(style="whitegrid", font_scale=1)
    sns.displot(df_consolidate_r2_each_neighbor, x="r2", bins=100)
    plt.show()

    f, ax = plt.subplots(figsize=(12, 6))
    sns.set(style="white", font_scale=1)
    sns.relplot(
        data=df_pivot,
        x="Populacao", y="r2"
    )
    plt.show()


    plt.figure(figsize=(30, 20))
    sns.boxplot(data=df_consolidate_r2_each_neighbor, y="r2")
    plt.show()


def pp_score_analysis():

    target = "taxa_dengue"
    # target = "dengue_diagnosis"

    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_taxa_v2.csv")
        df = df_dengue_origem.drop(columns=['Unnamed: 0', 'nome_bairro', 'dengue_diagnosis', 'ano', 'mes', 'chave',
                                            'Estacao', 'cod_bairro'])
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_v3.csv")
        df = df_dengue_origem.drop(columns=['Unnamed: 0', 'nome_bairro','ano', 'mes', 'chave',
                                            'Estacao', 'cod_bairro'])

    df_pps = pps.matrix(df)

    df_pps_all = df_pps[(df_pps["ppscore"] >= 0.3) & (-df_pps["case"].isin(["predict_itself"]))]

    df_pps_target = df_pps[(df_pps["y"] == target) & (-df_pps["case"].isin(["predict_itself"]))]


'''
##### Classification ####
'''
def main_classificacao_analise_por_valor_taxa(percentil, peso, method="lightGBM"):
    '''
    Este método faz a classificação por taxa de dengue ((# casos / população) * 100)
    Importante saber que os valores de t-n e sum_vizinhos precisam ser alterados para terem o valor de taxas
    :return:
    '''

    # percentil = 75
    # peso = 1

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'
    ano = list(range(2015, 2021))

    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source_taxa.csv")
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")

    columns_filtered = ['Populacao',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                        't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                        'temperatura (°C)-1', 'precipitacao (mm)-1',
                        'umidade ar (%)-2', 'sum_vizinhos_t-1']

    categorical_columns = []

    columns_filtered_categorical = []

    threshold = np.percentile(list(df_dengue_origem[target]), percentil)
    df_dengue_origem["outbreak"] = df_dengue_origem[target].apply(
        lambda x: 1 if x > threshold else 0)

    df_result = pd.DataFrame(columns=['Bairro', 'ano', 'mes', "meses futuro", "outbreak_real",
                                      "outbreak_previsto"])

    for a in ano:

        df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=a, mes_teste=12, ano_treino=3)

        X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        X_teste, y_teste = prepare_base_treino(df_teste, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        for p in list(range(0, 7)):

            if not p == 0:
                X_train.drop(columns=["t-{}".format(p)], inplace=True)
                X_teste.drop(columns=["t-{}".format(p)], inplace=True)

            if method == "lightGBM":
                auc, classifier = run_lightGBM_classifier(X_train, y_train, p=peso)
            elif method == "catboost":
                auc, classifier = run_catboost_classifier(X_train, y_train, p=peso)

            y_pred = classifier.predict(X_teste)
            y_pred = [round(y, 2) for y in y_pred]

            df_aux = pd.DataFrame(
                columns=['Bairro', 'ano', 'mes', "meses futuro", "outbreak_real", "outbreak_previsto"])

            df_aux['Bairro'] = df_teste['nome_bairro']
            df_aux['ano'] = df_teste['ano']
            df_aux['mes'] = df_teste['mes']
            df_aux['meses futuro'] = [p] * df_teste.shape[0]
            df_aux["outbreak_real"] = df_teste["outbreak"]
            df_aux["outbreak_previsto"] = list(y_pred)

            df_result = df_result.append(df_aux)

    df_result["outbreak_real"] = df_result["outbreak_real"].apply(lambda x: float(x))
    df_result["outbreak_previsto"] = df_result["outbreak_previsto"].apply(lambda x: float(x))

    df_result.to_csv(base_path + \
        "/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_{}_peso{}_{}.csv".format(
            percentil, peso, method))

    return df_result


def apply_cross_validate_classification(X_train, y_train, classifier, model_name):

    scoring = {'precision': 'precision',
               'recall': 'recall',
               'f1': 'f1',
               'accuracy': 'accuracy',
               'roc_auc': 'roc_auc'}

    accuracies = cross_validate(estimator=classifier,
                                X=X_train,
                                y=y_train,
                                scoring=scoring,
                                cv=10)

    print('######################### {} ###########################'.format(model_name))
    print('Acurácia: ({}, {})'.format(round(accuracies['test_accuracy'].mean(), 2),
                                      round(accuracies['test_accuracy'].std(), 2)))
    print('Precisão: ({}, {})'.format(round(accuracies['test_precision'].mean(), 2),
                                      round(accuracies['test_precision'].std(), 2)))
    print('Cobertura: ({}, {})'.format(round(accuracies['test_recall'].mean(), 2),
                                       round(accuracies['test_recall'].std(), 2)))
    print('Métrica-F1: ({}, {})'.format(round(accuracies['test_f1'].mean(), 2),
                                        round(accuracies['test_f1'].std(), 2)))
    print('ROC AUC: ({}, {})'.format(round(accuracies['test_roc_auc'].mean(), 2),
                                        round(accuracies['test_roc_auc'].std(), 2)))

    return round(accuracies['test_roc_auc'].mean(), 2)


def run_lightGBM_classifier(X_train, y_train, p=1, is_unbalance=False):

    if is_unbalance:
        classifier = LGBMClassifier(n_estimators=100,
                                    boosting='dart',
                                    bagging_freq=1,
                                    feature_fraction=0.7,
                                    random_state=0,
                                    is_unbalance=is_unbalance)
    else:
        classifier = LGBMClassifier(n_estimators=100,
                                    boosting='dart',
                                    bagging_freq=1,
                                    feature_fraction=0.7,
                                    random_state=0,
                                    scale_pos_weight=p)

    auc = apply_cross_validate_classification(X_train, y_train, classifier, 'lightGBM')

    classifier.fit(X_train, y_train)

    return auc, classifier


def run_catboost_classifier(X_train, y_train, p=1, auto_class_weights=False):

    '''
    ######################### Catboost ###########################
    Acurácia: (0.92, 0.01)
    Precisão: (0.86, 0.02)
    Cobertura: (0.81, 0.04)
    Métrica - F1: (0.83, 0.02)
    ROC AUC: (0.97, 0.01)
    '''

    if not auto_class_weights:

        classifier = CatBoostClassifier(class_weights=[1, p], random_state=0)

    else:

        classifier = CatBoostClassifier(auto_class_weights="Balanced", random_state=0)


    # auc = apply_cross_validate_classification(X_train, y_train, classifier, 'Catboost')

    classifier.fit(X_train, y_train)

    return 0, classifier


def calc_auc_test(y_teste, y_pred):

    #Precisa dessa verificação pois da erro para o caso de bairros que possui todos os valores zerados
    if 1 in y_teste:
        if 1 in y_pred:
            fpr, tpr, thresholds = roc_curve(y_teste, y_pred, pos_label=2)
            res = metrics.auc(fpr, tpr)
        else:
            res = 0
    else:
        # Se os valores reais e previstos forem todos iguais a zero
        if not 1 in y_pred:
            res = 1
        else:
            res = 0

    # fpr, tpr, thresholds = roc_curve(y_teste, y_pred, pos_label=2)
    # res = metrics.auc(fpr, tpr)

    return res


def teste_classificacao():

    target = 'dengue_diagnosis'
    target = "taxa_dengue"
    ano = list(range(2015, 2021))

    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")
    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source_taxa.csv")

    threshold = np.percentile(list(df_dengue_origem[target]), 80)
    df_dengue_origem["outbreak"] = df_dengue_origem[target].apply(
        lambda x: 1 if x > threshold else 0)

    columns_filtered = ['Populacao',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                        't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                        'temperatura (°C)-1', 'precipitacao (mm)-1',
                        'umidade ar (%)-2', 'sum_vizinhos_t-1']

    categorical_columns = []

    columns_filtered_categorical = []

    df_result = pd.DataFrame(columns=['ano teste', 'mes passado retirado', 'auc treino', 'auc teste'])

    for a in ano:

        df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=a, mes_teste=12, ano_treino=3)

        X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        X_teste, y_teste = prepare_base_treino(df_teste, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        for p in list(range(0, 7)):

            if not p == 0:
                X_train.drop(columns=["t-{}".format(p)], inplace=True)
                X_teste.drop(columns=["t-{}".format(p)], inplace=True)

            print(X_train.columns)

            auc, classifier = run_lightGBM_classifier(X_train, y_train)

            y_pred = classifier.predict_proba(X_teste)
            y_pred = [v[1] for v in y_pred]

            df_inter = pd.DataFrame([[a, p, auc, roc_auc_score(np.array(y_teste), np.array(y_pred))]],
                                    columns=['ano teste', 'mes passado retirado', 'auc treino', 'auc teste'])

            df_result = df_result.append(df_inter)

    f, ax = plt.subplots(figsize=(15, 6))
    sns.boxplot(x='mes passado retirado', y='auc teste',
                data=df_result)
    plt.show()


def ajusta_dengue_taxa(df_dengue_origem, neighbor_columns):

    df_dengue_origem["taxa_dengue"] = df_dengue_origem[['dengue_diagnosis', 'Populacao']].apply(
        lambda x: round((x[0] / x[1]) * 100, 2), axis=1)

    df_dengue_origem.drop(columns=['t-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                                   't-7', 't-8', 't-9', 't-10', 't-11', 't-12'], inplace=True)

    for i in list(range(1, 13)):
        df_dengue_origem['t-' + str(i)] = df_dengue_origem["taxa_dengue"].shift(i)

    df_neighbors = sum_neighboors_to_prediction(df_dengue_origem.copy(), neighbor_columns, taxa=True)
    for n in neighbor_columns:
        df_dengue_origem[n] = list(df_neighbors[n])

    return df_dengue_origem


def analise_saida_classificacao_mapplot_and_prepara_dados_para_jupyter(percentil, peso, method):

    # df_result = pd.read_csv(base_path + \
    #     "/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_{}_peso{}_{}.csv".format(
    #         str(percentil), str(peso), method))

    df_result = main_classificacao_analise_por_valor_taxa(percentil, peso, method)

    df_result['falso_positivo'] = df_result[["outbreak_real", "outbreak_previsto"]].apply(
        lambda x: 1 if x[0] == 0 and x[1] == 1 else 0, axis=1)

    df_result['verdadeiro_positivo'] = df_result[["outbreak_real", "outbreak_previsto"]].apply(
        lambda x: 1 if x[0] == 1 and x[1] == 1 else 0, axis=1)

    df_result['falso_negativo'] = df_result[["outbreak_real", "outbreak_previsto"]].apply(
        lambda x: 1 if x[0] == 1 and x[1] == 0 else 0, axis=1)

    df_result['verdadeiro_negativo'] = df_result[["outbreak_real", "outbreak_previsto"]].apply(
        lambda x: 1 if x[0] == 0 and x[1] == 0 else 0, axis=1)

    df_consolidado = pd.DataFrame(columns=["Bairro", 'meses_retirados', '% falso positivo',
                                           'falso_positivo', "% surto", "surto abs", "AUC", "F1_Score"])

    for w in [0, 1, 2, 3, 4, 5, 6]:
        df = df_result[df_result['meses futuro'] == w]
        df_plot = df.pivot_table(index=['Bairro'],
                            values=['falso_positivo', 'verdadeiro_positivo',
                                    'falso_negativo', 'verdadeiro_negativo'],
                            aggfunc="sum")

        df_plot = df_plot.reset_index()

        df_auc_f1 = pd.DataFrame(columns=["Bairro", "AUC", "F1_Score"])
        for b in list(df['Bairro'].unique()):
            temp = df[df['Bairro'] == b]
            try:
                f1_bairro = f1_score(list(temp["outbreak_real"]), list(temp["outbreak_previsto"]), zero_division=1)
                auc_bairro = roc_auc_score(list(temp["outbreak_real"]), list(temp["outbreak_previsto"]))
            except Exception as ex:
                if re.match(".*Only one class present in y_true.*", str(ex)):
                    # Se os valores reais contiverem somente 0s, então o auc vai ser o inverso da
                    # (qtd 1s previsto / qtd de dados)
                    auc_bairro = 1 - (np.sum(temp["outbreak_previsto"])/temp.shape[0])
                else:
                    embed()
            temp = pd.DataFrame([[b, auc_bairro, f1_bairro]], columns=["Bairro", "AUC", "F1_Score"])
            df_auc_f1 = df_auc_f1.append(temp)

        df_plot = pd.merge(df_plot, df_auc_f1, on="Bairro")

        df_plot["% surto"] = df_plot[['verdadeiro_positivo', 'falso_negativo']].apply(
                                            lambda x: 0 if x[0] == 0 and x[0] + x[1] > 0 else \
                                                    100 if x[0] + x[1] == 0 else \
                                                round(x[0]/(x[0]+x[1]), 2)*100, axis=1)

        df_plot["surto abs"] = df_plot[['verdadeiro_positivo', 'falso_negativo']].apply(
            lambda x: x[0] + x[1], axis=1)

        df_plot["% falso positivo"] = df_plot[['falso_positivo', 'verdadeiro_negativo']].apply(
            lambda x: round(x[0] / (x[0] + x[1]), 2)*100, axis=1)

        df_plot["meses_retirados"] = [w] * df_plot.shape[0]

        df_consolidado = df_consolidado.append(df_plot[["Bairro", 'meses_retirados', '% falso positivo',
                                                        'falso_positivo', "% surto", "surto abs", "AUC",
                                                        "F1_Score"]])

    df_consolidado.fillna(1, inplace=True)

    df_consolidado.to_csv(base_path + \
        "/data_sus/finais/Resultados/Classificacao/dengue_resultado_classificacao_surto_fp_auc_percentil_{}_peso_{}_{}.csv".format(
            str(percentil), str(peso), method))


def compare_baseline_results_boxplot(percentil, peso, metodo):

    def calcula_metricas(df_result, coluna):

        df_result['falso_positivo'] = df_result[["outbreak_real", coluna]].apply(
            lambda x: 1 if x[0] == 0 and x[1] == 1 else 0, axis=1)

        df_result['verdadeiro_positivo'] = df_result[["outbreak_real", coluna]].apply(
            lambda x: 1 if x[0] == 1 and x[1] == 1 else 0, axis=1)

        df_result['falso_negativo'] = df_result[["outbreak_real", coluna]].apply(
            lambda x: 1 if x[0] == 1 and x[1] == 0 else 0, axis=1)

        df_result['verdadeiro_negativo'] = df_result[["outbreak_real", coluna]].apply(
            lambda x: 1 if x[0] == 0 and x[1] == 0 else 0, axis=1)

        df_auc_f1 = pd.DataFrame(columns=["nome_bairro", "AUC", "F1_Score"])
        for b in list(df_result['nome_bairro'].unique()):

            # Calcula AUC e F1-Score
            temp = df_result[df_result['nome_bairro'] == b]

            try:
                f1_bairro = f1_score(list(temp["outbreak_real"]), list(temp[coluna]), zero_division=1)
                auc_bairro = roc_auc_score(list(temp["outbreak_real"]), list(temp[coluna]))
            except Exception as ex:
                if re.match(".*Only one class present in y_true.*", str(ex)):
                    # Se os valores reais contiverem somente 0s, então o auc vai ser o inverso da
                    # (qtd 1s previsto / qtd de dados)
                    auc_bairro = 1 - (np.sum(temp[coluna])/temp.shape[0])
                else:
                    embed()
            temp = pd.DataFrame([[b, auc_bairro, f1_bairro]], columns=["nome_bairro", "AUC", "F1_Score"])
            df_auc_f1 = df_auc_f1.append(temp)

        # Calcula % surto e % falso positivo
        df_matriz_confusao = df_result.pivot_table(index=['nome_bairro'],
                                 values=['falso_positivo', 'verdadeiro_positivo',
                                         'falso_negativo', 'verdadeiro_negativo'],
                                 aggfunc="sum")

        df_matriz_confusao = df_matriz_confusao.reset_index()

        df_matriz_confusao["% surto"] = df_matriz_confusao[['verdadeiro_positivo', 'falso_negativo']].apply(
                                            lambda x: 0 if x[0] == 0 and x[0] + x[1] > 0 else \
                                                    100 if x[0] + x[1] == 0 else \
                                                round(x[0]/(x[0]+x[1]), 2)*100, axis=1)

        df_matriz_confusao["surto abs"] = df_matriz_confusao[['verdadeiro_positivo', 'falso_negativo']].apply(
            lambda x: x[0] + x[1], axis=1)

        df_matriz_confusao["% falso positivo"] = df_matriz_confusao[['falso_positivo', 'verdadeiro_negativo']].apply(
            lambda x: round(x[0] / (x[0] + x[1]), 2)*100, axis=1)

        df_auc_f1 = pd.merge(df_auc_f1, df_matriz_confusao, on='nome_bairro')

        nomes = [coluna.split("_")[2] + "_" + c for c in list(df_auc_f1.columns) if not c == "nome_bairro"]

        colunas = ["nome_bairro"]
        colunas.extend(nomes)

        df_auc_f1.columns = colunas

        return df_auc_f1

    def analise_surtos_no_tempo(percentil, peso, metodo, df_baseline_previsoes, columns):

        df_modelo_previsoes = pd.read_csv(base_path + \
                                          "/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_{}_peso{}_{}.csv".format(
                                              str(percentil), str(peso), metodo))

        df_modelo_previsoes.columns = ['Unnamed: 0', 'Bairro', 'ano', 'Mês', 'meses futuro', 'Real', 'Previsto']

        columns_chave = columns.copy()
        columns_chave.extend(["chave"])

        df_modelo_previsoes = df_modelo_previsoes[df_modelo_previsoes["meses futuro"] == 2]

        df_modelo_previsoes["chave"] = df_modelo_previsoes[["Bairro", "ano", 'Mês']].apply(
            lambda x: str(x[0]) + str(x[1]) + str(x[2]), axis=1)

        df_baseline_previsoes["chave"] = df_baseline_previsoes[["nome_bairro", "ano", 'Mês']].apply(
            lambda x: str(x[0]) + str(x[1]) + str(x[2]), axis=1)

        df_modelo_previsoes = pd.merge(df_modelo_previsoes, df_baseline_previsoes[columns_chave],
                                                                                    on="chave")

        columns_pivot = columns.copy()
        columns_pivot.extend(['Previsto', 'Real'])
        df_pivot_1 = pd.pivot_table(df_modelo_previsoes, index=["ano", 'Mês'],
                                                   values=columns_pivot,
                                                   aggfunc=np.sum)

        df_pivot_2 = pd.pivot_table(df_modelo_previsoes, index=["ano", 'Mês'],
                                                   values=columns,
                                                   aggfunc=np.sum)

        df_pivot = pd.merge(df_pivot_1, df_pivot_2, left_index=True, right_index=True)

        df_pivot = df_pivot.reset_index()

        df_melt = pd.melt(df_pivot, id_vars=['ano', 'Mês'],
                                  value_vars=columns_pivot)

        # df_melt = df_melt[~df_melt["variable"].isin(["previsao_baseline_bairro", "previsao_baseline_mes"])]

        f, ax = plt.subplots(figsize=(15, 10))
        sns.set_context(font_scale=0.9)
        for i, a in enumerate([2015, 2016, 2017, 2018, 2019, 2020]):
            ax = plt.subplot(231 + i)
            df_ano = df_melt[df_melt["ano"] == a]
            sns.lineplot(
                data=df_ano, x='Mês', y="value", hue="variable", linewidth=5)
            plt.title("Ano:{}, Meses Futuro: 3 (Percentil: {})".format(a, percentil))
            plt.ylim(0, 160)
            plt.ylabel("Qtd de Surtos")
            plt.legend(loc = 'upper right')
            ax.plot()

        plt.show()

    def analise_boxplot_metricas(percentil, peso, metodo,
                                 df_baseline_resultados_bairro,
                                 df_baseline_resultados_mes,
                                 df_baseline_resultados_bairro_mes):

        df_modelo_resultados = pd.read_csv(base_path + \
                                     "/data_sus/finais/Resultados/Classificacao/dengue_resultado_classificacao_surto_fp_auc_percentil_{}_peso_{}_{}.csv".format(
                                         str(percentil), str(peso), metodo))

        df_modelo_resultados = df_modelo_resultados[df_modelo_resultados["meses_retirados"] == 2]

        df_merge = pd.merge(df_baseline_resultados_bairro_mes, df_modelo_resultados,
                        left_on=["nome_bairro"], right_on=["Bairro"])

        # df_merge = pd.merge(df_baseline_resultados_mes, df_merge,
        #                     left_on=["nome_bairro"], right_on=["Bairro"])
        #
        # df_merge = pd.merge(df_baseline_resultados_bairro, df_merge,
        #                     left_on=["nome_bairro"], right_on=["Bairro"])

        f, ax = plt.subplots(figsize=(40, 20))
        for i, c in enumerate(['% falso positivo', '% surto', 'F1_Score']):

            df_melt = pd.melt(df_merge, id_vars=["Bairro"],
                         # value_vars=[c, "mes_"+c, "bairro_"+c, "bairro&mes_"+c]) Imprime os boxplots dos 3 baselines
                        value_vars = [c, "bairro&mes_" + c])

            df_melt['variable'] = df_melt['variable'].apply(lambda x: "Modelo" if x == c else "Baseline")

            ax = plt.subplot(131 + i)
            sns.set(style="whitegrid", font_scale=3)
            sns.boxplot(y="value", x="variable",
                        data=df_melt)
            # plt.title("Comparação com baselines {} (3 meses futuro) (Percentil: {})".format(c, percentil), fontsize=26)
            plt.ylabel(c, fontsize=40)
            # plt.legend(loc='upper left')
            ax.plot()

        plt.show()

    percentil = 80
    peso = 4
    metodo = "catboost"
    target = 'taxa_dengue'
    anos = list(range(2015, 2021))
    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/Inputs/dengue_input_from_source_taxa.csv")

    threshold = np.percentile(list(df_dengue_origem[target]), percentil)
    df_dengue_origem["outbreak_real"] = df_dengue_origem[target].apply(
        lambda x: 1 if x > threshold else 0)

    df_baseline_previsoes = pd.DataFrame(columns=['nome_bairro', 'ano', 'mes', 'outbreak_real',
                                        "previsao_baseline_bairro", "previsao_baseline_mes",
                                        "previsao_baseline_bairro&mes"])

    for a in anos:

        df_treino, df_teste = read_and_prepare_v3(
            df_dengue_origem, ano_teste=a, mes_teste=12, ano_treino=3)

        df_treino_baseline_bairro = df_treino.pivot_table(index=['nome_bairro'], values=["outbreak_real"],
                                                       aggfunc="sum")

        df_treino_baseline_mes = df_treino.pivot_table(index=['mes'], values=["outbreak_real"],
                                                       aggfunc="sum")

        df_treino_baseline_bairro_mes = df_treino.pivot_table(index=['nome_bairro', 'mes'], values=["outbreak_real"],
                                                       aggfunc="sum")

        denominador_bairro = df_treino['mes'].nunique() * df_treino['ano'].nunique()

        denominador_mes = df_treino['nome_bairro'].nunique() * df_treino['ano'].nunique()

        denominador_bairro_mes = df_treino['ano'].nunique()

        df_treino_baseline_bairro["% casos"] = df_treino_baseline_bairro["outbreak_real"].apply(
            lambda x: round(x/denominador_bairro, 2))

        df_treino_baseline_mes["% casos"] = df_treino_baseline_mes["outbreak_real"].apply(
            lambda x: round(x/denominador_mes, 2))

        df_treino_baseline_bairro_mes["% casos"] = df_treino_baseline_bairro_mes["outbreak_real"].apply(
            lambda x: round(x/denominador_bairro_mes, 2))

        df_teste["previsao_baseline_bairro"] = df_teste["nome_bairro"].apply(
            lambda x: 1 if np.random.random() < float(df_treino_baseline_bairro.loc[x,["% casos"]]) else 0)

        df_teste["previsao_baseline_mes"] = df_teste["mes"].apply(
            lambda x: 1 if np.random.random() < float(df_treino_baseline_mes.loc[x, ["% casos"]]) else 0)

        df_teste["previsao_baseline_bairro&mes"] = df_teste["mes"].apply(
            lambda x: 1 if np.random.random() < float(df_treino_baseline_mes.loc[x, ["% casos"]]) else 0)

        df_baseline_previsoes = df_baseline_previsoes.append(df_teste[['nome_bairro', 'ano', 'mes', 'outbreak_real',
                                "previsao_baseline_bairro", "previsao_baseline_mes", "previsao_baseline_bairro&mes"]])

    df_baseline_resultados_bairro = calcula_metricas(df_baseline_previsoes, "previsao_baseline_bairro")
    df_baseline_resultados_mes = calcula_metricas(df_baseline_previsoes, "previsao_baseline_mes")
    df_baseline_resultados_bairro_mes = calcula_metricas(df_baseline_previsoes, "previsao_baseline_bairro&mes")

    df_baseline_previsoes.columns = ['nome_bairro', 'ano', 'Mês', 'Real',
                                     'previsao_baseline_bairro', 'previsao_baseline_mes',
                                     'Baseline', 'falso_positivo', 'verdadeiro_positivo',
                                     'falso_negativo', 'verdadeiro_negativo']

    analise_surtos_no_tempo(percentil, peso, metodo, df_baseline_previsoes,
                            ["Baseline"])

    analise_boxplot_metricas(percentil, peso, metodo, df_baseline_resultados_bairro,
                             df_baseline_resultados_mes, df_baseline_resultados_bairro_mes)


def analisa_porcentagem_surto_por_qtd_de_surto_por_bairro_boxplot(percentil, peso, metodo):

    # percentil = 99.93
    # peso = 4
    # metodo = "catboost"

    # precisa rodar o método "main_classificacao_analise_por_valor_taxa(percentil, peso, method="lightGBM")" para gerar esse arquivo
    df_result = pd.read_csv(base_path + \
        "/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_{}_peso{}_{}.csv".format(
            str(percentil), str(peso), metodo))

    df_result = df_result[df_result["meses futuro"] == 2]

    df_result = df_result.pivot_table(index=["Bairro"], values=["outbreak_real"], aggfunc="sum")
    df_result = df_result.reset_index()
    df_result["grupo"] = df_result["outbreak_real"].apply(lambda x: "1.[0-5]" if x <= 6 else \
                                                         "2.[6-10]" if x <= 10 else
                                                         "3.[11-20]" if x <= 20 else \
                                                         "4.[21-30]" if x <= 30 else \
                                                         "5.[31-40]" if x <= 40 else \
                                                         "6.[41-50]" if x <= 50 else \
                                                         "7.[51-60]" if x <= 60 else \
                                                         "8.[61-70]")

    # precisa rodar o método "analise_saida_classificacao_mapplot_and_prepara_dados_para_jupyter" para gerar esse arquivo
    df_consolidado = pd.read_csv(base_path + \
        "/data_sus/finais/Resultados/Classificacao/dengue_resultado_classificacao_surto_fp_auc_percentil_{}_peso_{}_{}.csv".format(
            str(percentil), str(peso), metodo))

    temp = pd.merge(df_consolidado, df_result, on="Bairro", how="left")
    temp = temp[temp["meses_retirados"] == 1]

    temp.sort_values(by=["grupo"], inplace=True)

    sns.boxplot(y='% surto', x='grupo',
                data=temp)
    plt.title("% Surto (Meses Futuro = 2)".format(percentil, peso))
    # plt.ylim(0, 100)
    plt.xlabel("Qtd de Surtos entre 2015 e 2020")
    plt.show()


def analise_percentil_vs_qtd_surto():

    df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source_taxa.csv")

    percentil = [75, 80, 90, 95, 99.93]
    treshold_dict = dict()
    target = 'taxa_dengue'

    df_result = pd.DataFrame(columns=['nome_bairro', 'ano', 'outbreak', 'grupo', 'percentil'])

    for p in percentil:

        threshold = np.percentile(list(df_dengue_origem[target]), p)
        df_dengue_origem["outbreak"] = df_dengue_origem[target].apply(
            lambda x: 1 if x > threshold else 0)

        treshold_dict[p] = threshold

        df_pivot = df_dengue_origem.pivot_table(index=['nome_bairro', 'ano'], values=["outbreak"], aggfunc=np.sum)

        df_pivot = df_pivot.reset_index()

        df_pivot["percentil"] = [p] * df_pivot.shape[0]

        df_result = df_result.append(df_pivot)

    df_result = df_result[df_result["ano"].isin(list(range(2015, 2021)))]

    df_result = df_result.pivot_table(index=["nome_bairro", "percentil"], values=["outbreak"], aggfunc=np.sum)

    df_result["grupo"] = df_result["outbreak"].apply(lambda x: "1.[0-5]" if x <= 6 else \
                                                                "2.[6-10]" if x <= 10 else
                                                                "3.[11-20]" if x <= 20 else \
                                                                "4.[21-30]" if x <= 30 else \
                                                                "5.[31-40]" if x <= 40 else \
                                                                "6.[41-50]" if x <= 50 else \
                                                                "7.[51-60]" if x <= 60 else \
                                                                "8.[61-70]")

    df_result = df_result.reset_index()

    f, ax = plt.subplots(figsize=(15, 10))
    sns.set_context(font_scale=0.9)
    sns.color_palette("tab10")
    for i, a in enumerate(percentil):
        ax = plt.subplot(231 + i)
        df = df_result[df_result['percentil'] == a]
        df.sort_values(by=['grupo'], inplace=True)
        sns.histplot(
            data=df, x="grupo", multiple="layer")
        plt.title("Percentil: {} ({})".format(a, round(treshold_dict[a],2)))
        plt.ylim(0, 160)
        plt.ylabel("Qtd de bairros")
        ax.plot()
    plt.show()

####################
#   Visualization  #
####################

def analise_saida_classificacao_plot_line(target):

    percentil = 75
    peso = 1
    method = "catboost"

    df_result = main_classificacao_analise_por_valor_taxa(percentil, peso, method)

    df_result = pd.read_csv(base_path + \
        "/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_{}_peso{}_{}.csv".format(
            str(percentil), str(peso), method))

    for w in [0, 1, 2, 3, 4, 5, 6]:

        df_result_pivot = df_result[df_result["meses futuro"] == w]
        df_result_pivot = df_result_pivot.pivot_table(index=["ano", "mes"],
                                                values=["outbreak_real", "outbreak_previsto"],
                                                aggfunc="sum")

        df_result_pivot = df_result_pivot.reset_index()

        df_result_pivot = pd.melt(df_result_pivot, id_vars=['ano', 'mes'],
                          value_vars=['outbreak_previsto', 'outbreak_real'])

        f, ax = plt.subplots(figsize=(15, 10))
        sns.set_context(font_scale=0.9)
        for i, a in enumerate([2015, 2016, 2017, 2018, 2019, 2020]):
            ax = plt.subplot(231 + i)
            df_ano = df_result_pivot[df_result_pivot["ano"] == a]
            sns.lineplot(
                data=df_ano, x="mes", y="value", hue="variable", linewidth=5)
            plt.title("Ano:{} Meses Futuro: {}".format(a, w))
            plt.ylim(0, 160)
            plt.ylabel("Qtd de Surtos")
            ax.plot()

        if target == "taxa_dengue":
            f.suptitle("Valores por taxa de 100 habitantes", fontsize=26)
        else:
            f.suptitle("Valores Absolutos", fontsize=26)

        plt.show()


def analise_saida_classificacao_heatmap_old():

    df_result = main_classificacao_analise_por_valor_taxa()
    df_result = pd.read_csv(base_path + "/data_sus/finais/dengue_resultado_classificacao.csv")

    for w in [0, 1, 2, 3, 4, 5, 6]:

        df_result_pivot = df_result[df_result["meses futuro"] == w]
        df_result_pivot["diff"] = df_result_pivot[["outbreak_real", "outbreak_previsto"]].apply(
                                                                        lambda x: x[0] - x[1], axis=1)

        for i, a in enumerate([2015, 2016, 2017, 2018, 2019, 2020]):

            df_ano = df_result_pivot[df_result_pivot["ano"] == a]

            labels = df_ano.pivot_table(index=["Bairro"], columns=["mes"],
                                                    values=["diff"])

            f, ax = plt.subplots(figsize=(15, 12))
            sns.set_context(font_scale=0.9)
            sns.heatmap(
                data=labels, annot=labels, fmt='', linewidths=0.3, ax=ax)
            plt.title("Ano:{} Meses Futuro: {}".format(a, w))
            ax.plot()
            plt.show()


def analise_saida_classificacao_heatmap():

    df_result = teste_classificacao_analise_por_valor_taxa()
    df_result = pd.read_csv(base_path + "/data_sus/finais/dengue_resultado_classificacao.csv")

    for w in [0, 1, 2, 3, 4, 5, 6]:

        df_result_pivot = df_result[df_result["meses futuro"] == w]
        df_result_pivot = df_result_pivot.pivot_table(index=["Bairro", 'mes', 'meses futuro'],
                                                      values=['outbreak_real', 'outbreak_previsto'],
                                                      aggfunc="sum")

        df_result_pivot = df_result_pivot.reset_index()

        df_result_pivot["diff"] = df_result_pivot[["outbreak_real", "outbreak_previsto"]].apply(
                                                                        lambda x: x[1] - x[0], axis=1)

        labels = df_result_pivot.pivot_table(index=["Bairro"], columns=["mes"],
                                                values=["diff"])

        labels.columns = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]

        for i in [0, 1, 2, 3]:
            aux = labels.iloc[i*40:(i+1)*40, :]
            f, ax = plt.subplots(figsize=(15, 12))
            sns.set_context(font_scale=0.9)
            sns.heatmap(
                data=aux, annot=aux, fmt='', linewidths=0.3, ax=ax, vmin=-6, vmax=6)
            plt.title("Meses Futuro: {}".format(w))
            ax.plot()
            plt.show()


def plot_f1_score_vs_threshold():

    df_result = pd.read_csv(base_path+"/data_sus/finais/dengue_resultado_classificacao_v2.csv")

    df_plot = pd.DataFrame(columns=['limite', 'f1'])

    for r in list(np.arange(0, 1.1, 0.1)):
        df_result["outbreak_previsto_b"] = df_result["outbreak_previsto"].apply(
            lambda x: 1 if x >= r else 0)

        df = pd.DataFrame([[r, f1_score(list(df_result["outbreak_real"]),
                                        list(df_result["outbreak_previsto_b"]))]],
                          columns=['limite', 'f1'])

        df_plot = df_plot.append(df)

    sns.set_context(font_scale=0.9)
    sns.lineplot(
        data=df_plot, x="limite", y="f1", linewidth=5)
    plt.title("Limite vs F1-score")
    plt.ylabel("F1-Score")
    plt.show()


def analise_variacao_percentil():

    def gera_base_para_analise():
        df_percentil = pd.DataFrame(columns=['Bairro', 'ano', 'mes', 'meses futuro', 'outbreak_real',
           'outbreak_previsto', 'percentil'])

        for p in list(np.arange(10, 99, 10)):
            df = teste_classificacao_analise_por_valor_taxa(round(p, 2))
            df["percentil"] = [round(p, 2)] * df.shape[0]
            df_percentil = df_percentil.append(df)

        df_percentil.to_csv(base_path+"/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_v2.csv")

    df_percentil = pd.read_csv(base_path+"/data_sus/finais/dengue_resultado_classificacao_taxa_percentil_v2.csv")

    df_percentil_3_meses = df_percentil[df_percentil['meses futuro'] == 0]

    df_consolidado = pd.DataFrame(columns=["bairro", "auc", "percentil"])

    for p in list(df_percentil_3_meses["percentil"].unique()):
        temp_1 = df_percentil_3_meses[df_percentil_3_meses["percentil"] == p]
        for b in list(df_percentil["Bairro"].unique()):
            temp_2 = temp_1[temp_1["Bairro"] == b]
            try:
                auc_bairro = roc_auc_score(list(temp_2["outbreak_real"]), list(temp_2["outbreak_previsto"]))
            except Exception as ex:
                if re.match(".*Only one class present in y_true.*", str(ex)):
                    # Se os valores reais contiverem somente 0s, então o auc vai ser o inverso da
                    # (qtd 1s previsto / qtd de dados)
                    auc_bairro = 1 - (np.sum(temp_2["outbreak_previsto"])/temp_2.shape[0])
                else:
                    embed()
            df = pd.DataFrame([[b, auc_bairro, p]], columns=["bairro", "auc", "percentil"])
            df_consolidado = df_consolidado.append(df)

    f, ax = plt.subplots(figsize=(30, 20))
    sns.boxplot(y="auc", x="percentil",
                data=df_consolidado)
    plt.title("Variação de Percentil (1 meses futuro)", fontsize=26)
    plt.ylabel("AUC", fontsize=20)
    plt.xlabel("Percentil", fontsize=20)
    plt.show()


def analise_variacao_pesos_classificacao(percentil=80):

    target = "taxa_dengue"
    # target = 'dengue_diagnosis'
    ano = list(range(2015, 2021))

    if target == "taxa_dengue":
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source_taxa.csv")
    else:
        df_dengue_origem = pd.read_csv(base_path + "/data_sus/finais/dengue_input_from_source.csv")

    columns_filtered = ['Populacao',
                        't-1', 't-2', 't-3', 't-4', 't-5', 't-6',
                        't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                        'temperatura (°C)-1', 'precipitacao (mm)-1',
                        'umidade ar (%)-2', 'sum_vizinhos_t-1']

    categorical_columns = []

    columns_filtered_categorical = []

    threshold = np.percentile(list(df_dengue_origem[target]), percentil)
    df_dengue_origem["outbreak"] = df_dengue_origem[target].apply(
        lambda x: 1 if x > threshold else 0)

    df_result = pd.DataFrame(columns=['Bairro', 'ano', 'mes', "peso", "outbreak_real",
                                      "outbreak_previsto"])

    for a in ano:

        df_treino, df_teste = read_and_prepare_v3(df_dengue_origem, ano_teste=a, mes_teste=12, ano_treino=3)

        X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        X_teste, y_teste = prepare_base_treino(df_teste, columns_filtered, columns_filtered_categorical,
                                               categorical_columns, target_variable="outbreak")

        X_train.drop(columns=["t-{}".format(2)], inplace=True)
        X_teste.drop(columns=["t-{}".format(2)], inplace=True)

        for p in list(np.arange(1, 11, 1)):

            auc, classifier = run_lightGBM_classifier(X_train, y_train, p=p)

            # y_pred = classifier.predict_proba(X_teste)
            # y_pred = [round(y[0], 2) for y in y_pred]

            y_pred = classifier.predict(X_teste)
            y_pred = [round(y, 2) for y in y_pred]

            df_aux = pd.DataFrame(
                columns=['Bairro', 'ano', 'mes', "peso", "outbreak_real", "outbreak_previsto"])

            df_aux['Bairro'] = df_teste['nome_bairro']
            df_aux['ano'] = df_teste['ano']
            df_aux['mes'] = df_teste['mes']
            df_aux['peso'] = [p] * df_teste.shape[0]
            df_aux["outbreak_real"] = df_teste["outbreak"]
            df_aux["outbreak_previsto"] = list(y_pred)

            df_result = df_result.append(df_aux)

    df_result.to_csv(base_path+"/data_sus/finais/dengue_resultado_classificacao_taxa_analise_peso_v1.csv")

    return df_result


def plot_boxplot_analise_peso():

    df_result = pd.read_csv(base_path+"/data_sus/finais/dengue_resultado_classificacao_taxa_analise_peso_v1.csv")

    df_consolidado = pd.DataFrame(columns=["bairro", "peso", "auc"])

    for p in list(df_result["peso"].unique()):

        df = df_result[df_result["peso"] == p]
        for b in list(df["Bairro"].unique()):
            temp = df[df["Bairro"] == b]
            try:
                auc_bairro = roc_auc_score(list(temp["outbreak_real"]), list(temp["outbreak_previsto"]))
            except Exception as ex:
                if re.match(".*Only one class present in y_true.*", str(ex)):
                    # Se os valores reais contiverem somente 0s, então o auc vai ser o inverso da
                    # (qtd 1s previsto / qtd de dados)
                    auc_bairro = 1 - (np.sum(temp["outbreak_previsto"])/temp.shape[0])
                else:
                    embed()
            temp_2 = pd.DataFrame([[b, p, auc_bairro]], columns=["bairro", "peso", "auc"])
            df_consolidado = df_consolidado.append(temp_2)

    f, ax = plt.subplots(figsize=(30, 20))
    sns.boxplot(y="auc", x="peso",
                data=df_consolidado)
    plt.title("Variação de peso na classificação (3 meses futuro)", fontsize=26)
    plt.ylabel("AUC", fontsize=20)
    plt.xlabel("Peso", fontsize=20)
    plt.show()


def analise_geopandas():

    f = open("Bairros.json", )
    maps = json.load(f)
    df_maps = pd.DataFrame(columns=['nome', 'geometry'])

    for m in maps['features']:
        try:
            if m['geometry']['type'].upper() == "POLYGON":
                geo = Polygon(m['geometry']['coordinates'][0])
            elif m['geometry']['type'].upper() == "MULTIPOLYGON":
                polygons = [Polygon(x[0]) for x in m['geometry']['coordinates']]
                geo = MultiPolygon(polygons)

            nome = m['properties']['NM_BAIRRO']
            df = pd.DataFrame([[nome, geo]], columns=['nome', 'geometry'])
            df_maps = df_maps.append(df)
        except Exception as ex:
            embed()

    df_maps["nome"] = df_maps["nome"].apply(lambda x: x.title())

    list_bairros = list()
    for c in list(df_maps["nome"]):

        if not c in list(df_result["Bairro"].unique()):
            print(c)
            list_bairros.append(c)

    temp = df_result.pivot_table(index=["Bairro"])
    temp = pd.DataFrame(list(df_result["Bairro"].unique()), columns=["bairro"])

    temp = pd.merge(df_maps, temp,
                    left_on="nome", right_on="bairro",
                    how="left")


def plot_boxplot_surto_fp_auc_f1(percentil, peso, method):

    # percentil = 75
    # peso = 1
    # method = "catboost"

    df_consolidado = pd.read_csv(base_path + \
        "/data_sus/finais/Resultados/Classificacao/dengue_resultado_classificacao_surto_fp_auc_percentil_{}_peso_{}_{}.csv".format(
            str(percentil), str(peso), method))

    sns.boxplot(y='AUC', x='meses_retirados',
                data=df_consolidado)
    plt.title("AUC (Percentil {}%, Peso {})".format(percentil, peso))
    plt.ylim(0, 1)
    plt.show()

    sns.boxplot(y='F1_Score', x='meses_retirados',
                data=df_consolidado)
    plt.title("F1_Score (Percentil {}%, Peso {})".format(percentil, peso))
    plt.ylim(0, 1)
    plt.show()

    sns.boxplot(y='% surto', x='meses_retirados',
                data=df_consolidado)
    plt.title("% surto (Percentil {}%, Peso {})".format(percentil, peso))
    # plt.ylim(0, 100)
    plt.show()

    sns.boxplot(y='% falso positivo', x='meses_retirados',
                data=df_consolidado)
    plt.title("% falso positivo (Percentil {}%, Peso {})".format(percentil, peso))
    plt.ylim(0, 50)
    plt.show()


if __name__ == '__main__':

