from datetime import date
from machine_learning.preProcessing import *
from machine_learning.mlMethods import *
from machine_learning.mlSupportMethods import *
from machine_learning.visualization import *
from machine_learning.measuringResults import *
from machine_learning.measuring_r2 import *

import pickle
from scipy import stats

# from sklearn.feature_selection import SelectKBest
# from sklearn.feature_selection import f_regression

from IPython import embed;
# embed()

pd.options.mode.chained_assignment = None
base_path = os.getcwd()

'''
Todas as variáveis
'''
categorical_columns = ['nome_bairro', 'mes']
columns_filtered = ['nome_bairro', 'mes', 'qtd_cnes', 'temp_solo',
                   'precipitacao', 'IDH', 't-1', 't-2', 't-3', 't-4',
                   't-5', 't-6', 't-7', 't-8', 't-9', 't-10', 't-11', 't-12',
                   'sum_vizinhos_t-1', 'sum_vizinhos_t-3', 'sum_vizinhos_t-6']

neighbor_columns=['sum_vizinhos_t-1', 'sum_vizinhos_t-3', 'sum_vizinhos_t-6']

columns_filtered_categorical = []

'''
Selecionar variáveis
'''

categorical_columns = ['mes']

columns_filtered = ['mes', 'qtd_cnes', 'temp_solo',
                   'precipitacao', 't-1', 't-2', 't-3', 't-4',
                   't-5', 't-6', 't-7', 't-8', 't-9', 't-10',
                   'sum_vizinhos_t-1']

neighbor_columns = ['sum_vizinhos_t-1']

columns_filtered_categorical = ['mes_4']

m = "linear_regression"
m = "polynomial_regression"
m = "svr"
m = "randomforest"
m = "extratrees"
m = "xgboost"
m = "lightGBM"

def read_and_prepare(columns_filtered, categorical_columns,
                            columns_filtered_categorical):

    df_input = read_and_get_input_v2(base_path+ \
                                     '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    # df_pop = sanitiza_populacao()
    # df_pop = df_pop[['Cod_bairro (12_dig)', 'População 2010']]
    #
    # df_input = pd.merge(df_input, df_pop,
    #                         left_on='cod_bairro', right_on='Cod_bairro (12_dig)')

    ano_teste = 2017

    df_treino = df_input[df_input['ano'].isin(
        list(range(ano_teste -3, ano_teste, 1)))]
    df_teste = df_input[df_input['ano'].isin(
        list(range(ano_teste, ano_teste +1, 1)))]
    df_teste = df_teste[df_input['mes'].isin(
        list(range(1, 13, 1)))]

    X_train = df_treino[columns_filtered]
    y_train = df_treino['dengue_diagnosis']

    X_train = pd.get_dummies(X_train, columns=categorical_columns)

    if not columns_filtered_categorical == []:
        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
        X_train = X_train[new_columns_features + columns_filtered_categorical]

    columns, index_time_cols = get_indices_columns_time(X_train.columns)
    X_train = X_train[columns]

    return X_train, y_train, df_teste


def read_and_prepare_v2(ano_teste=2017, mes_teste=12):

    df_input = read_and_get_input_v2(base_path+ \
                                     '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    df_treino = df_input[df_input['ano'].isin(
        list(range(ano_teste -3, ano_teste, 1)))]
    df_teste = df_input[df_input['ano'].isin(
        list(range(ano_teste, ano_teste +1, 1)))]
    df_teste = df_teste[df_input['mes'].isin(
        list(range(1, mes_teste+1, 1)))]

    return df_treino, df_teste


def main_process(m):

    df_treino, df_teste = read_and_prepare_v2(mes_teste=12)

    X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                        categorical_columns)

    regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

    # pickle.dump(regressor, open(base_path+ \
    #                                  '/data_sus/finais/model_{}_all_variables.p'.format(m), "wb"))

    df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                               columns_filtered, columns_filtered_categorical,
                                                               list(X_train.columns), categorical_columns,
                                                               neighbor_columns)


    df_aux = df_output_model[['chave', 'dengue_diagnosis_previsto']]
    df_teste = pd.merge(df_teste, df_aux, on='chave', how='left')
    df_teste['diff'] = df_teste[['dengue_diagnosis', 'dengue_diagnosis_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    df_variance_each_city = apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(
        df_treino, df_teste)

    df_variance_each_city['cluster'] = df_variance_each_city['r2'].apply(lambda x: "[0.5, 1]" if x >= 0.5 else \
                                         "[0, 0.5)" if x >= 0 else "[-0.5, 0)" \
                                         if x >=-0.5 else "[-5,-0.5)"  \
                                             if x >= -5 else "[,-5)")

    df_variance_each_city.sort_values(by=["r2"], inplace=True, ascending=False)

    df_variance_each_city['cluster'].value_counts()

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.kdeplot(data=df_variance_each_city, x="r2")

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.kdeplot(x="r2",
                fill=True,
                data=df_variance_each_city)

    plt.show()

    # sns.set_theme(style="white")
    # f, ax = plt.subplots(figsize=(8, 6))
    # sns.set_context(font_scale=0.9)
    # sns.barplot(x='city', y='r2',
    #             data=df_variance_each_city)
    # plt.title(m)
    # plt.show()


def selecao_melhores_parametros(m):

    df_treino, df_teste = read_and_prepare_v2(mes_teste=12)

    X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                        categorical_columns)

    if m in ["randomforest", "extratrees", "xgboost", "lightGBM"]:
        regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=True)
    else:
        regressor, scaler, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

    apply_adjusted_cross_validate(X_train, y_train, regressor, m)


def select_features(m):

    # Dataframe que saiu da análise de shap values
    df_features = pd.read_csv(base_path+\
            '/data_sus/finais/feature_importance_{}.csv'.format(m))

    # Base de dados completa
    df_input = read_and_get_input_v2(base_path+ \
                                     '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    ano_teste = 2017

    df_input = df_input[df_input['ano'].isin(
        list(range(ano_teste-3, ano_teste+1, 1)))]

    # Separação entre independentes e dependente
    X_train_copy = df_input[columns_filtered]
    y_train = df_input['dengue_diagnosis']

    X_train_copy = pd.get_dummies(X_train_copy, columns=categorical_columns)
    columns, index_time_cols = get_indices_columns_time(X_train_copy.columns)
    X_train_copy = X_train_copy[columns]

    # Criação do df com a consolidação dos resultados
    columns_result = ['Modelo', 'R2', 'Quantidade de Variaveis']
    df_results = pd.DataFrame(columns=columns_result)

    i = 1
    while i <= 19:

        X_train = X_train_copy.loc[:, list(df_features['feature'])[:i*10]]

        regressor, r2 = globals()["run_" + m](X_train, y_train)

        aux = pd.DataFrame([[m, r2, X_train.shape[1]]], columns=columns_result)

        df_results = df_results.append(aux)

        i += 1

    df_results.to_csv(base_path + "/data_sus/finais/auc_qtd_features_{}.csv".format(m))

    return df_results


def r2_in_time(m, df_treino, df_teste, columns_filtered, categorical_columns,
               neighbor_columns, columns_filtered_categorical):

    df_r2_time = analyse_r2_by_time_v3(m, df_treino, df_teste,
                       columns_filtered, categorical_columns,
                       columns_filtered_categorical, neighbor_columns)

    df_r2_time.to_csv(base_path + \
                          '/data_sus/finais/r2_por_trimestre_{}.csv'.format(m))


def spearman_calculation():

    df_treino, df_teste = read_and_prepare_v2()

    X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                        categorical_columns)

    regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

    df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                               columns_filtered, columns_filtered_categorical,
                                                               categorical_columns, neighbor_columns)


    df_pivot_real = pd.pivot_table(data=df_teste, index=['nome_bairro'],
                                   values=['dengue_diagnosis'], aggfunc="sum")

    df_pivot_real.sort_values(by=['nome_bairro'], inplace=True)

    df_pivot_prev = pd.pivot_table(data=df_output_model, index=['nome_bairro'],
                                   values=['dengue_diagnosis_previsto'], aggfunc="sum")

    df_pivot_prev.sort_values(by=['nome_bairro'], inplace=True)

    print(stats.spearmanr(df_pivot_real['dengue_diagnosis'],
                          df_pivot_prev['dengue_diagnosis_previsto']))

    # SpearmanrResult(correlation=0.7005077114793128,
    #                 pvalue=6.366690681924964e-25)

    df_pivot_prev.sort_values(by=['dengue_diagnosis_previsto'], inplace=True, ascending=False)
    df_pivot_real.sort_values(by=['dengue_diagnosis'], inplace=True, ascending=False)

    df_pivot_prev = df_pivot_prev.reset_index()
    df_pivot_real = df_pivot_real.reset_index()

    df_pivot_prev = df_pivot_prev.iloc[:10, :]
    df_pivot_real = df_pivot_real.iloc[:10, :]

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.barplot(x='nome_bairro', y='dengue_diagnosis_previsto',
                data=df_pivot_prev)
    plt.show()

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.barplot(x='nome_bairro', y='dengue_diagnosis',
                data=df_pivot_real)
    plt.show()


def outbreak_validation():

    df_treino, df_teste = read_and_prepare_v2()

    X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                        categorical_columns)

    regressor, r2 = globals()["run_{}".format(m)](X_train, y_train, grid_search=False)

    df_output_model = apply_future_prediction_update_neighbors(regressor, df_teste,
                                                               columns_filtered, columns_filtered_categorical,
                                                               categorical_columns, neighbor_columns)

    df_real = df_treino.append(df_teste)

    outbreak_value = np.percentile(list(df_real["dengue_diagnosis"]), 98)

    outbreak_real = list()
    outbreak_previsto = list()

    for r in list(df_teste['mes'].unique()):
        list_r = df_teste[df_teste['mes'] == r]["dengue_diagnosis"]
        list_f = df_output_model[df_output_model['mes'] == r]["dengue_diagnosis_previsto"]
        r_aux = [1 if n >= outbreak_value else 0 for n in list_r]
        f_aux = [1 if n >= outbreak_value else 0 for n in list_f]
        outbreak_real.append(np.sum(r_aux))
        outbreak_previsto.append(np.sum(f_aux))

    df_outbreak = pd.DataFrame(outbreak_real, columns=['Valor'])
    df_outbreak_aux = pd.DataFrame(outbreak_previsto, columns=['Valor'])
    df_outbreak['Categoria'] = ["Real"] * 12
    df_outbreak['Mês'] = list(range(1, 13))
    df_outbreak_aux['Categoria'] = ["Previsto"] * 12
    df_outbreak_aux['Mês'] = list(range(1, 13))
    df_outbreak = df_outbreak.append(df_outbreak_aux)

    f, ax = plt.subplots(figsize=(15, 6))
    sns.set_context(font_scale=0.9)
    sns.lineplot(
        data=df_outbreak, x="Mês", y="Valor", hue="Categoria", linewidth=5)
    plt.show()


if __name__ == '__main__':

    '''
    TO DOs
    
    Foi executado o grid_search, os melhores parâmetros irão ser retornados
    depois rodar a validação cruzada
    
    Depois fazer a seleção de variáveis com o SHAP values, coletar desempenhos
    e depois rodar grid_search novamente
    
    '''

    df_select_features = select_features(m)

    df_features = pd.read_csv(base_path + \
                              '/data_sus/finais/feature_importance_{}.csv'.format(m))

    df_r2_features = pd.read_csv(base_path + "/data_sus/finais/auc_qtd_features_{}.csv".format(m))

    regressor = pickle.load(open(base_path + \
                                 '/data_sus/finais/model_{}_all_variables.p'.format(m), "rb"))

    df_r2_in_time = r2_in_time(m)