#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:36:31 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
import statsmodels.api as sm
from sklearn.base import BaseEstimator, RegressorMixin
from codigos_higienizacao.leitura_e_higienizacao_network import sum_neighboors_to_prediction, network_neigboor
import shap
import pickle
from IPython import embed
base_path = os.getcwd()


#######################
# Métodos utilizados  #
#######################
def prepare_base_teste(df_previsao, columns_filtered, columns_filtered_categorical,
                            categorical_columns, train_columns, scaler):

    # O método recebe a base de teste com todas as colunas da base original
    # df_previsao_X é o df que será inserido no modelo para previsão
    df_previsao_X = df_previsao[columns_filtered]

    # Se existem variáveis categoricas então transforma em binário
    if not categorical_columns == []:
        df_previsao_X = pd.get_dummies(df_previsao_X, columns=categorical_columns)

    # para as variáveis categóricas filtra as variáveis e valores previamente secionados
    mes_list = ["mes_{}".format(i) for i in list(df_previsao["mes"].unique())]

    if not columns_filtered_categorical == []:

        # para o caso de análise de r2 no tempo
        if 'mes' in categorical_columns:
            mes_list = [m for m in mes_list if m in columns_filtered_categorical]

            categorical_list = [a for a in categorical_columns if not re.match(".*mes.*", a)]

            # Se houver mais variáveis categóricas além de mes
            if len(categorical_list) > 0:
                columns_filtered_categorical_new = categorical_list + mes_list
            else:
                columns_filtered_categorical_new = mes_list

        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
        df_previsao_X = df_previsao_X[new_columns_features + columns_filtered_categorical_new]

    # Caso o modelo foi treinado com meses que estão além dos meses na base de teste
    meses_ausentes = [m for m in train_columns if re.match(".*mes.*", m) and not m in mes_list]
    if len(meses_ausentes) > 0:
        for m in meses_ausentes:
            df_previsao_X[m] = [0] * df_previsao_X.shape[0]

    # Se o regressor exige valores normalizados
    if scaler:
        df_previsao_X = pd.DataFrame(scaler.transform(df_previsao_X),
                                     columns=df_previsao_X.columns)

    columns, index_time_cols = get_indices_columns_time(df_previsao_X.columns)
    df_previsao_X = df_previsao_X[columns]

    # Precisa dessas variáveis para percorrer na previsão
    df_previsao_X['ano'] = df_previsao['ano']
    df_previsao_X['mes'] = df_previsao['mes']
    df_previsao_X['nome_bairro'] = df_previsao['nome_bairro']
    df_previsao_X['chave'] = df_previsao['chave']
    df_previsao_X['cod_bairro'] = df_previsao['cod_bairro']

    return df_previsao_X, columns


def prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                            categorical_columns, target_variable='dengue_diagnosis'):

    X_train = df_treino[columns_filtered]
    y_train = df_treino[target_variable]

    X_train = pd.get_dummies(X_train, columns=categorical_columns)

    # Para o caso de haver variáveis categoricas que serão selecionadas
    if not columns_filtered_categorical == []:

        # Tratativa especial para o caso de mês
        if 'mes' in categorical_columns:

            # Irá buscar somente os meses que estão na base de treino, pode ir de 1 a 12
            mes_list = ["mes_{}".format(i) for i in list(df_treino["mes"].unique())]
            mes_list = [m for m in mes_list if m in columns_filtered_categorical]
            categorical_list = [a for a in categorical_columns if not re.match(".*mes.*", a)]

            # Se na lista de variáveis categoricas não tivessem somente o mês
            if len(categorical_list) > 0:
                columns_filtered_categorical_new = categorical_list + mes_list
            else:
                columns_filtered_categorical_new = mes_list

        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
        X_train = X_train[new_columns_features + columns_filtered_categorical_new]

        meses_ausentes = [m for m in columns_filtered_categorical if re.match(".*mes.*", m) and not m in mes_list]
        if len(meses_ausentes) > 0:
            for m in meses_ausentes:
                X_train[m] = [0] * X_train.shape[0]


    columns, index_time_cols = get_indices_columns_time(X_train.columns)
    X_train = X_train[columns]

    return X_train, y_train


def apply_future_prediction_update_neighbors(regressor, df_previsao, columns_filtered,
                                             columns_filtered_categorical, train_columns,
                                             categorical_columns=[], neighbor_columns=[],
                                             target_variable='dengue_diagnosis', scaler=False):

    df_previsao_X, columns = prepare_base_teste(df_previsao, columns_filtered, columns_filtered_categorical,
                            categorical_columns, train_columns, scaler)

    list_results = list()
    df_result_consolidate = pd.DataFrame(
        columns=list(df_previsao_X.columns)+[target_variable+"_previsto"])

    for a in list(df_previsao_X['ano'].unique()):
        for m in list(df_previsao_X['mes'].unique()):
            try:
                # Filtra o ano e mes de forma sequencial
                df = df_previsao_X[(df_previsao_X['ano'] == a) & (df_previsao_X['mes'] == m)]

                # Se não for o primeiro, então precisa mover os casos para os
                # meses anteriores e recalcular os vizinhos
                if len(list_results) > 0:

                    # Atribui os valores que já foram previstos para o novo df
                    for n in list(range(1, len(list_results)+1)):
                        df['t-{}'.format(n)] = list_results[-n]

                    # Apaga as colunas dos vizinhos para calcular novamente
                    if not neighbor_columns == []:
                        for n in neighbor_columns:
                            df.drop(columns=[n], inplace=True)

                        # Ajustar soma casos dos vizinhos
                        df_neighbors = sum_neighboors_to_prediction(df.copy(), neighbor_columns)
                        for n in neighbor_columns:
                            df[n] = list(df_neighbors[n])

                df_prev = df.drop(columns=['nome_bairro', 'chave', 'ano', 'mes', 'cod_bairro'])
                df_prev = df_prev[columns]

                # Faz a previsão e armazena na lista de suporte e no df de resultado final
                list_results.append(regressor.predict(df_prev))
                if target_variable == 'dengue_diagnosis':
                    list_results[-1] = [round(a) for a in list_results[-1]]
                df[target_variable+"_previsto"] = list_results[-1]
                df_result_consolidate = df_result_consolidate.append(df)
            except Exception as ex:
                print("apply_future_prediction_update_neighbors")
                embed()

    # Inverte novamente as colunas categóricas
    for cat in categorical_columns:
        for c in df_previsao[cat].unique():
            if cat + '_' + str(c) in list(df_result_consolidate.columns):
                df_result_consolidate.drop(columns=[cat + '_' + str(c)], inplace=True)

    df_result_consolidate.sort_values(by=["cod_bairro", "ano", "mes"], inplace=True)

    return df_result_consolidate


##############################
# Métodos Antigos            #
##############################
class SMWrapper(BaseEstimator, RegressorMixin):
    """ A universal sklearn-style wrapper for statsmodels regressors """
    def __init__(self, model_class, fit_intercept=True):
        self.model_class = model_class
        self.fit_intercept = fit_intercept

    def fit(self, X, y):
        if self.fit_intercept:
            X = sm.add_constant(X)
        self.model_ = self.model_class(y, X)
        self.results_ = self.model_.fit()

    def predict(self, X):
        if self.fit_intercept:
            X = sm.add_constant(X)
        return self.results_.predict(X)


def apply_future_prediction(regressor, df_teste, columns_filtered, target_variable,  columns_filtered_categorical,
                            categorical_columns=[], scaler=False):

    df_previsao_X = df_teste[columns_filtered]
    if not categorical_columns == []:
        df_previsao_X = pd.get_dummies(df_previsao_X, columns=categorical_columns)

    if not columns_filtered_categorical == []:
        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
        df_previsao_X = df_previsao_X[new_columns_features+columns_filtered_categorical]

    df_previsao_X["nome_bairro"] = df_teste["nome_bairro"]

    # If regressor used scaler
    if scaler:
        df_previsao_X = pd.DataFrame(scaler.transform(df_previsao_X),
                                     columns=df_previsao_X.columns)
        
    # Execute prediction in test base, recording time series
    city_dict = {}
    df_result_consolidate = pd.DataFrame(
        columns=columns_filtered+[target_variable])
    index_time_cols = []
    for c in df_teste['nome_bairro'].unique():
                
        # Create dataframe with region that will be analysed
        df = df_previsao_X[df_previsao_X["nome_bairro"] == c]
        df.drop(columns=["nome_bairro"], inplace=True)
            
        if index_time_cols == []:
            columns, index_time_cols = get_indices_columns_time(df.columns)
        df = df[columns]
        
        # Create a dict to record the results
        city_dict[c] = list()
        
        # It will pass by all lines of the test base
        for i in list(range(0, df.shape[0])):
            
            # When it is not the first, record last prediction
            if not i == 0 and len(index_time_cols) > 0:
                # Put the last predicted value in t-1
                df.iloc[i,index_time_cols[0]] = y_predict
                
                # Shift values since t-2 until t-n
                for ct in list(range(index_time_cols[0], index_time_cols[1])):
                    try:
                        df.iloc[i,ct+1] = df.iloc[i-1,ct]
                    except:
                        print(ct)
            
            # Predict new value
            df_temp = pd.DataFrame(np.array(df.iloc[i,:]).reshape(1,-1), 
                                  columns=df.columns)
            y_predict = round(regressor.predict(df_temp)[0])
            
            # Record all predict values to build target column
            city_dict[c].append(y_predict)
        
        # Consolidating all results
        df[target_variable] = city_dict[c]
        df = df[columns+[target_variable]]
        df_result_consolidate = df_result_consolidate.append(df, sort=False)
    
    # Inverting categorical columns
    for cat in categorical_columns:
        for c in df_teste[cat].unique():
            if cat+'_'+str(c) in list(df_result_consolidate.columns):
                df_result_consolidate.drop(columns=[cat+'_'+str(c)], inplace=True)
        df_result_consolidate[cat] = df_teste[cat]
        
    df_result_consolidate['chave'] = df_teste['chave']

    if not 'ano' in df_result_consolidate.columns:
        df_result_consolidate['ano'] = df_teste['ano']

    if not 'nome_bairro' in df_result_consolidate.columns:
        df_result_consolidate['nome_bairro'] = df_teste['nome_bairro']
        df_result_consolidate = df_result_consolidate[['chave', 'nome_bairro', 'ano'] + \
                                                      columns_filtered + [target_variable]]
    else:
        df_result_consolidate = df_result_consolidate[['chave', 'ano'] + \
                                                      columns_filtered+[target_variable]]
    
    return df_result_consolidate


def apply_future_prediction_network(regressor, df_previsao, columns_filtered,
                            target_variable,
                            categorical_columns=[],
                            scaler=False):
    
    df_previsao_X = df_previsao[columns_filtered]
    
    # If the base has categorical columns
    if not categorical_columns == []:
        df_previsao_X = pd.get_dummies(df_previsao_X, columns=categorical_columns)
        
    # If regressor used scaler
    if scaler:
        df_previsao_X = pd.DataFrame(scaler.transform(df_previsao_X),
                                     columns=df_previsao_X.columns)
        
    columns, index_time_cols = get_indices_columns_time(df_previsao_X.columns)
    
    df_previsao_X = df_previsao_X[columns]
        
    # Execute prediction in test base, recording time series
    city_dict = {}
    df_result_consolidate = pd.DataFrame(columns=columns+[target_variable])
    
    # Loop with interval of number time steps to predict
    for t in list(range(0, df_previsao[    
        df_previsao == df_previsao['nome_bairro'].unique()[0]].shape[0])):
        
        for c in df_previsao['nome_bairro'].unique():
            
            c = 'nome_bairro_' + c
            # Create dataframe with region that will be analysed
            df = df_previsao_X[df_previsao_X[c] > 0]
            
            # Create a dict to record the results
            if not c in city_dict.keys(): 
                city_dict[c] = list()
                
            # TO DO: Implementation network
            
            # Record all predict values to build target column
            # city_dict[c].append(y_predict)
            
        # Consolidating all results
        df[target_variable] = city_dict[c]
        df = df[columns+[target_variable]]
        df_result_consolidate = df_result_consolidate.append(df, sort=False)
    
    # Inverting categorical columns
    for cat in categorical_columns:
        for c in df_previsao[cat].unique():
            df_result_consolidate.drop(columns=[cat+'_'+str(c)], inplace=True)
        df_result_consolidate[cat] = df_previsao[cat]
        
    df_result_consolidate['chave'] = df_previsao['chave']
    
    if not 'ano' in  df_result_consolidate.columns:
        df_result_consolidate['ano'] = df_previsao['ano']
        
    df_result_consolidate = df_result_consolidate[['chave', 'ano'] + \
                                                  columns_filtered+[target_variable]]
    
    return df_result_consolidate


def save_to_pickle(object, name_arq):

    arq = open(base_path+\
            '/data_sus/finais/shaps/{}'.format(name_arq), 'wb')
    pickle.dump(object, arq, pickle.HIGHEST_PROTOCOL)
    arq.close()


def read_from_pickle(name_arq):

    arq = open(base_path+\
            '/data_sus/finais/shaps/{}'.format(name_arq), 'rb')
    object = pickle.load(arq)

    return object


def get_indices_columns_time(columns):
    
    columns_time = list()
    columns_others = list()
    
    for c in columns:
        if re.match("t-.", c):
            columns_time.append(c)
        else:
            columns_others.append(c)

    if len(columns_time) > 0:
    
        index_time_cols = (len(columns_others),
                           len(columns_others)+len(columns_time)-1)

        columns_others = columns_others + columns_time

        return columns_others, index_time_cols

    else:
        return columns_others, list()

  
def backward_elimination(X_train, y_train, sl=0.05):
    
    elimitedColumns = list()

    # Creating a first column with ones
    X = np.append(arr=np.ones((len(X_train), 1)),
                  values=X_train, axis=1)
    columns = list(range(X.shape[1]))
    X_opt = X[:, columns]

    for i in range(len(X_opt[0])):
        regressor_OLS = sm.OLS(y_train, X_opt).fit()
        maxVar = float(max(regressor_OLS.pvalues))
        if maxVar > sl:
            for j in range(len(columns)):
                if regressor_OLS.pvalues[j].astype(float) == maxVar:
                    del columns[j]
                    X_opt = X[:, columns]
    # print(regressor_OLS.summary())

    return regressor_OLS, columns


def apply_future_prediction_old(regressor, df_previsao, columns_filtered,
                                target_variable,
                                categorical_columns=[],
                                scaler=False):
    df_previsao_X = df_previsao[columns_filtered]

    # If the base has categorical columns
    if not categorical_columns == []:
        df_previsao_X = pd.get_dummies(df_previsao_X, columns=categorical_columns)

    # If regressor used scaler
    if scaler:
        df_previsao_X = pd.DataFrame(scaler.transform(df_previsao_X),
                                     columns=df_previsao_X.columns)

    columns, index_time_cols = get_indices_columns_time(df_previsao_X.columns)

    df_previsao_X = df_previsao_X[columns]

    # Execute prediction in test base, recording time series
    city_dict = {}
    df_result_consolidate = pd.DataFrame(columns=columns + [target_variable])
    for c in df_previsao['nome_bairro'].unique():

        c = 'nome_bairro_' + c
        # Create dataframe with region that will be analysed
        df = df_previsao_X[df_previsao_X[c] > 0]

        # Create a dict to record the results
        city_dict[c] = list()

        # It will pass by all lines of the test base
        for i in list(range(0, df.shape[0])):

            # When it is not the first, record last prediction
            if not i == 0:

                # Put the last predicted value in t-1
                df.iloc[i, index_time_cols[0]] = y_predict

                # Shift values since t-2 until t-n
                for ct in list(range(index_time_cols[0], index_time_cols[1])):
                    df.iloc[i, ct + 1] = df.iloc[i - 1, ct]

                    # Predict new value
            df_temp = pd.DataFrame(np.array(df.iloc[i, :]).reshape(1, -1),
                                   columns=df.columns)
            y_predict = round(regressor.predict(df_temp)[0])

            # Record all predict values to build target column
            city_dict[c].append(y_predict)

        # Consolidating all results
        df[target_variable] = city_dict[c]
        df = df[columns + [target_variable]]
        df_result_consolidate = df_result_consolidate.append(df, sort=False)

    # Inverting categorical columns
    for cat in categorical_columns:
        for c in df_previsao[cat].unique():
            df_result_consolidate.drop(columns=[cat + '_' + str(c)], inplace=True)
        df_result_consolidate[cat] = df_previsao[cat]

    df_result_consolidate['chave'] = df_previsao['chave']

    if not 'ano' in df_result_consolidate.columns:
        df_result_consolidate['ano'] = df_previsao['ano']

    df_result_consolidate = df_result_consolidate[['chave', 'ano'] + \
                                                  columns_filtered + [target_variable]]

    return df_result_consolidate
