#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:25:43 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
from machine_learning.mlSupportMethods import *
from machine_learning.visualization import *
from machine_learning.mlMethods import run_lightGBM, run_randomforest
from machine_learning.preProcessing import read_and_get_input_v2

from treeinterpreter import treeinterpreter as ti, utils
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.model_selection import cross_validate
import math
from IPython import embed

base_path = os.getcwd()


######################
# Métodos utilizados #
######################

def analyse_r2_by_time_v3(m, df_treino, df_teste,
                       columns_filtered, categorical_columns,
                       columns_filtered_categorical, neighbor_columns, target):
    '''
    :param m: nome do modelo que será utilizado
    :param df_input_X: base de dados de treinamento x
    :param df_input_y: base de dados de treinamento y
    :param df_previsao_real: base de dados de teste
    :param columns_filtered: colunas que serão selecionadas na base de teste
    :param categorical_columns: quais colunas selecionadas são categoricas
    :param columns_filtered_categorical: das variáveis actegóricas, quais se mantém
    :param neighbor_columns: quais colunas de soma de vizinhos são mantidas
    :return: df com a análise de R2 por trimestre dentro do ano de teste
    '''

    X_train, y_train = prepare_base_treino(df_treino, columns_filtered, columns_filtered_categorical,
                            categorical_columns, target_variable=target)

    df_results = pd.DataFrame()

    for i in [1, 3, 6, 9, 12]:
        for n in list(range(10)):
            try:
                X_train_sample, X_test_sample, y_train_sample, y_test_sample = train_test_split(X_train,
                                                                    y_train,
                                                                    test_size=0.1,
                                                                    random_state=n)

                regressor, r2 = globals()["run_{}".format(m)](X_train_sample, y_train_sample)

                df_previsao = df_teste[df_teste['mes'] <= i]

                df_prev = apply_future_prediction_update_neighbors(regressor, df_previsao,
                                                  columns_filtered, columns_filtered_categorical,
                                                  list(X_train_sample.columns), categorical_columns,
                                                  neighbor_columns, target_variable=target)

                df_results = df_results.append(pd.Series(["t+" + str(i), "M_" + str(n + 1),
                                                          apply_adjusted_r2(df_treino, df_teste,
                                                          df_prev, target_variable=target)]), ignore_index=True)

            except Exception as ex:
                print("analyse_r2_by_time_v3: {}".format(ex))
                embed()

    df_results.columns = ["Time Prediction (Months)", "Model", "R2"]

    sns.boxplot(x='Time Prediction (Months)', y='R2',
                data=df_results)
    # sns.despine(offset=10, trim=True)
    plt.ylim(0, 1)
    plt.title("Model:{} (Ano: {})".format(m, df_teste['ano'].unique()[0]))
    plt.show()

    return df_results


def apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(df_treino, df_teste,
                                                                             target_variable='dengue_diagnosis'):
    '''
    Nesse caso o cálculo do denominador da formula considera a diferença com o valor médio para bairro, ou seja,
    no denominador é levado em consideração os dados de treino e teste
    '''

    df_treino[target_variable+'_previsto'] = [np.nan] * df_treino.shape[0]
    df_merge = df_treino.append(df_teste)

    df_adjusted_r2 = pd.DataFrame(columns=["city", "r2"])

    for b in list(df_merge['nome_bairro'].unique()):

        distance_avg_values = list()
        df_region_all_data = df_merge[df_merge["nome_bairro"] == b]
        df_region_all_data.index = df_region_all_data["chave"]

        # Caculando a variância geral (considerando dados de treino e teste) de cada região com a média
        for c in list(df_region_all_data["chave"]):
            distance_avg_values.append(
                pow(df_region_all_data.loc[c, target_variable]-np.mean(df_region_all_data[target_variable]), 2))

        distance_predicted_values = list()
        df_region = df_teste[df_teste["nome_bairro"] == b]
        df_region.index = df_region["chave"]

        # Calculo da variância para cada região com o resultado do modelo
        for c in list(df_region["chave"]):
            distance_predicted_values.append(
                pow(df_region.loc[c,  target_variable] - df_region.loc[c,  target_variable+'_previsto'], 2))

        # Calculo do R2 para cada região
        try:
            if np.sum(distance_avg_values) == 0 and np.sum(distance_predicted_values) == 0:
                adjusted_r2 = 1
            else:
                adjusted_r2 = 1 - ((np.sum(distance_predicted_values)/df_region.shape[0])/\
                                   (np.sum(distance_avg_values)/df_region_all_data.shape[0]))
                if str(adjusted_r2) == "-inf":
                    adjusted_r2 = 1 - (df_region[target_variable+'_previsto'].sum()/df_region_all_data.shape[0])
        except Exception as ex:
            embed()

        temp = pd.DataFrame([[b, round(adjusted_r2, 2)]], columns=["city", "r2"])
        df_adjusted_r2 = df_adjusted_r2.append(temp)

    df_adjusted_r2.sort_values(by=['r2'], inplace=True, ascending=False)

    return df_adjusted_r2


def apply_adjusted_r2(df_treino, df_teste, df_prev, target_variable='dengue_diagnosis'):
    '''
    Nesse caso o cálculo do denominador da formula considera a diferença com o valor médio para bairro, ou seja,
    no denominador é levado em consideração os dados de treino e teste

    Esse método calcula da mesma forma que o "apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data",
    aqui ele só foi adaptado para o método que calcula r2 no tempo "analyse_r2_by_time_v2"
    '''

    # Junta as bases de treino e de teste
    df_teste = df_teste[df_teste["mes"].isin(list(df_prev['mes'].unique()))]
    df_merge = df_treino.append(df_teste)

    # Cria a lista que ira armazenar os valores para aplicaçao da somatoria
    distance_avg_values = list()
    df_merge.index = df_merge["chave"]

    # Caculando a variância geral (considerando dados de treino e teste) de cada região com a média
    for c in list(df_merge["chave"]):
        distance_avg_values.append(
            pow(df_merge.loc[c, target_variable]-np.mean(df_merge[target_variable]), 2))

    distance_predicted_values = list()

    df_prev.index = df_prev["chave"]
    # Calculo da variância para cada região com o resultado do modelo
    for c in list(df_prev["chave"]):
        distance_predicted_values.append(
                pow(df_merge.loc[c,  target_variable] - df_prev.loc[c,  target_variable+'_previsto'], 2))

    # Calculo do R2 para cada região
    if np.sum(distance_avg_values) == 0 and np.sum(distance_predicted_values) == 0:
        adjusted_r2 = 1
    else:
        adjusted_r2 = 1 - ((np.sum(distance_predicted_values)/df_prev.shape[0])/\
                           (np.sum(distance_avg_values)/df_merge.shape[0]))
        if str(adjusted_r2) == "-inf":
            adjusted_r2 = 1 - (df_prev[target_variable+'_previsto'].sum()/df_merge.shape[0])

    return round(adjusted_r2, 2)


def apply_adjusted_r2_all_base(df_treino, df_teste, target_variable='dengue_diagnosis'):
    '''
    Nesse caso o cálculo do denominador da formula considera a diferença com o valor médio para bairro, ou seja,
    no denominador é levado em consideração os dados de treino e teste

    Esse método calcula da mesma forma que o "apply_adjusted_r2",
    aqui ele só foi adaptado para o cálculo da base como um todo
    '''

    # Cria a lista que ira armazenar os valores para aplicaçao da somatoria
    distance_avg_values = list()
    df_treino.index = df_treino["chave"]
    df_treino = df_treino[df_treino["ano"] < df_teste["ano"].unique()[0]]

    # Caculando a variância geral (considerando dados de treino e teste) de cada região com a média
    for c in list(df_treino["chave"]):
        distance_avg_values.append(
            pow(df_treino.loc[c, target_variable]-np.mean(df_treino[target_variable]), 2))

    distance_predicted_values = list()

    df_teste.index = df_teste["chave"]
    # Calculo da variância para cada região com o resultado do modelo
    for c in list(df_teste["chave"]):
        distance_predicted_values.append(
                pow(df_teste.loc[c,  target_variable] - df_teste.loc[c,  target_variable+'_previsto'], 2))

    # Calculo do R2 para cada região
    if np.sum(distance_avg_values) == 0 and np.sum(distance_predicted_values) == 0:
        adjusted_r2 = 1
    else:
        adjusted_r2 = 1 - ((np.sum(distance_predicted_values)/df_teste.shape[0])/\
                           (np.sum(distance_avg_values)/df_treino.shape[0]))
        if str(adjusted_r2) == "-inf":
            adjusted_r2 = 1 - (df_teste[target_variable+'_previsto'].sum()/df_treino.shape[0])

    df = pd.DataFrame([[round(adjusted_r2, 2),
                        round(mean_absolute_error(list(df_teste[target_variable]),
                                                  list(df_teste[target_variable+'_previsto'])), 2),
                        round(pow(mean_squared_error(list(df_teste[target_variable]),
                                                  list(df_teste[target_variable+'_previsto'])), 1/2), 2)]],
                      columns=["R2", "MAE", "RMSE"])

    return df


def apply_r2_standart_for_each_city_test_data(df_valores_previstos, df_previsao_real,
                                              target_variable='dengue_diagnosis'):
    '''
    Calculate r2 of each city
    '''

    df_r2 = pd.DataFrame(columns=['city', 'r2'])

    for i, c in enumerate(list(df_previsao_real['nome_bairro'].unique())):
        df_aux = pd.DataFrame({'city': [c], 'r2': [r2_score(
            y_true=df_previsao_real[df_previsao_real['nome_bairro'] == c][target_variable],
            y_pred=df_valores_previstos[df_valores_previstos['nome_bairro'] == c][target_variable+'_previsto'])]})
        df_r2 = df_r2.append(df_aux)

    df_r2.sort_values(by=['r2'], inplace=True, ascending=False)

    return df_r2


####################
# Métodos antigos  #
####################
def antigos():

    def apply_r2_for_each_city_train_data(regressor, df_input_X,
                                          df_input_y, df_input_ml, ml_type):
        '''
        From training data, calculate r2 of each city

        '''

        y_pred = regressor.predict(df_input_X)

        df_input_X['dengue_diagnosis_real'] = df_input_y
        df_input_X['dengue_diagnosis_prev'] = y_pred

        df_r2 = pd.DataFrame(columns=['city', 'r2'])

        for i, c in enumerate(list(df_input_ml['name_neighborhood'].unique())):
            df_aux = pd.DataFrame({'city': [c], 'r2': [r2_score(
                df_input_X[df_input_X['name_neighborhood_ ' +c] == 1]['dengue_diagnosis_real'],
                df_input_X[df_input_X['name_neighborhood_ ' +c] == 1]['dengue_diagnosis_prev'])]})
            df_r2 = df_r2.append(df_aux)

        df_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        df_r2.to_csv(base_path+ \
                     '/data_sus/finais/analise_train_' + '2008_2014_' + ml_type + '_r2_por_bairro.csv')


    def apply_explained_variance_score_for_each_city_test_data(df_valores_previstos, df_previsao_real):
        '''
        Calculate r2 of each city

        '''

        df_r2 = pd.DataFrame(columns=['city', 'r2'])

        # for i, c in enumerate(list(df_previsao_real['nome_bairro'].unique())):
        #     df_aux = pd.DataFrame({'city': [c], 'r2': [explained_variance_score(
        #         df_previsao_real[df_previsao_real['nome_bairro'] == c]['dengue_diagnosis'],
        #         df_valores_previstos[df_valores_previstos['nome_bairro'] == c]['dengue_diagnosis'])]})
        #     df_r2 = df_r2.append(df_aux)

        df_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        return df_r2


    def apply_r2_in_validate_database(df_previsao, df_prev):

        print(r2_score(df_previsao['dengue_diagnosis'], df_prev['dengue_diagnosis']))


    def study_variables_by_r2():
        features = ['qtd_cnes', 'qtd_serv_atencao_basica', 'qtd_serv_ACS_program',
                    'qtd_serv_prenatal', 'qtd_serv_neonatal',
                    'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus',
                    'temp_solo', 'precipitacao', 't-1', 't-2', 't-3']

        df_prev = pd.read_csv(
            base_path + \
            '/data_sus/intermediarias/previsao_rf_dengue_considerando_mes_2015_2017_to_plot.csv')

        df_r2 = pd.read_csv(base_path + \
                            '/data_sus/finais/analise_test_2015_2017_plot_rf_r2_por_bairro.csv')

        df_r2['category'] = df_r2['r2'].apply(
            lambda x: "(0,)" if x > 0 else "(0, -0.5)" if x > -0.5 else "(,-0.5)")

        df = pd.merge(df_prev, df_r2,
                      left_on="nome_bairro", right_on="city",
                      how="left")

        for c in features:
            sns.boxplot(x="category", y=c,
                        data=df)
            sns.despine(offset=10, trim=True)
            plt.show()


    def apply_adjusted_r2_for_each_region_test_data_variance_all_city(df_treino, df_teste,
                                                                      target_variable='dengue_diagnosis'):
        '''
        Nesse caso o cálculo do denominador da formula considera a diferença com o valor médio para todo o municipio do
        rio de janeiro, ao contrario do outro método com no denominador considera a diferença de cada bairro somente
        '''

        df_treino[target_variable+'_previsto'] = [np.nan] * df_treino.shape[0]
        df_merge = df_treino.append(df_teste)

        distance_avg_values = list()
        index_target_variable = list(df_merge.columns).index(target_variable)

        # Caculando a variância geral (considerando dados de treino e teste) com a média
        for i in list(range(df_merge.shape[0])):
            distance_avg_values.append(
                pow(df_merge.iloc[i, [index_target_variable]]-np.mean(df_merge[target_variable]), 2))

        df_adjusted_r2 = pd.DataFrame(columns=["city", "r2"])

        # Calculando a variância de cada região com o resultado do modelo e R2
        for b in list(df_teste['nome_bairro'].unique()):

            distance_predicted_values = list()
            df_region = df_teste[df_teste["nome_bairro"] == b]
            df_region.index = df_region["chave"]

            # Calculo da variância para cada região com o resultado do modelo
            for c in list(df_region["chave"]):
                distance_predicted_values.append(pow(df_region.loc[c,  target_variable] - df_region.loc[c,  target_variable+'_previsto'], 2))

            # Calculo do R2 para cada região
            adjusted_r2 = 1 - (np.sum(distance_predicted_values)/df_region.shape[0])/(np.sum(distance_avg_values)/df_merge.shape[0])
            temp = pd.DataFrame([[b, adjusted_r2]], columns=["city", "r2"])
            df_adjusted_r2 = df_adjusted_r2.append(temp)

        df_adjusted_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        return df_adjusted_r2



    def apply_adjusted_r2_for_each_city_test_data_with_root_square(df_valores_previstos, df_previsao_real,
                                                                   target_variable='dengue_diagnosis'):
        '''
        Nesse caso o cálculo do denominador da formula considera a diferença com o valor médio para todo o municipio do
        rio de janeiro, ao contrario do outro método com no denominador considera a diferença de cada bairro somente
        '''

        df_adjusted_r2 = pd.DataFrame(columns=["city", "r2"])

        # Calculando o R² de cada bairro
        for b in list(df_previsao_real['nome_bairro'].unique()):

            # Cria lista para armazenamento dos calculos
            distance_predicted_values = list()
            distance_avg_values = list()

            # Filtra a região de análise dos dataframes de valor real e previsto
            real_values = df_previsao_real[df_previsao_real["nome_bairro"] == b]
            forecast_values = df_valores_previstos[df_valores_previstos["nome_bairro"] == b]

            # Inseri a chave o index do dataframe para a consulta
            real_values.index = real_values["chave"]
            forecast_values.index = forecast_values["chave"]

            # Calcula o numerador (diferença com o previsto) e denominador (diferença com a média) do R2
            for c in list(real_values["chave"]):
                distance_predicted_values.append(pow(real_values.loc[c,  target_variable] - forecast_values.loc[c,  target_variable], 2))
                distance_avg_values.append(pow(real_values.loc[c,  target_variable] - np.mean(real_values[target_variable]),2))

            # Calcula do R2 ajustado e armazena no dataframe de resultado final
            if np.sum(distance_avg_values) == 0 and np.sum(distance_predicted_values) == 0:
                adjusted_r2 = 1
            else:
                adjusted_r2 = 1 - np.sum(distance_predicted_values)/np.sum(distance_avg_values)
                if str(adjusted_r2) == "-inf":
                    adjusted_r2 = 0

            temp = pd.DataFrame([[b, adjusted_r2]], columns=["city", "r2"])
            df_adjusted_r2 = df_adjusted_r2.append(temp)

        df_adjusted_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        return df_adjusted_r2


    def apply_r2_for_each_city_normalized_test_data(df_valores_previstos, df_previsao_real):
        '''
        Calculate r2 of each city
        '''

        # R2 com normalização
        # df_real, scaler = apply_feature_scaling_specific_columns(df_previsao_real, ['dengue_diagnosis'])
        # df_valores_previstos['dengue_diagnosis_scaling'] = scaler.transform(
        #     np.array(df_valores_previstos['dengue_diagnosis']).reshape(-1, 1))
        #
        # df_real['dengue_diagnosis_scaling'] = df_real['dengue_diagnosis_scaling'].apply(
        #     lambda x: round(x, 2))
        # df_valores_previstos['dengue_diagnosis_scaling'] = df_valores_previstos['dengue_diagnosis_scaling'].apply(
        #     lambda x: round(x, 2))
        #
        # df_r2 = pd.DataFrame(columns=['city', 'r2'])
        #
        # for i, c in enumerate(list(df_real['nome_bairro'].unique())):
        #     df_aux = pd.DataFrame({'city': [c], 'r2': [r2_score(
        #         df_real[df_real['nome_bairro'] == c]['dengue_diagnosis_scaling'],
        #         df_valores_previstos[df_valores_previstos['nome_bairro'] == c]['dengue_diagnosis_scaling'])]})
        #     df_r2 = df_r2.append(df_aux)

        # df_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        # return df_r2


    def apply_adjusted_r2_for_each_city_test_data_with_absolute_value(df_valores_previstos, df_previsao_real,
                                                                      target_variable='dengue_diagnosis'):

        df_adjusted_r2 = pd.DataFrame(columns=["city", "r2"])

        # Calculando o R² de cada bairro
        for b in list(df_previsao_real['nome_bairro'].unique()):

            # Cria lista para armazenamento dos calculos
            distance_predicted_values = list()
            distance_avg_values = list()

            # Filtra a região de análise dos dataframes de valor real e previsto
            real_values = df_previsao_real[df_previsao_real["nome_bairro"] == b]
            forecast_values = df_valores_previstos[df_valores_previstos["nome_bairro"] == b]

            # Inseri a chave o index do dataframe para a consulta
            real_values.index = real_values["chave"]
            forecast_values.index = forecast_values["chave"]

            # Calcula o numerador (diferença com o previsto) e denominador (diferença com a média) do R2
            for c in list(real_values["chave"]):
                distance_predicted_values.append(abs(real_values.loc[c,  target_variable] - forecast_values.loc[c,  target_variable]))
                distance_avg_values.append(abs(real_values.loc[c,  target_variable] - np.mean(real_values[target_variable])))

            # Calcula do R2 ajustado e armazena no dataframe de resultado final
            if np.sum(distance_avg_values) == 0 and np.sum(distance_predicted_values) == 0:
                adjusted_r2 = 1
            else:
                adjusted_r2 = 1 - np.sum(distance_predicted_values)/np.sum(distance_avg_values)
                if str(adjusted_r2) == "-inf":
                    adjusted_r2 = 0

            temp = pd.DataFrame([[b, adjusted_r2]], columns=["city", "r2"])
            df_adjusted_r2 = df_adjusted_r2.append(temp)

        df_adjusted_r2.sort_values(by=['r2'], inplace=True, ascending=False)

        return df_adjusted_r2


    def analyse_r2_by_time():

        def predict_test_base_just_one_time_step():

            df_previsao = df_previsao_real[df_previsao_real['mes'] <= 1]

            df_aux = df_previsao[columns_filtered]

            df_aux = pd.get_dummies(df_aux, columns=categorical_columns)

            # Alternative 1
            y_predicted_batch = regressor_rf.predict(df_aux)

            # Alternative 2
            y_predicted = list()
            for i in list(range(df_aux.shape[0])):
                df = pd.DataFrame(np.array(df_aux.iloc[i, :]).reshape(1, -1), columns=df_aux.columns)
                y_predicted.append(round(regressor_rf.predict(df)[0]))

            # Collecting results
            df_aux['dengue_diagnosis_1'] = y_predicted_batch

            df_aux['dengue_diagnosis_1'] = df_aux['dengue_diagnosis_1'].apply(
                lambda x: round(x))

            df_aux['dengue_diagnosis_2'] = y_predicted

            df_aux['dengue_diagnosis_2'] = df_aux['dengue_diagnosis_2'].apply(
                lambda x: round(x))

            df_aux['nome_bairro'] = df_previsao['nome_bairro']

            r2_score(df_previsao['dengue_diagnosis'],
                     y_predicted_batch)

            r2_score(df_previsao['dengue_diagnosis'],
                     y_predicted)

        df_input = read_and_get_input_v2(os.getcwd() + \
                                         '/data_sus/finais/input_ml_ocorrencia_doencas_v2.csv')

        df_input_ml = df_input[df_input['ano'].isin(
            list(range(2014, 2017, 1)))]
        df_previsao_real = df_input[df_input['ano'].isin(
            list(range(2017, 2018, 1)))]
        df_previsao_real = df_previsao_real[df_input['mes'].isin(
            list(range(1, 13, 1)))]

        # columns_filtered = ['nome_bairro', 'qtd_cnes',
        #                     'qtd_serv_atencao_basica', 'qtd_serv_ACS_program',
        #                     'qtd_serv_prenatal', 'qtd_serv_neonatal',
        #                     'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus',
        #                     'temp_solo', 'precipitacao', 't-1', 't-2', 't-3',
        #                     't-4', 't-5', 't-6','t-7', 't-8', 't-9','t-10', 't-11', 't-12']

        columns_filtered = ['nome_bairro', 'mes',
                            'temp_solo', 'precipitacao', 't-1', 't-2', 't-3',
                            't-4', 't-5', 't-6']

        categorical_columns = ['nome_bairro', 'mes']

        df_input_X = df_input_ml[columns_filtered]

        # Categorical values
        if not categorical_columns == []:
            df_input_X = pd.get_dummies(df_input_X, columns=categorical_columns)

        columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_input_X.columns)

        df_input_X = df_input_X[columns_filtered_ordered]

        df_input_y = df_input_ml['dengue_diagnosis']

        df_results = pd.DataFrame()

        for i in [1, 3, 6, 9, 12]:
            for n in list(range(10)):
                X_train, X_test, y_train, y_test = train_test_split(df_input_X,
                                                                    df_input_y,
                                                                    test_size=0.1,
                                                                    random_state=n)

                regressor_rf = run_randomforest(X_train, y_train)

                df_previsao = df_previsao_real[df_previsao_real['mes'] <= i]

                df_prev = apply_future_prediction(regressor_rf, df_previsao,
                                                  columns_filtered,
                                                  'dengue_diagnosis',
                                                  categorical_columns)

                df_results = df_results.append(pd.Series(["t+" + str(i), "M_" + str(n + 1),
                                                          r2_score(df_previsao['dengue_diagnosis'],
                                                                   df_prev['dengue_diagnosis'])]), ignore_index=True)

        df_results.columns = ["Time Prediction (Months)", "Model", "R2"]

        sns.boxplot(x='Time Prediction (Months)', y='R2',
                    data=df_results)
        # sns.despine(offset=10, trim=True)
        # plt.ylim(-0.3, 1)
        plt.show()

        return df_results


    def analyse_r2_by_time_v2(m, df_treino, df_teste,
                           columns_filtered, categorical_columns,
                           columns_filtered_categorical, neighbor_columns):
        '''
        :param m: nome do modelo que será utilizado
        :param df_input_X: base de dados de treinamento x
        :param df_input_y: base de dados de treinamento y
        :param df_previsao_real: base de dados de teste
        :param columns_filtered: colunas que serão selecionadas na base de teste
        :param categorical_columns: quais colunas selecionadas são categoricas
        :param columns_filtered_categorical: das variáveis actegóricas, quais se mantém
        :param neighbor_columns: quais colunas de soma de vizinhos são mantidas
        :return: df com a análise de R2 por trimestre dentro do ano de teste
        '''

        df_results = pd.DataFrame()

        for i in [1, 3, 6, 9, 12]:
            for n in list(range(10)):

                # try:
                df_treino_copy = df_treino[df_treino['mes'] <= i]

                X_train = df_treino_copy[columns_filtered]
                y_train = df_treino_copy['dengue_diagnosis']

                X_train = pd.get_dummies(X_train, columns=categorical_columns)

                # Para o caso de haver variáveis categoricas que serão selecionadas
                if not columns_filtered_categorical == []:

                    # Tratativa especial para o caso de mês
                    if 'mes' in categorical_columns:

                        # Irá buscar somente os meses que estão na base de treino, pode ir de 1 a 12
                        mes_list = ["mes_{}".format(i) for i in list(df_treino["mes"].unique())]
                        categorical_list = [a for a in categorical_columns if not re.match(".*mes.*", a)]

                        # Se na lista de variáveis categoricas não tivessem somente o mês
                        if len(categorical_list) > 0:
                            columns_filtered_categorical = categorical_list + mes_list
                        else:
                            columns_filtered_categorical = mes_list
                        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
                        X_train = X_train[new_columns_features + columns_filtered_categorical]
                    else:
                        new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
                        X_train = X_train[new_columns_features + columns_filtered_categorical]

                columns, index_time_cols = get_indices_columns_time(X_train.columns)
                X_train = X_train[columns]

                X_train_sample, X_test_sample, y_train_sample, y_test_sample = train_test_split(X_train,
                                                                    y_train,
                                                                    test_size=0.1,
                                                                    random_state=n)

                # regressor, r2 = globals()["run_{}".format(m)](X_train_sample, y_train_sample)
                regressor, r2 = run_lightGBM(X_train_sample, y_train_sample)
                # regressor, r2 = run_randomforest(X_train_sample, y_train_sample)

                df_previsao = df_teste[df_teste['mes'] <= i]

                df_prev = apply_future_prediction_update_neighbors(regressor, df_previsao,
                                                  columns_filtered, 'dengue_diagnosis', columns_filtered_categorical,
                                                  categorical_columns, neighbor_columns, False)

                df_results = df_results.append(pd.Series(["t+" + str(i), "M_" + str(n + 1),
                                                          apply_adjusted_r2(df_treino, df_teste,
                                                          df_prev)]), ignore_index=True)

                # df_results = df_results.append(pd.Series(["t+"+str(i), "M_"+str(n+1),
                #                                         r2_score(df_previsao['dengue_diagnosis'],
                #                                                 df_prev['dengue_diagnosis_previsto'])]),
                #                                         ignore_index=True)


        df_results.columns = ["Time Prediction (Months)", "Model", "R2"]

        sns.boxplot(x='Time Prediction (Months)', y='R2',
                    data=df_results)
        # sns.despine(offset=10, trim=True)
        plt.ylim(0, 1)
        plt.title(m)
        plt.show()

        return df_results

