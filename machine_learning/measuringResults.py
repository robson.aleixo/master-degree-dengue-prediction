#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:25:43 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
from treeinterpreter import treeinterpreter as ti, utils
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.model_selection import cross_validate
import seaborn as sns
from matplotlib import pyplot as plt
import shap
from datetime import date
from IPython import embed

from machine_learning.preProcessing import read_and_get_input
from machine_learning.visualization import plot_two_lines
from machine_learning.mlSupportMethods import get_indices_columns_time, save_to_pickle

base_path = os.getcwd()


##########################
# Métodos utilizados     #
##########################
def apply_shap_values(X_train, regressor, m, base_path):

    if m in ['randomforest', "extratrees", "xgboost", "lightGBM"]:
        explainer = shap.TreeExplainer(regressor)
        shap_values = explainer.shap_values(X_train, check_additivity=False)
    elif m in ['svr']:
        explainer = shap.KernelExplainer(regressor.predict_proba, X_train, link="logit")
        shap_values = explainer.shap_values(X_train, nsamples=5)
    elif m in ["linear_regression", "polynomial_regression"]:
        explainer = shap.LinearExplainer(regressor, X_train, feature_dependence="independent")
        shap_values = explainer.shap_values(X_train)

    df_shap_values_1 = pd.DataFrame(shap_values, columns=list(X_train.columns))

    list_importance = list()

    for c in list(df_shap_values_1.columns):
        df_shap_values_1[c] = df_shap_values_1[c].apply(lambda x: abs(x))
        list_importance.append(np.mean(df_shap_values_1[c]))

    df_feature_importance = pd.DataFrame(columns=["feature", "importance"])
    df_feature_importance["feature"] = list(X_train.columns)
    df_feature_importance["importance"] = list_importance
    df_feature_importance["importance_perc"] = df_feature_importance["importance"].apply(
        lambda x: round(x/np.sum(df_feature_importance["importance"]), 2))
    df_feature_importance.sort_values(by=["importance"], inplace=True, ascending=False)

    df_feature_importance.to_csv(base_path+\
            '/data_sus/finais/feature_importances/feature_importance_{}.csv'.format(m))

    f, ax = plt.subplots(figsize=(15, 15))
    shap.summary_plot(shap_values, X_train)

    return df_feature_importance


def apply_cross_validate(X_train, y_train, regressor, name):
    scoring = {'r2': 'r2',
               'neg_root_mean_squared_error': 'neg_root_mean_squared_error',
               'neg_mean_absolute_error': 'neg_mean_absolute_error'}

    accuracies = cross_validate(estimator=regressor,
                                X=X_train,
                                y=y_train,
                                scoring=scoring,
                                cv=10)

    print("####################### {} ###################".format(name))
    print('test mean r2: {}'.format(round(accuracies['test_r2'].mean(), 2)))
    print('test std r2: {}'.format(round(accuracies['test_r2'].std(), 2)))
    print(
        'test mean root_mean_squared_error: {}'.format(round(accuracies['test_neg_root_mean_squared_error'].mean(), 2)))
    print('test std root_mean_squared_error: {}'.format(round(accuracies['test_neg_root_mean_squared_error'].std(), 2)))
    print('test mean mean_absolute_error: {}'.format(round(accuracies['test_neg_mean_absolute_error'].mean(), 2)))
    print('test std mean_absolute_error: {}'.format(round(accuracies['test_neg_mean_absolute_error'].std(), 2)))

    return round(accuracies['test_r2'].mean(), 2)


def apply_adjusted_cross_validate(X_train, y_train, regressor, name):
    # Listas onde serão armazenados os resultados
    result_r_squared = list()
    mae = list()
    rmse = list()

    # Define quantidade que vai para teste, nesse caso 10%
    bucket = int(0.1 * X_train.shape[0])

    # Identificaçao das linhas para poder separar em treino e teste
    X_train['id'] = list(range(X_train.shape[0]))
    df_y_train = pd.DataFrame(list(y_train), columns=['values'])
    df_y_train['id'] = list(range(X_train.shape[0]))

    for i in list(range(10)):

        # Seleciona os 10% da base para teste
        X_test = X_train.iloc[bucket * i:bucket * (i + 1), :]
        df_y_test = df_y_train.iloc[bucket * i:bucket * (i + 1), :]
        y_test = list(df_y_test['values'])

        # Seleciona os 90% de treinamento
        X_train_sample = X_train[~X_train['id'].isin(list(X_test['id']))]
        y_train_sample = list(df_y_train[~df_y_train['id'].isin(list(df_y_test['id']))]['values'])

        # Elimina a coluna de identificação
        X_train_sample.drop(columns=['id'], inplace=True)
        X_test.drop(columns=['id'], inplace=True)

        # Ajusta o modelo com a base de treinamento
        regressor.fit(X_train_sample, y_train_sample)

        # Previsão da base de teste
        y_pred = list(regressor.predict(X_test))

        variance_model = list()
        variance_avg = list()

        for i in list(range(len(y_pred))):
            variance_model.append(pow(y_test[i] - y_pred[i], 2))

        y_train = list(y_train)
        for i in list(range(len(y_train))):
            variance_avg.append(pow(y_train[i] - np.mean(y_train), 2))

        r_square_adjusted = 1 - (np.sum(variance_model) / np.sum(variance_avg))

        result_r_squared.append(round(r_square_adjusted, 2))
        mae.append(round(mean_absolute_error(y_test, y_pred), 2))
        rmse.append(round(np.sqrt(mean_squared_error(y_test, y_pred)), 2))

    print("######################## {} ########################".format(name))
    print('adjusted_r_square: ({}, {})'.format(round(np.mean(result_r_squared), 2), round(np.std(result_r_squared), 2)))
    print('mae: ({}, {})'.format(round(np.mean(mae), 2), round(np.std(mae), 2)))
    print('rmse: ({}, {})'.format(round(np.mean(rmse), 2), round(np.std(rmse), 2)))


def apply_grid_search(X_train, y_train, parameters_grid, regressor):
    grid_search = GridSearchCV(estimator=regressor,
                               param_grid=parameters_grid,
                               scoring='r2',
                               cv=10,
                               n_jobs=-1
                               )

    grid_search = grid_search.fit(X_train, np.ravel(y_train))
    print('best precision: {}'.format(round(grid_search.best_score_, 2)))
    print('best_parameters: {}'.format(grid_search.best_params_))

    return grid_search.best_params_


############################
# Métodos Antigos          #
############################

def create_df_with_real_and_prev_results_by_city(regressor, df_input_X,
                                                 df_input_y, df_input_ml, city_columns):
    '''
    Create dataframe with the real and predicted values
    '''

    y_pred = regressor.predict(df_input_X)

    df_input_X['dengue_diagnosis_real'] = df_input_y
    df_input_X['dengue_diagnosis_prev'] = y_pred

    df_analysis_city = df_input_X.copy()

    df_analysis_city['name_neighborhood'] = df_analysis_city[city_columns].idxmax(axis=1)

    df_analysis_city['name_neighborhood'] = df_analysis_city['name_neighborhood'].apply(
        lambda x: x.replace('name_neighborhood_', ''))

    df_analysis_city['dengue_diagnosis_prev'] = df_analysis_city['dengue_diagnosis_prev'].apply(
        lambda x: round(x))

    df_analysis_city = df_analysis_city[['name_neighborhood', 'dengue_diagnosis_real',
                                         'dengue_diagnosis_prev']]

    df_analysis_city['year'] = list(df_input_ml['ano'])
    df_analysis_city['month'] = list(df_input_ml['mes'])

    df_analysis_city['date'] = df_analysis_city[['year', 'month']].apply(
        lambda x: date(x[0], x[1], 1) , axis=1)

    df_analysis_city.to_csv(
        base_path+\
            '/data_sus/intermediarias/valores_reais_provisionados_por_cidade_ano.csv',
        decimal=',', float_format='%.0f')

    return df_analysis_city


def make_features_importances_df(df_input_X, regressor, base_path, ml_type):
    
    df_features = pd.DataFrame()

    df_features['features'] = list(df_input_X.columns)
    df_features['importances'] = list(regressor.feature_importances_)
    df_features.sort_values(by='importances', inplace=True, ascending=False)
    df_features.to_csv(base_path + \
                       '/data_sus/finais/features_importances_'+ ml_type + '_previsao_dengue_by_variable.csv')


    categoricos = ['t-', 'nome_bairro', 'mes']

    def return_category(x, categoricos):

        result = ''
        for c in categoricos:
            if re.match('.*' + c + '.*', x):
                if c == 't-':
                    return 'num_historico'
                else:
                    return c

        if result == '':
            return x

    df_features['category'] = df_features['features'].apply(
        lambda x: return_category(x, categoricos))

    df_features_pivot = pd.pivot_table(df_features, index=['category'],
                                       values=['importances'], aggfunc=np.sum)

    df_features_pivot.sort_values(by=['importances'], ascending=False,
                                  inplace=True)
    
    df_features_pivot.to_csv(base_path + \
                '/data_sus/finais/feature_importances/features_importances_'+ ml_type + '_previsao_dengue_by_category.csv')


def study_variables(cities_to_plot):
    
    df_input, df_input_ml, df_previsao = read_and_get_input()
    df_input = df_input[df_input['ano'] <= 2017]
    variables = ['temp_solo', 'precipitacao', 'dengue_diagnosis', 'umidade', 'qtd_cnes',
       'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 'qtd_serv_prenatal',
       'qtd_serv_neonatal', 'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus']
    city_one = 2
    city_two = -3
    
    for v in variables:
        plot_two_lines(df_input[df_input['nome_bairro'] == cities_to_plot[city_one]][v].values, 
                       df_input[df_input['nome_bairro'] == cities_to_plot[city_two]][v].values, 
                       title=v, 
                       yreal_label=cities_to_plot[city_one], 
                       yhat_label=cities_to_plot[city_two])


def cal_max_mean_median_forecast_versus_real(df_valores_previstos, df_previsao_real, df_r2):

    df_diff = df_valores_previstos[['chave', 'nome_bairro', 'dengue_diagnosis']]
    df_diff.columns = ["chave", "nome_bairro", "dengue_diagnosis_forecast"]
    df_diff = pd.merge(df_diff, df_previsao_real[["chave", "dengue_diagnosis"]], on="chave")

    df_diff["Diference"] = df_diff[["dengue_diagnosis_forecast", "dengue_diagnosis"]].apply(
        lambda x: abs(x[1] - x[0]), axis=1)

    df_diff_pivot = df_diff.pivot_table(index=["nome_bairro"], values=["Diference"], aggfunc="max")
    df_diff_pivot = df_diff_pivot.reset_index()
    df_diff_pivot.columns = ["nome_bairro", "Diference_max"]

    df_aux = df_diff.pivot_table(index=["nome_bairro"], values=["Diference"], aggfunc="mean")
    df_aux = df_aux.reset_index()
    df_aux.columns = ["nome_bairro", "Diference_mean"]
    df_diff_pivot = pd.merge(df_diff_pivot, df_aux, on="nome_bairro")

    df_aux = df_diff.pivot_table(index=["nome_bairro"], values=["Diference"], aggfunc="median")
    df_aux = df_aux.reset_index()
    df_aux.columns = ["nome_bairro", "Diference_median"]
    df_diff_pivot = pd.merge(df_diff_pivot, df_aux, on="nome_bairro")

    df_diff_pivot = pd.merge(df_diff_pivot, df_r2, left_on="nome_bairro", right_on="city")

    df_diff.drop(columns=["city"], inplace=True)
    df_diff = df_diff[['nome_bairro', 'r2', 'Diference_max', 'Diference_mean', 'Diference_median']]
    df_diff.sort_values(by=["r2", "Diference_max"], ascending=False, inplace=True)

    return df_diff_pivot


def confidence_interval_and_pearson_correlation(df_teste, df_valores_previstos):

    ##################
    # Interval Confidence
    #################
    import statsmodels.stats.api as sms
    interval = sms.DescrStatsW(df_teste['diff']).tconfint_mean()
    import numpy as np, scipy.stats as st
    interval_v2 = st.t.interval(0.95, len(df_teste['diff']) - 1, loc=np.mean(df_teste['diff']), scale=st.sem(df_teste['diff']))

    ###################
    # Pearson Correlation
    ##################
    from scipy.stats import pearsonr
    r2_pearson = pow(pearsonr(df_teste['dengue_diagnosis'], df_valores_previstos['dengue_diagnosis'])[0],2)


###############################
# Appling  Feature Selection  #
###############################
def apply_treeinterpreter(df_treino, regressor, target_variable, regiao_bom_desempenho, regiao_baixo_desempenho,
                          columns_filtered, categorical_columns):
    ''':except
    É passado aqui quais as regiões queremos ter os resultados analisados
    '''


    df_treino_inte = df_treino[columns_filtered+[target_variable]]
    df_treino_X = df_treino_inte[columns_filtered]

    # Categorical values
    if not categorical_columns == []:
        df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)

    columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)

    df_treino_X = df_treino_X[columns_filtered_ordered]

    df_treino_X[target_variable+'_predicted'] = regressor.predict(df_treino_X)
    df_treino_X[target_variable + '_predicted'] = df_treino_X[target_variable+'_predicted'].apply(lambda x: round(x))
    df_treino_X[target_variable] = df_treino[target_variable]
    df_treino_X["diff"] = df_treino_X[[target_variable+'_predicted', target_variable]].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    if 'nome_bairro' in categorical_columns:

        max_diff = np.max(df_treino_X[df_treino_X['nome_bairro_'+regiao_baixo_desempenho] == 1]['diff'])
        min_diff = np.min(df_treino_X[df_treino_X['nome_bairro_'+regiao_bom_desempenho] == 1]['diff'])

        selected_rows = [(regiao_bom_desempenho,
                          list(df_treino_X[df_treino_X['nome_bairro_'+regiao_bom_desempenho] == 1]['diff']).index(min_diff)),
                         (regiao_baixo_desempenho,
                          list(df_treino_X[df_treino_X['nome_bairro_'+regiao_baixo_desempenho] == 1]['diff']).index(max_diff))]

        df_treino_X_interpret = df_treino_X.drop(columns=[target_variable, target_variable + '_predicted', "diff"])

        selected_df_a = df_treino_X_interpret[df_treino_X_interpret['nome_bairro_'+regiao_bom_desempenho] == 1].iloc[selected_rows[0][1], :].values
        selected_df_b = df_treino_X_interpret[df_treino_X_interpret['nome_bairro_'+regiao_baixo_desempenho] == 1].iloc[selected_rows[1][1], :].values

        selected_df = np.array([list(selected_df_a), list(selected_df_b)])

        prediction, bias, contributions = ti.predict(regressor, selected_df)

        for n, i in enumerate(selected_rows):
            print("Região: ", i[0])
            print("Prediction:",
                  list(df_treino_X[df_treino_X['nome_bairro_'+i[0]] == 1][target_variable + '_predicted'])[i[1]],
                  'Actual Value:', list(df_treino_X[df_treino_X['nome_bairro_'+i[0]] == 1][target_variable])[i[1]])
            print("Bias (trainset mean)", bias[n])
            print("Feature contributions:")
            for c, feature in sorted(zip(contributions[n],
                                         df_treino_X_interpret.columns),
                                     key=lambda x: -abs(x[0])):
                print(feature, round(c, 2))
            print("-" * 20)

    else:
        print("tem que fazer")
        # max_diff = np.max(df_treino_X[df_treino_X['nome_bairro'] == regiao_bom_desempenho]['diff'])
        # min_diff = np.min(df_treino_X[df_treino_X['nome_bairro'] == regiao_baixo_desempenho]['diff'])
        #
        # selected_rows = [(regiao_bom_desempenho,
        #                   list(df_treino_X[df_treino_X['nome_bairro'] == regiao_bom_desempenho]['diff']).index(min_diff)),
        #                  (regiao_baixo_desempenho,
        #                   list(df_treino_X[df_treino_X['nome_bairro'] == regiao_baixo_desempenho]['diff']).index(max_diff))]
        #


def apply_treeinterpreter_geral(df_treino, regressor, target_variable,
                          columns_filtered, categorical_columns):
    ''':except
    Diferente do método anterior, olha para o melhor e pior resultado de forma geral, sem separar por região
    '''


    df_treino_inte = df_treino[columns_filtered+[target_variable]]
    df_treino_X = df_treino_inte[columns_filtered]

    # Categorical values
    if not categorical_columns == []:
        df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)

    columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)

    df_treino_X = df_treino_X[columns_filtered_ordered]

    df_treino_X[target_variable+'_predicted'] = regressor.predict(df_treino_X)
    df_treino_X[target_variable + '_predicted'] = df_treino_X[target_variable+'_predicted'].apply(lambda x: round(x))
    df_treino_X[target_variable] = df_treino[target_variable]
    df_treino_X["diff"] = df_treino_X[[target_variable+'_predicted', target_variable]].apply(
        lambda x: abs(x[0] - x[1]), axis=1)


    max_diff = np.max(df_treino_X['diff'])
    min_diff = np.min(df_treino_X['diff'])

    selected_rows = [list(df_treino_X['diff']).index(min_diff),
                     list(df_treino_X['diff']).index(max_diff)]

    df_treino_X_interpret = df_treino_X.drop(columns=[target_variable, target_variable + '_predicted', "diff"])

    selected_df = df_treino_X_interpret.iloc[[selected_rows[0][1], selected_rows[1][1]], :].values

    prediction, bias, contributions = ti.predict(regressor, selected_df)

    for n, i in enumerate(selected_rows):
        print("Região: ", i[0])
        print("Prediction:",
              list(df_treino_X[df_treino_X['nome_bairro_'+i[0]] == 1][target_variable + '_predicted'])[i[1]],
              'Actual Value:', list(df_treino_X[df_treino_X['nome_bairro_'+i[0]] == 1][target_variable])[i[1]])
        print("Bias (trainset mean)", bias[n])
        print("Feature contributions:")
        for c, feature in sorted(zip(contributions[n],
                                     df_treino_X_interpret.columns),
                                 key=lambda x: -abs(x[0])):
            print(feature, round(c, 2))
        print("-" * 20)


def apply_shap_treeexplainer(df_treino, df_teste, regressor, columns_filtered,
                           categorical_columns, target_variable="dengue_diagnosis"):

    save_to_pickle(regressor, "regressor_v1")
    df_treino_X = df_treino[columns_filtered]
    df_teste_X = df_teste[columns_filtered]

    # Categorical values
    if not categorical_columns == []:
        df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)
        df_teste_X = pd.get_dummies(df_teste_X, columns=categorical_columns)

    columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)
    df_treino_X = df_treino_X[columns_filtered_ordered]
    df_teste_X = df_teste_X[columns_filtered_ordered]

    df_teste.drop(columns=['dengue_diagnosis_previsto', 'diff'], inplace=True)
    df_teste['dengue_diagnosis_previsto'] = regressor.predict(df_teste_X)
    df_teste['diff'] = df_teste[['dengue_diagnosis', 'dengue_diagnosis_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    # Applying to train data
    explainer_train = shap.TreeExplainer(regressor, df_treino_X,
                                   feature_perturbation="interventional")
    shap_values_train = explainer_train.shap_values(df_treino_X, check_additivity=False)

    # Applying to test data
    explainer_test = shap.TreeExplainer(regressor, df_teste_X,
                                   feature_perturbation="interventional")
    explainer_test_v2 = shap.TreeExplainer(regressor)
    shap_values_test = explainer_test.shap_values(df_teste_X, check_additivity=False)

    save_to_pickle(explainer_test, "tree_explainer_test_v1")
    save_to_pickle(shap_values_test, "shap_values_test_v1")

    explainer_test_v2.shap_interaction_values(df_teste_X)
    '''
    TO DOs:
    O calculo do ultimo shap value está no arquivo só precisa ler.
    Tudo foi feito com a base de treino, testar com a base de teste.
    Precisar ver como analisar as linhas certas baseado nas diferenças calculadas na coluna diff da base de teste
    '''
    explainer_test = read_from_pickle('tree_explainer_test_v1')
    shap_values_test = read_from_pickle('shap_base_teste_v1')

    list_diff = list(df_teste["diff"])
    list_diff_orde = list(df_teste["diff"])
    list_diff_orde.sort(reverse=True)

    print("Valor real: {}".format(df_teste.iloc[list_diff.index(list_diff_orde[0]),
                                                 list(df_teste.columns).index('dengue_diagnosis')]))
    print("Valor previsto: {}".format(df_teste.iloc[list_diff.index(list_diff_orde[0]),
                                                 list(df_teste.columns).index('dengue_diagnosis_previsto')]))

    shap.force_plot(explainer_test.expected_value, shap_values_test[list_diff.index(list_diff_orde[0]), :],
                                                    df_teste_X.iloc[list_diff.index(list_diff_orde[0]), :],
                                                    matplotlib=True)
    shap.force_plot()

    shap.dependence_plot("t-1", shap_values_test, df_treino_X,
                         interaction_index="temp_solo")

