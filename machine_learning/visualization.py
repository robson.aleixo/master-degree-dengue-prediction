#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:19:02 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
from matplotlib import pyplot as plt
import seaborn as sns
import shap
from datetime import date
from codigos_higienizacao.leitura_e_higienizacao import sanitiza_populacao

base_path = os.getcwd()

############################
# Visualization            #
############################
def plot_one_line(df, cities_to_plot):
    
    for c in cities_to_plot:
        df_plot = df[df['nome_bairro'] == c]['dengue_diagnosis']
        plt.figure()
        plt.title(c)
        plt.ylabel('dengue_diagnosis')
        plt.plot(df_plot)
        plt.show()


def plot_two_lines(yreal, yhat, xlabel='', ylabel='', title='', 
                   yreal_label='', yhat_label=''):

    font = {'family': 'normal',
            'weight': 'normal',
            'size': '36'}
    title_font = {'fontname':'Arial', 'size':'32', 'color':'black', 'weight':'normal',
                  'verticalalignment':'bottom'}

    plt.figure(figsize=(20,10))
    plt.xlabel(xlabel, **font)
    plt.ylabel(ylabel, **font)
    plt.title(title, **title_font)
    plt.plot(yreal, 'r', label=yreal_label, linewidth=5)
    plt.plot(yhat, 'b', label=yhat_label, linewidth=5)
    plt.legend()
    plt.show()


def plot_two_lines_v2(list1, list2, listX, labelList1="Novo", labelList2="Antigo",
                      ylabel="Número de casos de Dengue", title=''):

    fig, ax1 = plt.subplots(figsize=(20, 10))

    ax1.plot(listX, list1, color='b', linewidth=6, label=labelList1)
    ax1.plot(listX, list2, color='g', linewidth=6, label=labelList2,
             linestyle='--')

    ax1.set_xlabel('Data')
    ax1.set_ylabel(ylabel, size=26)
    plt.title(title)
    plt.legend(loc='best')
    plt.show()


def plot_r2_analysis_by_neighborhood_train_data(regressor, df_input_X, df_input_y,
                                     df_final_total, cities_to_plot):

    df_analysis_city = create_df_with_real_and_prev_results_by_city(regressor,
                                                                    df_input_X, df_input_y, df_input_ml)
    
    

    df_analysis_city.drop(columns=['year', 'month', 'Unnamed: 0'], inplace=True)

    df_pred = df_final_total[df_final_total['ano'].isin(list(range(2015, 2020)))]

    df_pred = df_pred[['name_neighborhood', 'dengue_diagnosis']]
    df_pred['dengue_diagnosis'] = df_pred['dengue_diagnosis'].apply(lambda x: int(x))

    for w in list(df_analysis_city['name_neighborhood'].unique()):
        if w in cities_to_plot:
            df_plot = df_analysis_city[df_analysis_city['name_neighborhood'] == w][['dengue_diagnosis_real',
                                                                  'dengue_diagnosis_prev']]
            pred = list(df_plot['dengue_diagnosis_prev'])
            pred = pred + list(df_pred[df_pred['name_neighborhood'] == w]['dengue_diagnosis'])
            plot_two_lines(list(df_plot['dengue_diagnosis_real']), pred,
                           xlabel='Time (months)', ylabel='Case Numbers', title=w)


def plot_r2_analysis_by_neighborhood_test_data(df_final_total, df_previsao, df_r2,
                                     cities_to_plot):

    df_r2.index = df_r2["city"]
    df_final_total = df_final_total[df_final_total['ano'].isin(
        list(range(2014, 2018, 1)))]

    df_final_total = df_final_total[['chave', 'nome_bairro', 
                                     'dengue_diagnosis']]
    
    df_final_total.columns = ['chave', 'nome_bairro', 
                              'dengue_diagnosis_real']
    
    df_previsao = df_previsao[['chave', 'dengue_diagnosis']]
    df_previsao.columns = ['chave', 'dengue_diagnosis_prev']
    
    df_analysis_city = pd.merge(df_final_total, df_previsao, on='chave', how="left")

    for w in cities_to_plot:
        df_plot = df_analysis_city[df_analysis_city['nome_bairro'] == w][['dengue_diagnosis_real',
                                                              'dengue_diagnosis_prev']]
        plot_two_lines(list(df_plot['dengue_diagnosis_real']), list(df_plot['dengue_diagnosis_prev']),
                       xlabel='Time (months)', ylabel='Case Numbers', title=w+" (R² = "+str(round(df_r2.loc[w, ["r2"]]["r2"],3))+")",
                       yreal_label='Real', yhat_label='Previsão')


def plot_dependent_independent_variables(df, indep_variable):
    
    for w in list(df.columns):
        if not w in [indep_variable, 'name_neighborhood', 'Unnamed: 0',
                     'code_neighborhood', 'chave']:
            sns.scatterplot(x=w, y=indep_variable,
                           data=df)
            plt.show()
    

def plot_std_predicted_value(df_previsao, df_final_total):
    
    df_final_total = df_final_total[['chave', 'name_neighborhood', 
                                     'mes', 'ano', 'dengue_diagnosis']]
    df_final_total.columns = ['chave', 'name_neighborhood', 
                              'mes', 'ano', 'dengue_diagnosis_prev']
    df_previsao = df_previsao[['chave', 'dengue_diagnosis']]
    df_previsao.columns = ['chave', 'dengue_diagnosis_real']
    
    df_analysis_city = pd.merge(df_final_total, df_previsao, on='chave')
    
    df_analysis_city['diff'] = df_analysis_city[
                    ['dengue_diagnosis_real','dengue_diagnosis_prev']].apply(
                    lambda x: x[0] - x[1], axis=1)

    sns.scatterplot(x='dengue_diagnosis_real', y='diff',
                               data=df_analysis_city).set_title(
                                   "Erro Random Forest")
    
    plt.show()


def visualize_features_by_region(df_input, region_list):

    columns_filtered = ['nome_bairro', 'mes',
                        'qtd_cnes', 'qtd_serv_atencao_basica', 'qtd_serv_ACS_program',
                        'qtd_serv_prenatal', 'qtd_serv_neonatal', 'qtd_serv_plano_familia_STD',
                        'qtd_afiliacao_sus', 'temp_solo', 'precipitacao',
                        'min_vizinhos', 'max_vizinhos', 'Esperança de vida ao nascer (em anos)',
                        'Taxa de alfabetização de adultos (%)',
                        'Taxa bruta de frequência escolar (%)',
                        'Renda per capita (em R$ de 2000)', 'IDH-L', 'IDH-E', 'IDH-R', 'IDH',
                        't-1', 't-2', 't-3', 't-4',
                        't-5', 't-6']

    df_study = df_input[df_input["nome_bairro"].isin(region_list)]
    df_study['ano/mes'] = df_study[['ano', 'mes']].apply(lambda x: date(x[0], x[1], 1), axis=1)
    df_study = df_study[df_study['ano'].isin(
        list(range(2014, 2018, 1)))]
    columns_filtered.append("dengue_diagnosis")
    columns_filtered.append("ano/mes")
    columns_filtered.remove("mes")
    df_study = df_study[columns_filtered]

    for c in columns_filtered:
        if not c in ["nome_bairro", "ano/mes"]:
            plt.figure(figsize=(20, 10))
            plt.title(c)
            sns.lineplot(x=df_study["ano/mes"], y=df_study[c], hue=df_study["nome_bairro"])
            plt.show()


def histogram_visualization(df_teste):

    df_teste['diff'] = df_teste[['dengue_diagnosis', 'dengue_diagnosis_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)
    sns.distplot(df_teste['diff'])
    plt.show()

    df_teste['diff'].hist()
    plt.show()

    df_teste['diff'].hist()
    plt.show()


def general_visualizations(df_input):

    # Bar graph
    def bar_graph_quali(df_input):

        df_input = df_input[df_input['ano'].isin(list(range(2015, 2021)))]
        pivot = pd.pivot_table(index=['nome_bairro'],
                               values=['dengue_diagnosis'],
                               aggfunc="sum", data=df_input)

        pivot_2017 = pivot.reset_index()
        pivot_2017['perc'] = pivot_2017['dengue_diagnosis'].apply(lambda x: x/np.sum(pivot_2017['dengue_diagnosis']))
        pivot_2017.sort_values(by=['dengue_diagnosis'], ascending=False, inplace=True)
        perc_list = list()
        for c in list(range(pivot_2017.shape[0])):
            perc_list.append(pivot_2017.iloc[c, list(pivot_2017.columns).index('perc')] + \
                             np.sum(pivot_2017.iloc[:c, list(pivot_2017.columns).index('perc')]))
        pivot_2017['perc_acu'] = perc_list
        pivot_2017['id'] = list(range(1, 161))

        pivot_2017 = pivot_2017.iloc[:10,]

        pivot_2017.columns = ['Nome do bairro', 'Número de Casos de Dengue',
                              'Percentual Individual', 'Percentual Acumulado']
        f, ax = plt.subplots(figsize=(15, 6))
        sns.set_context(font_scale=0.9)
        sns.barplot(x='Nome do bairro', y='Número de Casos de Dengue',
                    data=pivot_2017)
        plt.show()

    # Line graph
    def line_graph_quali(df_input):

        df_input['data'] = df_input[['ano', 'mes']].apply(lambda x: date(x[0], x[1], 1), axis=1)
        df = df_input[df_input['ano'].isin(list(range(2015, 2021)))]
        df = df[df['nome_bairro'].isin(['Campo Grande', 'Bangu', 'Realengo', 'Senador Camará'])]
        df = df[['nome_bairro', 'data', 'dengue_diagnosis']]
        df.columns = ["Nome do bairro", "Data", "Número de casos de Dengue"]
        f, ax = plt.subplots(figsize=(15, 6))
        sns.set_context(font_scale=0.5)
        sns.lineplot(x='Data', y="Número de casos de Dengue",
                    hue="Nome do bairro", data=df)
        plt.show()

    # Temp e Casos de Dengue
    def temp_casos_dengue(df_input):

        pivot_temp = pd.pivot_table(index=['ano', "mes"],
                               values=['dengue_diagnosis'],
                               aggfunc="sum", data=df_input)

        pivot_temp = pivot_temp.reset_index()
        pivot_temp['chave'] = pivot_temp[['ano', 'mes']].apply(lambda x: str(x[0]) + str(x[1]), axis=1)
        pivot_temp.drop(columns=["ano", "mes"], inplace=True)

        pivot = pd.pivot_table(index=['ano', "mes"],
                               values=['temperatura (°C)', 'precipitacao (mm)'],
                               aggfunc="mean", data=df_input)

        pivot = pivot.reset_index()
        pivot['chave'] = pivot[['ano', 'mes']].apply(lambda x: str(x[0]) + str(x[1]), axis=1)

        pivot_temp = pd.merge(pivot_temp, pivot, on='chave')
        pivot_temp = pivot_temp[pivot_temp['ano'].isin(list(range(2015, 2021)))]
        pivot_temp['data'] = pivot_temp[['ano', 'mes']].apply(lambda x: date(x[0], x[1], 1), axis=1)

        pivot_temp.columns = ["Número de casos de Dengue", 'chave', 'ano', 'mes',
                              'Precipitação (mm)', 'Temperatura (°C)', 'Data']

        fig, ax1 = plt.subplots(figsize=(20, 10))

        ax2 = ax1.twinx()
        ax1.plot(pivot_temp['Data'], pivot_temp["Número de casos de Dengue"], color='b', linewidth=6, linestyle='--')
        ax2.plot(pivot_temp['Data'], pivot_temp['Temperatura (°C)'], color='g', linewidth=6)

        ax1.set_xlabel('Data')
        ax1.set_ylabel("Número de casos de Dengue", size=26)
        ax2.set_ylabel('Temperatura (°C)', size=26)
        plt.legend(loc='best')
        plt.show()

        fig, ax1 = plt.subplots(figsize=(20, 10))

        ax2 = ax1.twinx()
        ax1.plot(pivot_temp['Data'], pivot_temp["Número de casos de Dengue"], color='b', linewidth=6, linestyle='--')
        ax2.plot(pivot_temp['Data'], pivot_temp['Precipitação (mm)'], color='g', linewidth=6)

        ax1.set_xlabel('Data')
        ax1.set_ylabel("Número de casos de Dengue", size=26)
        ax2.set_ylabel('Precipitação (mm)', size=26)
        plt.legend(loc='best')
        plt.show()

    # IDH e Dengue
    def idh_dengue(df_input):

        df_input = df_input[df_input['ano'].isin([2019])]

        pivot_dengue = pd.pivot_table(index=['nome_bairro'],
                               values=['dengue_diagnosis', 'qtd_cnes'],
                               aggfunc={'dengue_diagnosis': "sum", 'qtd_cnes':"mean"},
                                        data=df_input)

        pivot_dengue = pivot_dengue.reset_index()

        pivot_dengue = pd.merge(pivot_dengue, df_input[['nome_bairro', 'Populacao', 'IDH']], on='nome_bairro', how='left')

        pivot_dengue['Taxa de Casos de Dengue por 1000 habitantes'] = \
            pivot_dengue[['dengue_diagnosis', 'Populacao']].apply(lambda x: (x[0]/x[1]) * 1000, axis=1)

        pivot_dengue['Estabelecimentos de Saúde por 1000 habitantes'] = \
            pivot_dengue[['qtd_cnes', 'Populacao']].apply(lambda x: (x[0] / x[1]) * 1000, axis=1)

        pivot_dengue.columns = ["Bairro", "Número de casos de Dengue", 'Quantidade de Estabelecimentos de Saúde',
                             'População (Censo 2010)', 'IDH (Censo 2010)',
                             'Casos de Dengue por 1000 habitantes (2019)',
                               'Estabelecimentos de Saúde por 1000 habitantes']

        pivot_dengue.sort_values(by=['Casos de Dengue por 1000 habitantes (2019)'],
                              ascending=False, inplace=True)

        pivot_dengue.drop_duplicates(inplace=True)

        f, ax = plt.subplots(figsize=(12, 6))
        sns.set_context(font_scale=0.5)
        sns.scatterplot(x='IDH (Censo 2010)', y='Casos de Dengue por 1000 habitantes (2019)',
                    size='Estabelecimentos de Saúde por 1000 habitantes', ax=ax,
                    sizes=(0, 1000), data=pivot_dengue, legend='brief')
        plt.ylim(0, 30)
        plt.show()

    df_input['data'] = df_input[['ano', 'mes']].apply(lambda x: date(x[0], x[1], 1), axis=1)
    df_input.to_csv("graficos_qualificacao_normal.csv")

    def plot_feature_importance(df):

        models = ["randomforest", "lightGBM",
                  "extratrees", "xgboost"]

        for m in models:
            df = pd.read_csv(base_path + "/data_sus/finais/feature_importances/feature_importance_{}.csv".format(m))
            df = df.iloc[:10, :]
            df.drop(columns=['Unnamed: 0'], inplace=True)

            sns.set_theme(style="white")
            f, ax = plt.subplots(figsize=(8, 6))
            sns.set_context(font_scale=0.9)
            sns.barplot(x='importance', y='feature',
                        data=df)
            plt.title(m)
            plt.show()


if __name__ == '__main__':

    bar_graph_quali(df_dengue_origem)
    temp_casos_dengue(df_dengue_origem)
    line_graph_quali(df_dengue_origem)
    idh_dengue(df_dengue_origem)