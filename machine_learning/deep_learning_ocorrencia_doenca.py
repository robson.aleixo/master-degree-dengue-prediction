#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 14 18:44:05 2020

@author: robson
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import os
import re
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from sklearn.metrics import r2_score
from numpy import hstack, vstack
from IPython import embed
import pickle


base_path = os.getcwd()

'''
TO DOs:
    - Continuar olhando para esse site aqui: 
    https://machinelearningmastery.com/start-here/#deep_learning_time_series 
    na seção Long Short-Term Memory Networks (LSTMs)
    - Vai ajudar inclusive na parte de ml:
    https://machinelearningmastery.com/multi-step-time-series-forecasting-with-machine-learning-models-for-household-electricity-consumption/
        
'''

#############################
# Pre Processing            #
#############################
def read_and_get_input():

    df_input = pd.read_csv(
        base_path+\
            '/data_sus/finais/dengue_temp_prec_umi_reg_SARIMAX.csv')
        
    # df_previsao = df_input[df_input['ano'].isin(list(range(2018, 2023, 1)))]
    # df_input = df_input[df_input['ano'].isin(list(range(2007, 2019, 1)))]
       
    df_input = df_input[['geo_tube_code_neighborhood', 'geo_tube_name_neighborhood', 
                         'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2', 
                         'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                         'prec_month', 'dengue_diagnosis', 'umidade']]   
    
    df_input.columns = ['code_neighborhood', 'name_neighborhood', 
                         'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2', 
                         'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                         'prec_month', 'dengue_diagnosis', 'umidade']
    
    df_input.columns = ['cod_bairro', 'nome_bairro', 
                         'ano', 'mes', 'chave', 'qtd_cnes', 
                         'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 
                         'qtd_serv_prenatal', 'qtd_serv_neonatal', 
                         'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                         'temp_solo', 'precipitacao', 'dengue_diagnosis', 
                         'umidade']

    # Time series window
    for i in list(range(1, 13)):
        df_input['t-' + str(i)] = df_input['dengue_diagnosis'].shift(i)
    
    df_input = df_input[df_input['ano'].isin(list(range(2008, 2023, 1)))]
    df_input_ml = df_input[df_input['ano'].isin(list(range(2014, 2017, 1)))]
    df_previsao = df_input[df_input['ano'].isin(list(range(2017, 2018, 1)))]
    
    return df_input_ml, df_previsao


def split_sequences(sequences, n_steps):
    '''
    Método copiado do site:
    https://machinelearningmastery.com/how-to-develop-lstm-models-for-time-series-forecasting/
    # split a multivariate sequence into samples
    '''

    X, y = list(), list()
    for i in range(len(sequences)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the dataset
        if end_ix > len(sequences):
            break
		# gather input and output parts of the pattern
        seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix-1, -1]
        X.append(seq_x)
        y.append(seq_y)
    
    return np.array(X), np.array(y)


def apply_feature_scaling(array_target, columns_index=0, scaler=False):
    
    if not scaler:
        scaler = MinMaxScaler(feature_range = (0,1))
    
    if columns_index == 0:
        array_target = scaler.fit_transform(array_target)   
    else:
        array_target[:,:columns_index] = scaler.fit_transform(
            array_target[:,:columns_index])
    
    return array_target, scaler


def prepare_time_data(df_input_dl_um_bairro, timestamp, column_target):
    
    # Take dependent variable
    array_target = df_input_dl_um_bairro.loc[:,[column_target]].values
    
    # Apply feature scaling
    array_target_scaled, scaler = apply_feature_scaling(array_target) 
    
    x_train = list()
    y_train = list()
    
    for i in list(range(timestamp, len(array_target_scaled))):
        x_train.append(array_target_scaled[i-timestamp:i, 0])
        y_train.append(array_target_scaled[i, 0])
        
    x_train, y_train = np.array(x_train), np.array(y_train)
    
    # reshape input to be 3D [samples, timesteps, features]
    x_train = np.reshape(x_train, (x_train.shape[0], timestamp, 1))

    return x_train, y_train


def prepare_time_data_considering_many_features(df_input_dl, 
                                                timesteps, column_target,
                                                categorical_columns, 
                                                columns_filtered, 
                                                scaler=False,
                                                test_base=False):
    
    def consolidate_by_region(df, name_region):
        
        '''
        This method helps building input considering many cities
        '''
        
        first = False
        
        for r in list(df[name_region].unique()):
            
            df_region = df[df[name_region] == r]
            
             # Dealing with categorival values
            df_region = pd.get_dummies(df_region, columns=categorical_columns)
            
            values = df_region.values
            y_train = values[:,list(df_region.columns).index(column_target)]
            x_train = np.delete(values, list(df_region.columns).index(column_target), 
                                axis=1)
            
            y_train = y_train.reshape(-1,1)
        
            # puting x and y together to strutucture the input
            dataset = hstack((x_train, y_train))
            
            # building the input, considering samples=rows and 
            # in each sample timesteps=timesteps
            x_train_reshape, y_train_reshape = split_sequences(dataset, 
                                                               timesteps)
            
            if not first:
                x_train_reshape_total = x_train_reshape 
                y_train_reshape_total = y_train_reshape
                first = True
            else:
                x_train_reshape_total = vstack((x_train_reshape_total, 
                                                x_train_reshape))
                y_train_reshape_total = np.concatenate((y_train_reshape_total, 
                                                y_train_reshape))
            
        return x_train_reshape_total, y_train_reshape_total
    
    # Filtering the columns that will be used in the model
    df = df_input_dl.loc[:, columns_filtered+[column_target]]    
    columns_scaling = [a for a in columns_filtered if not a in categorical_columns]
    
    if not test_base:
        scaler = MinMaxScaler(feature_range = (0,1))
        df[columns_scaling] = scaler.fit_transform(df[columns_scaling])
        
    else:
        df[columns_scaling] = scaler.transform(df[columns_scaling])

       
    x_train_reshape, y_train_reshape = consolidate_by_region(df, 'nome_bairro')
    
    if not test_base:
        return x_train_reshape, y_train_reshape, scaler
    else:
        return x_train_reshape, y_train_reshape
    
############################
# Deep Learning            #
############################
    
def build_lstm(x_train, y_train):
    
    number_of_neurons = 50 
    dropout_percentage = 0.2
    epochs = 1000
    batch_size = 50
    optimizer = 'adam' # Try after with RMSprop [rmsprop, adam]
    loss_function = 'mean_squared_error'
    
    regressor = Sequential()
    
    # Adding the fisrt LSTM layer
    regressor.add(LSTM(units = number_of_neurons, 
                       return_sequences = True,
                       input_shape = (x_train.shape[1], x_train.shape[2])))
    regressor.add(Dropout(dropout_percentage))
    
    # Adding the second LSTM layer
    regressor.add(LSTM(units = number_of_neurons, 
                       return_sequences = True))
    regressor.add(Dropout(dropout_percentage))
    
    # Adding the third LSTM layer
    regressor.add(LSTM(units = number_of_neurons, 
                       return_sequences = True))
    regressor.add(Dropout(dropout_percentage))
    
    # Adding the fourth LSTM layer
    regressor.add(LSTM(units = number_of_neurons, 
                       return_sequences = False))
    regressor.add(Dropout(dropout_percentage))
    
    # Adding the output layer
    regressor.add(Dense(units = 1))
    
    # Compiling (Try after with RMSprop optimizer)
    regressor.compile(optimizer = optimizer, loss = loss_function)
    
    # Fitting the LSTM to the training set
    regressor.fit(x_train, y_train, epochs = epochs, batch_size=batch_size)
    
    return regressor


def prepare_test_base(df_input_dl_um_bairro, df_previsao_um_bairro, timestamp,
                      column_target):

    array_test_target = df_previsao_um_bairro.loc[:,[column_target]].values
    
    df_total = pd.concat((df_input_dl_um_bairro[column_target], 
                          df_previsao_um_bairro[column_target]), axis = 0)
    
    inputs = df_total[len(df_total) - len(df_previsao_um_bairro) - timestamp:].values
    inputs = inputs.reshape(-1, 1)
    input_scaled = scaler.transform(inputs) 
    x_test = list()
    for i in list(range(timestamp, timestamp+len(array_test_target))):
        x_test.append(inputs[i-timestamp:i, 0])
    x_test = np.array(x_test)
    x_test = x_test.reshape(x_train.shape[0], 1, 
                                   x_train.shape[1])
    
    return x_test


############################
#    Plotting              #
############################
def plot_real_predicted(array_target, y_train_predicted, 
                        y_test_predicted):
    
    aux = [np.nan] * (len(array_target) - len(y_train_predicted))
    y_train_predicted = y_train_predicted + aux
    
    aux = [np.nan] * (len(array_target) - len(y_test_predicted))
    y_test_predicted = aux + y_test_predicted 
    
    plt.plot(array_test_target, color="red", label="Real")
    plt.plot(y_predicted, color="blue", label="Predicted")
    plt.title("Deep Learning Prediction")
    plt.xlabel("Time")
    plt.ylabel("Dengue Ocurrence")
    plt.legend()
    plt.show()


def main():
    
    timesteps = 12
    column_target = "dengue_diagnosis"
    
    df_input_dl, df_previsao = read_and_get_input()
    
    columns_filtered = ['nome_bairro', 'qtd_cnes', 
                         'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 
                         'qtd_serv_prenatal', 'qtd_serv_neonatal', 
                         'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                         'temp_solo', 'precipitacao']
    
    categorical_columns = ['nome_bairro']        
    
    x_train, y_train, scaler = prepare_time_data_considering_many_features(
                                                df_input_dl, 
                                                timesteps, column_target,
                                                categorical_columns,
                                                columns_filtered, 
                                                test_base=False)
    
    # Build LSTM
    regressor = build_lstm(x_train, y_train)
    pickle.dump(regressor, open('models/deep_regressor_1000_epochs_year_20142016.sav', 'wb'))
    
    # Building test input
    x_test, y_test = prepare_time_data_considering_many_features(df_previsao, 
                                                timesteps, column_target,
                                                categorical_columns,
                                                columns_filtered, 
                                                scaler,
                                                test_base=True)
    
    # Predict values from test data
    y_train_predicted = regressor.predict(x_train)
    y_train_predicted = y_train_predicted.reshape(1, -1)[0]
    y_test_predicted = regressor.predict(x_test)
    y_test_predicted = y_test_predicted.reshape(1, -1)[0]
    
    # Look to the performance by r2 metric
    print('R2 train: {}'.format(r2_score(y_train, y_train_predicted))) 
    print('R2 test: {}'.format(r2_score(y_test, y_test_predicted)))    

    

if __name__ == '__main__':
    
    main()
    

    