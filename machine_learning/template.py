#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 17:33:03 2020

@author: robson
"""
from machine_learning.mlMethods import *
from machine_learning.mlSupportMethods import *
from machine_learning.visualization import *
pd.options.mode.chained_assignment = None

base_path = os.getcwd()

#############
# Template  #
#############

'''
Reading input, you need to pass the path file
'''
df_input = read_and_get_input_v2(base_path+\
            '/data_sus/finais/input_ml_ocorrencia_doencas_v2.csv')

'''
Choose the period to separate train (df_input_ml) 
and test (df_previsao_real) data 
'''
df_input_ml = df_input[df_input['ano'].isin(
    list(range(2014, 2017, 1)))]
df_previsao_real = df_input[df_input['ano'].isin(
    list(range(2017, 2018, 1)))]
    
'''
Select columns to input in the model
'''
columns_filtered = ['nome_bairro', 
                      'qtd_cnes', 
                      'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 
                      'qtd_serv_prenatal', 'qtd_serv_neonatal', 
                      'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                      'temp_solo', 'precipitacao', 't-1', 't-2', 't-3', 
                      't-4', 't-5', 't-6', 't-7', 't-8', 't-9', 't-10', 
                      't-11', 't-12']

'''
Put here categorical columns from columns_filtered and target variable
'''
categorical_columns = ['nome_bairro']
target_variable = 'dengue_diagnosis'

'''
Preparing bases to model input
'''
df_input_X = df_input_ml[columns_filtered]

if not categorical_columns == []:
    df_input_X = pd.get_dummies(df_input_X, columns=categorical_columns)

columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_input_X.columns)

df_input_X = df_input_X[columns_filtered_ordered]

df_input_y = df_input_ml[target_variable]

'''
Applying Machine Learning (in this case it is using random forest)
'''
ml_type = "rf"
regressor_rf = run_randomforest(df_input_X, df_input_y)

'''
Applying model to feature values, df_prev is the result of predicted values
'''
df_prev = apply_future_prediction(regressor_rf, df_previsao_real, 
                              columns_filtered,
                              'dengue_diagnosis',
                              categorical_columns)

df_prev.to_csv(
    base_path + \
    '/data_sus/intermediarias/df_previsao_output_ml.csv')


