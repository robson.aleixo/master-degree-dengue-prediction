from statsmodels.tsa.ar_model import AR
from statsmodels.tsa.arima_model import ARMA, ARIMA
from statsmodels.tsa.statespace.sarimax import SARIMAX
from statsmodels.tsa.holtwinters import ExponentialSmoothing, SimpleExpSmoothing


################################################
# Machine Learning 2 - Temporal Analysis       #
################################################
def apply_temporal_univariable_analysis(df_input):

    import warnings
    warnings.filterwarnings('ignore', 'statsmodels.tsa.ar_model.AR',
                            FutureWarning)

    def fit_apply(model, data, city, modelname):

        model_fit = model.fit()
        yhat = model_fit.predict(end=156)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("################### [{}] City {}: {} #####################".format(
            modelname, city, r2_score(data, yhat)))

    def apply_ar(data, city):

        model = AR(data)
        fit_apply(model, data, city, 'AR')

    def apply_arma(data, city, i):

        model = ARMA(data, order=(0, 1))
        model_fit = model.fit(disp=False)
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[ARMA] City {}: {}".format(i,
                                          r2_score(data, yhat)))

    def apply_arma_autoregressive(data, city, i):

        model = ARMA(data, order=(2, 1))
        model_fit = model.fit(disp=False)
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[ARMA AUTO] City {}: {}".format(i,
                                               r2_score(data, yhat)))

    def apply_arima(data, city, i):

        model = ARIMA(data, order=(1, 1, 1))
        model_fit = model.fit(disp=False)
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[ARIMA] City {}: {}".format(i,
                                           r2_score(data, yhat)))

    def apply_sarima(data, city, i):

        model = SARIMAX(data, order=(1, 1, 1), seasonal_order=(1, 1, 1, 1))
        model_fit = model.fit(disp=False)
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[SARIMA] City {}: {}".format(i,
                                            r2_score(data, yhat)))

    def apply_sarima_autoregressive(data1, data2, data, city):

        model = SARIMAX(data1, exog=data2, order=(1, 1, 1),
                        seasonal_order=(0, 0, 0, 0))
        model_fit = model.fit(disp=False)
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[SARIMA AUTO] City {}: {}".format(city,
                                                 r2_score(data, yhat)))

    def apply_ses(data, city):

        model = SimpleExpSmoothing(data)
        model_fit = model.fit()
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[SES] City {}: {}".format(city,
                                         r2_score(data, yhat)))

    def apply_hwes(data, city):

        model = ExponentialSmoothing(data)
        model_fit = model.fit()
        yhat = model_fit.predict(end=132)
        plot_two_lines(data, yhat, title='Auto Regressive Model - AR')
        print("[HWES] City {}: {}".format(city,
                                          r2_score(data, yhat)))

    cities_test = ['Copacabana', 'Cidade Universitária',
                   'Freguesia (Ilha Do Governador)', 'Maracanã', 'Ipanema']

    for c in cities_test:
        df = df_input[df_input['geo_tube_name_neighborhood'] == c]
        apply_ar(list(df['dengue_diagnosis']), c)

