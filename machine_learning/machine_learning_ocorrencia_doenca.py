#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 11:02:57 2020

@author: robson
"""

from datetime import date
from machine_learning.preProcessing import *
from machine_learning.mlMethods import *
from machine_learning.mlSupportMethods import *
from machine_learning.visualization import *

# from sklearn.feature_selection import SelectKBest
# from sklearn.feature_selection import f_regression

from IPython import embed;
# embed()

pd.options.mode.chained_assignment = None
base_path = os.getcwd()

# %matplotlib inline

'''
d_gpsv1 - qtd_serv_atencao_basica
d_gpsv2 - qtd_serv_ACS_program
d_gpsv4 - qtd_serv_prenatal
d_gpsv5 - qtd_serv_neonatal
d_gpsv7 - qtd_serv_plano_familia_STD
d_sus - qtd_afiliacao_sus

'''


def analyse_by_clustering(df_r2, df_previsao_real):

    np.mean(df_r2['r2'])

    df_r2['cluster'] = df_r2['r2'].apply(lambda x: "[0.5, 1]" if x >= 0.5 else \
                                         "[0, 0.5)" if x >= 0 else "[-0.5, 0)" \
                                         if x >=-0.5 else "[-5,-0.5)"  \
                                             if x >= -5 else "[,-5)")

    df_r2.sort_values(by=["r2"], inplace=True, ascending=False)

    df_r2['cluster'].value_counts()

    df_merge_prev = pd.merge(df_previsao_real, df_r2,
                        left_on='nome_bairro', right_on='city',
                        how='left')
    
    df_merge_real = pd.merge(df_previsao_real, df_r2,
                        left_on='nome_bairro', right_on='city',
                        how='left')
    
    df_merge_real['data'] = df_merge_real[['ano', 'mes']].apply(
                                lambda x: date(x[0], x[1], 1), axis=1)
    
    df_merge_prev['data'] = df_merge_real[['ano', 'mes']].apply(
                                lambda x: date(x[0], x[1], 1), axis=1)
    
    features = ['qtd_cnes','qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 
                          'qtd_serv_prenatal', 'qtd_serv_neonatal', 
                          'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                          'temp_solo', 'precipitacao', 't-1', 't-2', 't-3', 
                          't-4', 't-5', 't-6', 't-7', 't-8', 't-9', 't-10', 
                          't-11', 't-12']
    
    # Boxplot
    for c in features:
        sns.boxplot(x="cluster", y=c,
                data=df_merge_real)
        sns.despine(offset=10)
        plt.show()
        
    # Lines
    for c in features:
        sns.lineplot(x="data", y=c,
                 hue="cluster",
                 data=df_merge_real)
        plt.show()
    

def r2_por_ano_desde_2008():
    '''
    Analisar R2 treinando o modelo desde o inicio da base,
    acumulando até chegar no último ano da base
    :return:
    '''
    def execute_ml(df_treino, df_teste, columns_filtered, categorical_columns):

        df_treino_X = df_treino[columns_filtered]

        # Categorical values
        if not categorical_columns == []:
            df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)

        columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)

        df_treino_X = df_treino_X[columns_filtered_ordered]

        df_treino_y = df_treino['dengue_diagnosis']

        ml_type = "rf"
        regressor_rf = run_randomforest(df_treino_X, df_treino_y)

        df_valores_previstos = apply_future_prediction(regressor_rf, df_teste,
                                                       columns_filtered,
                                                       'dengue_diagnosis',
                                                       categorical_columns)

        return df_valores_previstos, regressor_rf

    df_input = read_and_get_input_v2(base_path+\
            '/data_sus/finais/input_ml_ocorrencia_doencas_v3.csv')

    for a in list(range(2010, 2018, 1)):

        df_treino = df_input[df_input['ano'].isin(
            list(range(2009, a, 1)))]
        df_teste = df_input[df_input['ano'] == a]

        columns_filtered = ['nome_bairro', "mes",
                           'temp_solo', 'precipitacao',
                           'max_vizinhos',
                           'IDH', 't-1', 't-2', 't-3', 't-4', 't-5', 't-6']

        categorical_columns = ['nome_bairro','mes']

        df_valores_previstos, regressor_rf = execute_ml(
            df_treino, df_teste, columns_filtered, categorical_columns)

        print('ano de teste = {}, r2 = {} '.format(
            a, r2_score(df_teste['dengue_diagnosis'],
                df_valores_previstos['dengue_diagnosis'])))


def r2_por_ano_treino_agrupado_tres_anos():

    def execute_ml(df_treino, df_teste, columns_filtered, categorical_columns):

        df_treino_X = df_treino[columns_filtered]

        # Categorical values
        if not categorical_columns == []:
            df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)

        columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)

        df_treino_X = df_treino_X[columns_filtered_ordered]

        df_treino_y = df_treino['dengue_diagnosis']

        ml_type = "rf"
        regressor_rf = run_randomforest(df_treino_X, df_treino_y)

        df_valores_previstos = apply_future_prediction(regressor_rf, df_teste,
                                                       columns_filtered,
                                                       'dengue_diagnosis',
                                                       categorical_columns)

        return df_valores_previstos, regressor_rf

    df_input = read_and_get_input_v2(base_path+\
            '/data_sus/finais/input_ml_ocorrencia_doencas_v3.csv')

    for ano_teste in list(range(2012, 2018)):

        df_treino = df_input[df_input['ano'].isin(
            list(range(ano_teste-3, ano_teste, 1)))]
        df_teste = df_input[df_input['ano'].isin(
            list(range(ano_teste, ano_teste+1, 1)))]
        df_teste = df_teste[df_input['mes'].isin(
            list(range(1, 13, 1)))]

        columns_filtered = ['nome_bairro', "mes",
                           'temp_solo', 'precipitacao',
                           'max_vizinhos',
                           'IDH', 't-1', 't-2', 't-3', 't-4', 't-5', 't-6']

        categorical_columns = ['nome_bairro','mes']

        df_valores_previstos, regressor_rf = execute_ml(df_treino, df_teste, columns_filtered, categorical_columns)

        print('ano de teste = {}, r2 = {} '.format(
            ano_teste, r2_score(df_teste['dengue_diagnosis'],
                        df_valores_previstos['dengue_diagnosis'])))


def region_visualization_by_r2(df_r2_visualization, df_input, df_valores_previstos, ascending, number):

    # df_input["data"] = df_input[["ano", "mes"]].apply(lambda x: date(x[0], x[1], 1))
    df_r2_visualization.sort_values(by=["r2"], ascending=ascending, inplace=True)
    cities_to_plot = list(df_r2_visualization['city'])[:number]

    plot_r2_analysis_by_neighborhood_test_data(df_input, df_valores_previstos, df_r2_visualization,
                                               cities_to_plot)


def main(columns_filtered, categorical_columns, columns_filtered_categorical):

    def execute_ml(df_treino, df_teste, columns_filtered, categorical_columns, columns_filtered_categorical):

        df_treino_X = df_treino[columns_filtered]

        # Categorical values
        if not categorical_columns == []:
            df_treino_X = pd.get_dummies(df_treino_X, columns=categorical_columns)

        if not columns_filtered_categorical == []:
            new_columns_features = [a for a in columns_filtered if a not in categorical_columns]
            df_treino_X = df_treino_X[new_columns_features+columns_filtered_categorical]

        columns_filtered_ordered, index_time_cols = get_indices_columns_time(df_treino_X.columns)

        df_treino_X = df_treino_X[columns_filtered_ordered]

        df_treino_y = df_treino['dengue_diagnosis']

        ml_type = "rf"
        regressor_rf = run_randomforest(df_treino_X, df_treino_y, grid_search=False)

        df_valores_previstos = apply_future_prediction_update_neighbors(regressor_rf, df_teste,
                                                       columns_filtered,
                                                       'dengue_diagnosis',
                                                       columns_filtered_categorical,
                                                       categorical_columns)

        return df_valores_previstos, regressor_rf

    df_input = read_and_get_input_v2(base_path+\
            '/data_sus/finais/input_ml_ocorrencia_doencas_v6.csv')

    df_input['temp_hist'] = df_input[['temp_solo', 't-1', 't-2', 't-3']].apply(
        lambda x: np.mean(x[1:])*x[0], axis=1)
    df_input['prec_hist'] = df_input[['precipitacao', 't-1', 't-2', 't-3']].apply(
        lambda x: np.mean(x[1:])*x[0], axis=1)

    ano_teste = 2017

    df_treino = df_input[df_input['ano'].isin(
        list(range(ano_teste-3, ano_teste, 1)))]
    df_teste = df_input[df_input['ano'].isin(
        list(range(ano_teste, ano_teste+1, 1)))]
    df_teste = df_teste[df_input['mes'].isin(
        list(range(1, 13, 1)))]

    df_valores_previstos, regressor = execute_ml(df_treino, df_teste, columns_filtered, categorical_columns,
                                                    columns_filtered_categorical)

    df_aux = df_valores_previstos[['chave', 'dengue_diagnosis']]
    df_aux.columns = ['chave', 'dengue_diagnosis_previsto']
    df_teste = pd.merge(df_teste, df_aux, on='chave', how='left')
    df_teste['diff'] = df_teste[['dengue_diagnosis', 'dengue_diagnosis_previsto']].apply(
        lambda x: abs(x[0] - x[1]), axis=1)

    return df_treino, df_teste, df_valores_previstos, regressor
    
    
if __name__ == "__main__":

    columns_filtered = ['nome_bairro', 'mes', 'temp_solo', 'precipitacao', 'qtd_cnes',
                        'sum_vizinhos_t-1', 'IDH', 't-1', 't-2', 't-3', 't-4', 't-5',
                        't-6', 't-7', 't-8', 't-9', 't-10', 't-11', 't-12']

    categorical_columns = ['nome_bairro', 'mes']

    columns_filtered_categorical = []

    columns_filtered_categorical = ['nome_bairro_Realengo',  'nome_bairro_Santa Cruz',
                                    'nome_bairro_Bangu', 'nome_bairro_Campo Grande',
                                    'nome_bairro_Senador Camará', "mes_1", "mes_2", "mes_3"]

    df_treino, df_teste, df_previsao, regressor = main(columns_filtered, categorical_columns,
                                                       columns_filtered_categorical)

    df_teste.sort_values(by=["diff"], ascending=False, inplace=True)

    # Apply r2 in validate database
    df_r2 = apply_r2_for_each_city_test_data(df_previsao, df_teste)

    df_variance_total_city_v2 = apply_adjusted_r2_for_each_region_test_data_variance_all_city(df_previsao, df_teste)
    np.mean(df_variance_total_city_v2["r2"])

    # df_variance_total_region = apply_adjusted_r2_for_each_region_test_data_variance_all_historical_data(df_treino, df_teste)

    df_variance_total_city_v2_aux = df_variance_total_city_v2.sort_values(by=["r2"], ascending=True)
    df_r2_aux = df_r2[df_r2['city'].isin(list(df_variance_total_city_v2_aux['city'])[:5])]

    # Visualization
    region_visualization_by_r2(df_r2_aux, df_treino.append(df_teste), df_previsao, True, 5)

    # Realengo,  Santa Cruz, Bangu, Campo Grande, Senador Camará

    sns.scatterplot(x='temp_solo', y='dengue_diagnosis',
                    data=df_input)

    plt.show()