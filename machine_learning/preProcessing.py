#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:16:51 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler

'''
d_gpsv1 - qtd_serv_atencao_basica
d_gpsv2 - qtd_serv_ACS_program
d_gpsv4 - qtd_serv_prenatal
d_gpsv5 - qtd_serv_neonatal
d_gpsv7 - qtd_serv_plano_familia_STD
d_sus - qtd_afiliacao_sus

'''


#############################
# Pre Processing            #
#############################
def read_and_get_input_v2(path_file, target_variable='dengue_diagnosis'):

    df_input = pd.read_csv(path_file)

    # Time series window
    for i in list(range(1, 13)):
        df_input['t-' + str(i)] = df_input[target_variable].shift(i)
    
    df_input = df_input[df_input['ano'].isin(list(range(2008, 2018, 1)))]

    return df_input


def read_and_get_input(path_file, target_variable='dengue_diagnosis'):

    df_input = pd.read_csv(path_file)

    df_input = df_input[['geo_tube_code_neighborhood', 'geo_tube_name_neighborhood',
                         'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2',
                         'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                         'prec_month', 'dengue_diagnosis', 'umidade',
                         'Esperança de vida ao nascer (em anos)',
                         'Taxa de alfabetização de adultos (%)',
                         'Taxa bruta de frequência escolar (%)',
                         'Renda per capita (em R$ de 2000)', 'Índice de Longevidade (IDH-L)',
                         'Índice de Educação (IDH-E)', 'Índice de Renda (IDH-R)',
                         'Índice de Desenvolvimento Humano (IDH)'
                         ]]

    # Reposicionando
    df_input.columns = ['code_neighborhood', 'name_neighborhood',
                        'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2',
                        'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                        'prec_month', 'dengue_diagnosis', 'umidade',
                        'Esperança de vida ao nascer (em anos)',
                        'Taxa de alfabetização de adultos (%)',
                        'Taxa bruta de frequência escolar (%)',
                        'Renda per capita (em R$ de 2000)', 'Índice de Longevidade (IDH-L)',
                        'Índice de Educação (IDH-E)', 'Índice de Renda (IDH-R)',
                        'Índice de Desenvolvimento Humano (IDH)'
                        ]

    # Renoemando
    df_input.columns = ['cod_bairro', 'nome_bairro',
                        'ano', 'mes', 'chave', 'qtd_cnes',
                        'qtd_serv_atencao_basica', 'qtd_serv_ACS_program',
                        'qtd_serv_prenatal', 'qtd_serv_neonatal',
                        'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus',
                        'temp_solo', 'precipitacao', 'dengue_diagnosis',
                        'umidade', 'Esperança de vida (em anos)',
                        'Taxa de alfabetização de adultos (%)',
                        'Taxa bruta de frequência escolar (%)',
                        'Renda per capita (em R$ de 2000)', 'IDH-L',
                        'IDH-E', 'IDH-R', 'IDH']

    # Time series window
    for i in list(range(1, 13)):
        df_input['t-' + str(i)] = df_input[target_variable].shift(i)

    df_min_max_neighbor = pd.read_csv(os.getcwd() +
                                      '/data_sus/finais/input_ml_ocorrencia_com_idh_min_max_neighboor.csv')

    df_min_max_neighbor = df_min_max_neighbor[['chave', 'min_vizinhos', 'max_vizinhos']]

    df_input = pd.merge(df_input, df_min_max_neighbor, on="chave", how="left")

    df_input = df_input[df_input['ano'].isin(list(range(2008, 2023, 1)))]

    return df_input


def read_and_get_input_network(path_file):

    df_input = pd.read_csv(path_file)
        
    net_columns = list()
    for c in df_input.columns:
        if re.match(".*(t-.).*"):
            net_columns.append(c)
       
    df_input = df_input[['geo_tube_code_neighborhood', 'geo_tube_name_neighborhood', 
                         'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2', 
                         'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                         'prec_month', 'dengue_diagnosis', 'umidade']]   
    
    # Reposicionando
    df_input.columns = ['code_neighborhood', 'name_neighborhood', 
                         'ano', 'mes', 'chave', 'cnes', 'd_gpsv1', 'd_gpsv2', 
                         'd_gpsv4', 'd_gpsv5', 'd_gpsv7', 'd_sus', 'alstdi03',
                         'prec_month', 'dengue_diagnosis', 'umidade']
    
    # Renoemand0
    df_input.columns = ['cod_bairro', 'nome_bairro', 
                         'ano', 'mes', 'chave', 'qtd_cnes', 
                         'qtd_serv_atencao_basica', 'qtd_serv_ACS_program', 
                         'qtd_serv_prenatal', 'qtd_serv_neonatal', 
                         'qtd_serv_plano_familia_STD', 'qtd_afiliacao_sus', 
                         'temp_solo', 'precipitacao', 'dengue_diagnosis', 
                         'umidade']

    # Time series window
    for i in list(range(1, 13)):
        df_input['t-' + str(i)] = df_input['dengue_diagnosis'].shift(i)
    
    df_input = df_input[df_input['ano'].isin(list(range(2008, 2023, 1)))]
    
    return df_input


def apply_feature_scaling(X_train):
    
    scaler = MinMaxScaler(feature_range = (0,1))

    X_train = pd.DataFrame(scaler.fit_transform(X_train),
                           columns=X_train.columns)
    
    return X_train, scaler


def apply_feature_scaling_specific_columns(X_train, columns):

    df = X_train[columns]

    scaler = MinMaxScaler(feature_range=(0, 1))

    df = pd.DataFrame(scaler.fit_transform(df),
                           columns=df.columns)

    for c in columns:
        X_train[c + "_scaling"] = list(df[c])

    return X_train, scaler


def apply_feature_scaling_y(y):
    
    scaler = MinMaxScaler(feature_range = (0,1))

    y = pd.DataFrame(scaler.fit_transform(np.reshape(y.values, (-1, 1))))
    
    return y, scaler


def apply_feature_selection_kbest(df_treino):
    df_treino_x = df_treino.drop(
        columns=['Unnamed: 0', 'cod_bairro', 'chave', "dengue_diagnosis", 'tuberculosis_diagnosis',
                 'syphilis_adquirida_diagnosis', 'syphilis_congenita_diagnosis',
                 'syphilis_gestante_diagnosis', 'influenza_diagnosis', 'ano', 'data', 'mes'])

    df_treino_y = df_treino[["dengue_diagnosis"]]

    fs = SelectKBest(score_func=f_regression, k=15)

    df_treino_x_selected = fs.fit_transform(df_treino_x.drop(columns=['nome_bairro']),
                                            df_treino_y['dengue_diagnosis'])