#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 16:27:02 2020

@author: robson
"""
import pandas as pd
import numpy as np
import os
import re

##########################
# Pos Processing         #
##########################
def build_df_to_dashboard(df_input_ml, df_prev, ml_type):

    # Building the final dataframe, putting past and future together (df to send to dashboard)
    df_final_total = df_input_ml.append(df_prev)

    df_final_total.sort_values(by=['code_neighborhood',
                                   'name_neighborhood', 'ano', 'mes'],
                               inplace=True)

    df_final_total = df_final_total[['code_neighborhood',
                                     'name_neighborhood', 'ano', 'mes',
                                     'dengue_diagnosis']]

    df_final_total = df_final_total[df_final_total['ano'].isin(list(range(2010, 2021)))]

    df_final_total.to_csv(
        base_path + \
        '/data_sus/finais/previsao_' + ml_type + '_dengue_2015_ate_2020.csv')